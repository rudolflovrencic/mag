#ifndef MAG_ALWAYSFALSE_HPP
#define MAG_ALWAYSFALSE_HPP

#include <type_traits>

namespace MAG {

template<typename T>
constexpr bool always_false = false;

}

#endif
