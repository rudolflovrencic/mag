#ifndef MAG_OUT_ABSTRACTSYNTAXTREE_HPP
#define MAG_OUT_ABSTRACTSYNTAXTREE_HPP

#include "Operator.hpp"
#include "Modifier.hpp"

#include <boost/variant.hpp>

#include <optional>
#include <string>
#include <vector>
#include <utility>

namespace MAG::Out {

// Statements
struct Assignment;
struct VoidFunctionCall;
struct FunctionDefinition;
struct ClassDefinition;
struct StructDefinition;
struct ConstructorDefinition;
struct ConstructorCall;
struct Visibility;
struct PreProcessor;
struct Return;
struct ExpressionStatement;
struct IfStatement;
struct ForStatement;
struct WhileStatement;
using Statement = boost::variant<boost::recursive_wrapper<Assignment>,
                                 boost::recursive_wrapper<VoidFunctionCall>,
                                 boost::recursive_wrapper<FunctionDefinition>,
                                 boost::recursive_wrapper<ClassDefinition>,
                                 boost::recursive_wrapper<StructDefinition>,
                                 boost::recursive_wrapper<ConstructorDefinition>,
                                 boost::recursive_wrapper<ConstructorCall>,
                                 boost::recursive_wrapper<Visibility>,
                                 boost::recursive_wrapper<PreProcessor>,
                                 boost::recursive_wrapper<Return>,
                                 boost::recursive_wrapper<ExpressionStatement>,
                                 boost::recursive_wrapper<IfStatement>,
                                 boost::recursive_wrapper<ForStatement>,
                                 boost::recursive_wrapper<WhileStatement>>;

// LValue expressions.
struct VariableDeclaration;
struct VariableReference;
struct StructuredBiding;
using LValue = boost::variant<boost::recursive_wrapper<VariableDeclaration>,
                              boost::recursive_wrapper<StructuredBiding>,
                              boost::recursive_wrapper<VariableReference>>;

// Expressions.
struct PrefixOperation;
struct BinaryOperation;
struct Lambda;
struct LambdaCaptureDeclaration;
struct LambdaCall;
struct FunctionCall;
struct VariableReference;
struct Literal;
struct StringLiteral;
struct MethodCall;
struct InitializerList;
using Expression = boost::variant<boost::recursive_wrapper<VariableDeclaration>,
                                  boost::recursive_wrapper<PrefixOperation>,
                                  boost::recursive_wrapper<BinaryOperation>,
                                  boost::recursive_wrapper<Lambda>,
                                  boost::recursive_wrapper<LambdaCaptureDeclaration>,
                                  boost::recursive_wrapper<LambdaCall>,
                                  boost::recursive_wrapper<FunctionCall>,
                                  boost::recursive_wrapper<StructuredBiding>,
                                  boost::recursive_wrapper<VariableReference>,
                                  boost::recursive_wrapper<Literal>,
                                  boost::recursive_wrapper<StringLiteral>,
                                  boost::recursive_wrapper<MethodCall>,
                                  boost::recursive_wrapper<InitializerList>>;

// Statements.

struct VariableDeclaration final {
    std::string type;
    std::string name;
};

struct Assignment final {
    LValue lvalue;
    AssignmentOperator assignment_operator;
    Expression rvalue;
};

struct VoidFunctionCall final {
    std::string function_name;
    std::vector<Expression> arguments;
};

struct FunctionDefinition final {
    std::string return_type;
    std::string function_name;
    std::vector<VariableDeclaration> arguments;
    FunctionModifier modifiers;
    std::vector<Statement> body;
};

struct ClassDefinition final {
    std::string name;
    std::vector<std::pair<VisibilityModifier, std::string>> inheritance;
    std::vector<Statement> body;
};

struct StructDefinition final {
    std::string name;
    std::vector<std::pair<VisibilityModifier, std::string>> inheritance;
    std::vector<Statement> body;
};

struct ConstructorDefinition final {
    using MemberInitializer = std::pair<std::string, std::vector<Expression>>;
    std::string name;
    std::vector<VariableDeclaration> arguments;
    std::vector<MemberInitializer> member_initializers;
    std::vector<Statement> body;
};

struct ConstructorCall final {
    VariableDeclaration declaration;
    std::vector<Expression> arguments;
};

struct Visibility final {
    VisibilityModifier visibility_modifier;
};

struct PreProcessor final {
    std::string directive;
};

struct Return final {
    Expression expression;
};

struct ExpressionStatement final {
    Expression expression;
};

struct IfStatement final {
    Expression condition;
    std::vector<Statement> then_statements;
    std::vector<Statement> else_statements;
};

struct ForStatement final {
    std::optional<Assignment> init_statement;
    std::optional<Expression> condition;
    std::optional<Assignment> step_statement;
    std::vector<Statement> body;
};

struct WhileStatement final {
    Expression condition;
    std::vector<Statement> body;
};

// Expressions.

struct PrefixOperation final {
    PrefixOperator prefix_operator;
    Expression expression;
};

struct BinaryOperation final {
    BinaryOperator binary_operator;
    Expression left_side;
    Expression right_side;
};

struct Lambda final {
    std::vector<Expression> capture;
    std::vector<VariableDeclaration> arguments;
    std::vector<Statement> body;
};

struct LambdaCaptureDeclaration final {
    std::string variable_name;
    Expression expression;
};

struct LambdaCall final {
    Lambda lambda;
    std::vector<Expression> arguments;
};

struct FunctionCall final {
    std::string function_name;
    std::vector<Expression> arguments;
};

struct StructuredBiding final {
    std::vector<std::string> variables;
};

struct VariableReference final {
    std::optional<std::string> struct_name;
    std::string variable_name;
    std::optional<Expression> index;
};

struct Literal final {
    std::string value;
};

struct StringLiteral final {
    std::string value;
};

struct MethodCall final {
    Expression expression;
    FunctionCall method;
};

struct InitializerList final {
    std::vector<Expression> expressions;
};

}

#endif
