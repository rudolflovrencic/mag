#include "out/Modifier.hpp"

namespace MAG::Out {

FunctionModifier operator|(FunctionModifier first, FunctionModifier second) noexcept
{
    return FunctionModifier{static_cast<unsigned>(first) | static_cast<unsigned>(second)};
}

}
