#ifndef MAG_OUT_COMPONENTABSTRACTSYNTAXTREE_HPP
#define MAG_OUT_COMPONENTABSTRACTSYNTAXTREE_HPP

#include "out/AbstractSyntaxTree.hpp"

#include <MAO/Configuration.hpp>

#include <vector>
#include <map>
#include <string>
#include <functional>
#include <optional>

namespace MAG::Out {

class ComponentAbstractSyntaxTree final {
  private:
    std::vector<Out::Statement> statements;

  public:
    ComponentAbstractSyntaxTree() noexcept;
    explicit ComponentAbstractSyntaxTree(std::size_t reserve_size) noexcept;
    explicit ComponentAbstractSyntaxTree(std::initializer_list<Out::Statement> statements) noexcept;

    void add(Out::Statement statement) noexcept;
    void operator+=(Out::Statement statement) noexcept;

    void add_all_from(ComponentAbstractSyntaxTree other) noexcept;
    void operator+=(ComponentAbstractSyntaxTree other) noexcept;

    std::vector<Out::Statement>& get_statements() noexcept;
    const std::vector<Out::Statement>& get_statements() const noexcept;
};

[[nodiscard]] ComponentAbstractSyntaxTree operator+(ComponentAbstractSyntaxTree lhs,
                                                    ComponentAbstractSyntaxTree rhs) noexcept;

struct HostRefLess final {
    [[nodiscard]] bool operator()(const MAO::Host& lhs, const MAO::Host& rhs) const noexcept;
};

using ComponentAbstractSyntaxTreeMap =
    std::map<std::reference_wrapper<const MAO::Host>, ComponentAbstractSyntaxTree, HostRefLess>;

ComponentAbstractSyntaxTreeMap operator+(ComponentAbstractSyntaxTreeMap lhs,
                                         ComponentAbstractSyntaxTreeMap rhs) noexcept;

using PartialExpressionMap = std::map<std::reference_wrapper<const MAO::Host>, Out::Expression, HostRefLess>;

using LValueMap = std::map<std::reference_wrapper<const MAO::Host>, Out::LValue, HostRefLess>;

}

#endif
