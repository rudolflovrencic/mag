#ifndef MAG_OUT_MODIFIER_HPP
#define MAG_OUT_MODIFIER_HPP

namespace MAG::Out {

enum class VisibilityModifier : unsigned char { Private, Protected, Public };

enum class FunctionModifier : unsigned {
    None     = 0,
    Const    = 1U << 0,
    NoExcept = 1U << 1,
    Override = 1U << 2,
};

FunctionModifier operator|(FunctionModifier first, FunctionModifier second) noexcept;

}

#endif
