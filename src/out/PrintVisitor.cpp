#include "out/PrintVisitor.hpp"

namespace MAG::Out {

namespace {

std::ostream& operator<<(std::ostream& os, const PrefixOperator& po) noexcept;
std::ostream& operator<<(std::ostream& os, const AssignmentOperator& ao) noexcept;
std::ostream& operator<<(std::ostream& os, const BinaryOperator& bo) noexcept;

template<typename Iterator>
void print_range(std::ostream& stream, Iterator begin, Iterator end) noexcept;

std::ostream& operator<<(std::ostream& os, const VisibilityModifier& modifier) noexcept;

std::ostream& operator<<(std::ostream& os, const VariableDeclaration& declaration) noexcept;

std::ostream& operator<<(std::ostream& os, const std::pair<VisibilityModifier, std::string>& p) noexcept;

std::ostream& operator<<(std::ostream& os, const ConstructorDefinition::MemberInitializer& mi) noexcept;

[[nodiscard]] bool has_modifier(const FunctionDefinition& function_definition, FunctionModifier modifier) noexcept;

}

PrintVisitor::PrintVisitor(std::ostream& out_stream) noexcept : out_stream(out_stream) {}

void PrintVisitor::apply_to_all(const std::vector<Statement>& ast) const noexcept
{
    for (const auto& statement : ast) { boost::apply_visitor(*this, statement); }
}

void PrintVisitor::operator()(const Assignment& assignment) const noexcept
{
    boost::apply_visitor(*this, assignment.lvalue);
    out_stream << ' ' << assignment.assignment_operator << ' ';
    boost::apply_visitor(*this, assignment.rvalue);
    out_stream << ";\n";
}

void PrintVisitor::operator()(const VoidFunctionCall& void_function_call) const noexcept
{
    out_stream << void_function_call.function_name << '(';
    visit_range(void_function_call.arguments);
    out_stream << ");\n";
}

void PrintVisitor::operator()(const FunctionDefinition& function_definition) const noexcept
{
    out_stream << function_definition.return_type << ' ';

    out_stream << function_definition.function_name << '(';
    print_range(out_stream, function_definition.arguments.begin(), function_definition.arguments.end());
    out_stream << ')';

    if (has_modifier(function_definition, FunctionModifier::Const)) { out_stream << " const"; }
    if (has_modifier(function_definition, FunctionModifier::NoExcept)) { out_stream << " noexcept"; }
    if (has_modifier(function_definition, FunctionModifier::Override)) { out_stream << " override"; }

    out_stream << "\n{\n";
    apply_to_all(function_definition.body);
    out_stream << "\n}\n";
}

void PrintVisitor::operator()(const ClassDefinition& class_definition) const noexcept
{
    out_stream << "class " << class_definition.name;
    if (!class_definition.inheritance.empty()) { out_stream << " : "; }
    print_range(out_stream, class_definition.inheritance.begin(), class_definition.inheritance.end());
    out_stream << '\n';

    out_stream << "{\n";
    apply_to_all(class_definition.body);
    out_stream << "\n};\n\n";
}

void PrintVisitor::operator()(const StructDefinition& struct_definition) const noexcept
{
    out_stream << "struct " << struct_definition.name;
    if (!struct_definition.inheritance.empty()) { out_stream << " : "; }
    print_range(out_stream, struct_definition.inheritance.begin(), struct_definition.inheritance.end());
    out_stream << '\n';

    out_stream << "{\n";
    apply_to_all(struct_definition.body);
    out_stream << "\n};\n\n";
}

void PrintVisitor::operator()(const ConstructorDefinition& constructor) const noexcept
{
    out_stream << constructor.name << '(';
    print_range(out_stream, constructor.arguments.begin(), constructor.arguments.end());
    out_stream << ')';
    if (!constructor.member_initializers.empty()) {
        out_stream << " : ";
        print_range(out_stream, constructor.member_initializers.begin(), constructor.member_initializers.end());
    }

    out_stream << "\n{\n";
    apply_to_all(constructor.body);
    out_stream << "\n}\n";
}

void PrintVisitor::operator()(const ConstructorCall& constructor_call) const noexcept
{
    out_stream << constructor_call.declaration;
    if (!constructor_call.arguments.empty()) {
        out_stream << '(';
        visit_range(constructor_call.arguments);
        out_stream << ')';
    }
    out_stream << ";\n";
}

void PrintVisitor::operator()(const Visibility& visibility) const noexcept
{
    out_stream << visibility.visibility_modifier << ":\n";
}

void PrintVisitor::operator()(const PreProcessor& pre_processor) const noexcept
{
    // NOTE: Currently, this prints out two new lines after each pre-processor
    //       statement. This could be improved in the future by adding logic
    //       which determines weather or not it is the last pre-processor before
    //       actual C++ code and only then print out the extra empty line.
    out_stream << '#' << pre_processor.directive << "\n\n";
}

void PrintVisitor::operator()(const Return& return_statement) const noexcept
{
    out_stream << "return ";
    boost::apply_visitor(*this, return_statement.expression);
    out_stream << ";\n";
}

void PrintVisitor::operator()(const ExpressionStatement& expr_statement) const noexcept
{
    boost::apply_visitor(*this, expr_statement.expression);
    out_stream << ";\n";
}

void PrintVisitor::operator()(const IfStatement& if_statement) const noexcept
{
    out_stream << "if (";
    boost::apply_visitor(*this, if_statement.condition);
    out_stream << ") {\n";
    apply_to_all(if_statement.then_statements);
    if (!if_statement.else_statements.empty()) {
        out_stream << "} else {\n";
        apply_to_all(if_statement.else_statements);
    }
    out_stream << "}\n";
}

void PrintVisitor::operator()(const ForStatement& for_statement) const noexcept
{
    out_stream << "for (";
    if (for_statement.init_statement) { operator()(*for_statement.init_statement); }
    out_stream << ';';
    if (for_statement.condition) { boost::apply_visitor(*this, *for_statement.condition); }
    out_stream << ';';
    if (for_statement.step_statement) { operator()(*for_statement.step_statement); }
    out_stream << ") {";
    apply_to_all(for_statement.body);
    out_stream << "}\n";
}

void PrintVisitor::operator()(const WhileStatement& while_statement) const noexcept
{
    out_stream << "while (";
    boost::apply_visitor(*this, while_statement.condition);
    out_stream << ") {";
    apply_to_all(while_statement.body);
    out_stream << "}\n";
}

void PrintVisitor::operator()(const VariableDeclaration& variable_declaration) const noexcept
{
    out_stream << variable_declaration;
}

void PrintVisitor::operator()(const PrefixOperation& prefix_operation) const noexcept
{
    out_stream << prefix_operation.prefix_operator;
    boost::apply_visitor(*this, prefix_operation.expression);
}

void PrintVisitor::operator()(const BinaryOperation& binary_operation) const noexcept
{
    boost::apply_visitor(*this, binary_operation.left_side);
    out_stream << ' ' << binary_operation.binary_operator << ' ';
    boost::apply_visitor(*this, binary_operation.right_side);
}

void PrintVisitor::operator()(const Lambda& lambda) const noexcept
{
    out_stream << '[';
    visit_range(lambda.capture);
    out_stream << "](";
    print_range(out_stream, lambda.arguments.begin(), lambda.arguments.end());
    out_stream << ")\n";

    out_stream << "{\n";
    for (const auto& statement : lambda.body) { boost::apply_visitor(*this, statement); }
    out_stream << "\n}\n";
}

void PrintVisitor::operator()(const LambdaCaptureDeclaration& capture_declaration) const noexcept
{
    out_stream << capture_declaration.variable_name << " = ";
    boost::apply_visitor(*this, capture_declaration.expression);
}

void PrintVisitor::operator()(const LambdaCall& lambda_call) const noexcept
{
    operator()(lambda_call.lambda);
    out_stream << '(';
    visit_range(lambda_call.arguments);
    out_stream << ")\n";
}

void PrintVisitor::operator()(const FunctionCall& function_call) const noexcept
{
    out_stream << function_call.function_name << '(';
    visit_range(function_call.arguments);
    out_stream << ')';
}

void PrintVisitor::operator()(const VariableReference& variable_reference) const noexcept
{
    if (variable_reference.struct_name) { out_stream << *variable_reference.struct_name << '.'; }
    out_stream << variable_reference.variable_name;
    if (variable_reference.index) {
        out_stream << '[';
        boost::apply_visitor(*this, *variable_reference.index);
        out_stream << ']';
    }
}

void PrintVisitor::operator()(const StructuredBiding& binding) const noexcept
{
    // NOTE: Copy is always created as "auto" is used instead of "auto&&". This
    //       is currently used for simplicity, but might be changed in the
    //       future and more granular control of structured binding type might
    //       eventually be provided.
    out_stream << "auto [";
    print_range(out_stream, binding.variables.begin(), binding.variables.end());
    out_stream << ']';
}

void PrintVisitor::operator()(const Literal& literal) const noexcept
{
    out_stream << literal.value;
}

void PrintVisitor::operator()(const StringLiteral& string_literal) const noexcept
{
    out_stream << '"' << string_literal.value << '"';
}

void PrintVisitor::operator()(const MethodCall& method_call) const noexcept
{
    boost::apply_visitor(*this, method_call.expression);
    out_stream << '.';
    this->operator()(method_call.method);
}

void PrintVisitor::operator()(const InitializerList& init_list) const noexcept
{
    out_stream << '{';
    visit_range(init_list.expressions);
    out_stream << '}';
}

void PrintVisitor::visit_range(const std::vector<Expression>& expressions) const noexcept
{
    if (expressions.empty()) { return; }
    for (auto it = expressions.begin(); it < expressions.end() - 1; ++it) {
        boost::apply_visitor(*this, *it);
        out_stream << ", ";
    }
    boost::apply_visitor(*this, *(expressions.end() - 1));
}

namespace {

std::ostream& operator<<(std::ostream& os, const PrefixOperator& po) noexcept
{
    switch (po) {
        case PrefixOperator::Not: return os << '!';
    }
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, const AssignmentOperator& ao) noexcept
{
    switch (ao) {
        case AssignmentOperator::Equals: return os << '=';
        case AssignmentOperator::PlusEquals: return os << "+=";
        case AssignmentOperator::MinusEquals: return os << "-=";
        case AssignmentOperator::MultiplyEquals: return os << "*=";
        case AssignmentOperator::DivideEquals: return os << "/=";
    }
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, const BinaryOperator& bo) noexcept
{
    switch (bo) {
        case BinaryOperator::Plus: return os << '+';
        case BinaryOperator::Minus: return os << '-';
        case BinaryOperator::Multiply: return os << '*';
        case BinaryOperator::Divide: return os << '/';
        case BinaryOperator::Equals: return os << "==";
        case BinaryOperator::NotEquals: return os << "!=";
        case BinaryOperator::Less: return os << '<';
        case BinaryOperator::LessEquals: return os << "<=";
        case BinaryOperator::Greater: return os << '>';
        case BinaryOperator::GreaterEquals: return os << ">=";
        case BinaryOperator::LeftShift: return os << "<<";
        case BinaryOperator::RightShift: return os << ">>";
        case BinaryOperator::LogicalAnd: return os << "&&";
        case BinaryOperator::LogicalOr: return os << "||";
    }
    std::terminate();
}

template<typename Iterator>
void print_range(std::ostream& os, Iterator begin, Iterator end) noexcept
{
    if (begin == end) { return; }
    for (auto it = begin; it < end - 1; ++it) { os << *it << ", "; }
    os << *(end - 1);
}

std::ostream& operator<<(std::ostream& os, const VisibilityModifier& modifier) noexcept
{
    switch (modifier) {
        case VisibilityModifier::Private: return os << "private";
        case VisibilityModifier::Protected: return os << "protected";
        case VisibilityModifier::Public: return os << "public";
    }
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, const VariableDeclaration& declaration) noexcept
{
    return os << declaration.type << ' ' << declaration.name;
}

std::ostream& operator<<(std::ostream& os, const std::pair<VisibilityModifier, std::string>& p) noexcept
{
    return os << p.first << ' ' << p.second;
}

std::ostream& operator<<(std::ostream& os, const ConstructorDefinition::MemberInitializer& mi) noexcept
{
    os << mi.first << '(';
    if (!mi.second.empty()) {
        for (auto it = mi.second.begin(); it < mi.second.end() - 1; ++it) {
            boost::apply_visitor(PrintVisitor(os), *it);
            os << ", ";
        }
        boost::apply_visitor(PrintVisitor(os), mi.second.back());
    }
    return os << ')';
}

bool has_modifier(const FunctionDefinition& function_definition, FunctionModifier modifier) noexcept
{
    return static_cast<unsigned>(function_definition.modifiers) & static_cast<unsigned>(modifier);
}

}

}
