#include "out/ComponentAbstractSyntaxTree.hpp"

namespace MAG::Out {

ComponentAbstractSyntaxTree::ComponentAbstractSyntaxTree() noexcept
{
    static constexpr std::size_t reserve_size{16};
    statements.reserve(reserve_size);
}

ComponentAbstractSyntaxTree::ComponentAbstractSyntaxTree(std::size_t reserve_size) noexcept
{
    statements.reserve(reserve_size);
}

ComponentAbstractSyntaxTree::ComponentAbstractSyntaxTree(std::initializer_list<Out::Statement> statements) noexcept
        : statements{statements}
{}

void ComponentAbstractSyntaxTree::add(Out::Statement statement) noexcept
{
    statements.push_back(std::move(statement));
}

void ComponentAbstractSyntaxTree::operator+=(Out::Statement statement) noexcept
{
    statements.push_back(std::move(statement));
}

void ComponentAbstractSyntaxTree::add_all_from(ComponentAbstractSyntaxTree other) noexcept
{
    statements.reserve(statements.size() + other.statements.size());
    for (auto& statement : other.statements) { statements.push_back(std::move(statement)); }
}

void ComponentAbstractSyntaxTree::operator+=(ComponentAbstractSyntaxTree other) noexcept
{
    add_all_from(std::move(other));
}

const std::vector<Out::Statement>& ComponentAbstractSyntaxTree::get_statements() const noexcept
{
    return statements;
}

std::vector<Out::Statement>& ComponentAbstractSyntaxTree::get_statements() noexcept
{
    return statements;
}

ComponentAbstractSyntaxTree operator+(ComponentAbstractSyntaxTree lhs, ComponentAbstractSyntaxTree rhs) noexcept
{
    lhs += std::move(rhs);
    return lhs;
}

bool HostRefLess::operator()(const MAO::Host& lhs, const MAO::Host& rhs) const noexcept
{
    return std::less<std::string>{}(lhs.get_name(), rhs.get_name());
}

ComponentAbstractSyntaxTreeMap operator+(ComponentAbstractSyntaxTreeMap lhs,
                                         ComponentAbstractSyntaxTreeMap rhs) noexcept
{
    for (auto& [key, value] : rhs) {
        auto& existing_value = lhs[key];
        existing_value.add_all_from(std::move(value));
    }
    return lhs;
}

}
