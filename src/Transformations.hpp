#ifndef MAG_TRANSFORMATIONS_HPP
#define MAG_TRANSFORMATIONS_HPP

#include "out/AbstractSyntaxTree.hpp"

#include <MAL/AbstractSyntaxTree.hpp>

namespace MAG {

[[nodiscard]] Out::PrefixOperator to_out(MAL::AST::PrefixOperator op) noexcept;
[[nodiscard]] Out::BinaryOperator to_out(MAL::AST::AdditionOperator op) noexcept;
[[nodiscard]] Out::BinaryOperator to_out(MAL::AST::MultiplicationOperator op) noexcept;
[[nodiscard]] Out::BinaryOperator to_out(MAL::AST::ComparisonOperator op) noexcept;
[[nodiscard]] Out::BinaryOperator to_out(MAL::AST::LogicalOperator op) noexcept;
[[nodiscard]] Out::AssignmentOperator to_out(MAL::AST::AssignmentOperator op) noexcept;

[[nodiscard]] std::string to_out(MAL::AST::TypeKeyword type_keyword, bool as_array = false) noexcept;
[[nodiscard]] std::string to_array_type(std::string type_str) noexcept;

[[nodiscard]] Out::BinaryOperator inverse(Out::BinaryOperator bop) noexcept;

}

#endif
