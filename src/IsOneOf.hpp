#ifndef MAG_ISONEOF_HPP
#define MAG_ISONEOF_HPP

#include <type_traits>

namespace MAG {

template<typename T, typename... Types>
constexpr bool is_one_of = (false || ... || std::is_same_v<T, Types>);

}

#endif
