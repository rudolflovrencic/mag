#include "Transformations.hpp"

#include "Visitor.hpp"

namespace MAG {

Out::PrefixOperator to_out(MAL::AST::PrefixOperator op) noexcept
{
    switch (op) {
        case MAL::AST::PrefixOperator::Not: return Out::PrefixOperator::Not;
    }
    std::terminate();
}

Out::BinaryOperator to_out(MAL::AST::AdditionOperator op) noexcept
{
    switch (op) {
        case MAL::AST::AdditionOperator::Plus: return Out::BinaryOperator::Plus;
        case MAL::AST::AdditionOperator::Minus: return Out::BinaryOperator::Minus;
    }
    std::terminate();
}

Out::BinaryOperator to_out(MAL::AST::MultiplicationOperator op) noexcept
{
    switch (op) {
        case MAL::AST::MultiplicationOperator::Multiply: return Out::BinaryOperator::Multiply;
        case MAL::AST::MultiplicationOperator::Divide: return Out::BinaryOperator::Divide;
    }
    std::terminate();
}

Out::BinaryOperator to_out(MAL::AST::ComparisonOperator op) noexcept
{
    switch (op) {
        using InCompOp = MAL::AST::ComparisonOperator;
        using OutBinOp = Out::BinaryOperator;
        case InCompOp::Equal: return OutBinOp::Equals;
        case InCompOp::NotEqual: return OutBinOp::NotEquals;
        case InCompOp::Less: return OutBinOp::Less;
        case InCompOp::Greater: return OutBinOp::Greater;
        case InCompOp::LessOrEqual: return OutBinOp::LessEquals;
        case InCompOp::GreaterOrEqual: return OutBinOp::GreaterEquals;
    }
    std::terminate();
}

Out::BinaryOperator to_out(MAL::AST::LogicalOperator op) noexcept
{
    switch (op) {
        case MAL::AST::LogicalOperator::And: return Out::BinaryOperator::LogicalAnd;
        case MAL::AST::LogicalOperator::Or: return Out::BinaryOperator::LogicalOr;
    }
    std::terminate();
}

Out::AssignmentOperator to_out(MAL::AST::AssignmentOperator op) noexcept
{
    switch (op) {
        case MAL::AST::AssignmentOperator::Equals: return Out::AssignmentOperator::Equals;
    }
    std::terminate();
}

std::string to_out(MAL::AST::TypeKeyword type_keyword, bool as_array) noexcept
{
    std::string type_str;
    switch (type_keyword) {
        case MAL::AST::TypeKeyword::Int: type_str = "int"; break;
        case MAL::AST::TypeKeyword::Float: type_str = "float"; break;
        case MAL::AST::TypeKeyword::Bool: type_str = "bool"; break;
    }
    if (as_array) { type_str = to_array_type(std::move(type_str)); }
    return type_str;
}

std::string to_array_type(std::string type_str) noexcept
{
    return "std::vector<" + std::move(type_str) + '>';
}

Out::BinaryOperator inverse(Out::BinaryOperator bop) noexcept
{
    switch (bop) {
        using OutBinOp = Out::BinaryOperator;
        case OutBinOp::Plus: return OutBinOp::Minus;
        case OutBinOp::Minus: return OutBinOp::Plus;
        case OutBinOp::Multiply: return OutBinOp::Divide;
        case OutBinOp::Divide: return OutBinOp::Multiply;
        case OutBinOp::Equals: return OutBinOp::NotEquals;
        case OutBinOp::NotEquals: return OutBinOp::NotEquals;
        case OutBinOp::Less: return OutBinOp::GreaterEquals;
        case OutBinOp::LessEquals: return OutBinOp::Greater;
        case OutBinOp::Greater: return OutBinOp::LessEquals;
        case OutBinOp::GreaterEquals: return OutBinOp::Less;
        case OutBinOp::LeftShift: return OutBinOp::RightShift;
        case OutBinOp::RightShift: return OutBinOp::LeftShift;
        case OutBinOp::LogicalAnd: return OutBinOp::LogicalOr;
        case OutBinOp::LogicalOr: return OutBinOp::LogicalAnd;
    }
    std::terminate();
}

}
