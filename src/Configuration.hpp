#ifndef MAG_CONFIGURATION_HPP
#define MAG_CONFIGURATION_HPP

#include <MAL.hpp>
#include <MAO/Configuration.hpp>
#include <boost/json.hpp>

#include <filesystem>
#include <stdexcept>
#include <type_traits>
#include <string_view>

namespace MAG {

[[nodiscard]] MAO::ConstraintVector load_constraints(const boost::json::array& constraints_array,
                                                     const MAO::TaggedNodeVector& tagged_nodes);

[[nodiscard]] MAO::HostVector load_hosts(const boost::json::array& hosts_array);

[[nodiscard]] MAO::Configuration load_mao_configuration(const boost::json::value& config,
                                                        const std::vector<MAL::AST::Statement>& app_ast);

class TopLevelJsonValueNotAnObjectError final : public std::runtime_error {
  public:
    explicit TopLevelJsonValueNotAnObjectError(boost::json::kind encountered_kind) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(boost::json::kind encountered_kind) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<TopLevelJsonValueNotAnObjectError>);

class MissingKeyError final : public std::runtime_error {
  public:
    explicit MissingKeyError(std::string_view missing_key) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(std::string_view missing_key) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingKeyError>);

class InvalidKeyKindError final : public std::runtime_error {
  public:
    InvalidKeyKindError(std::string_view key,
                        boost::json::kind encountered_kind,
                        boost::json::kind expected_kind) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(std::string_view key,
                                                         boost::json::kind encountered_kind,
                                                         boost::json::kind expected_kind) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidKeyKindError>);

class EmptyHostNameError final : public std::runtime_error {
  public:
    explicit EmptyHostNameError(std::size_t host_index) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyHostNameError>);

class EmptyHostArrayError final : public std::runtime_error {
  public:
    EmptyHostArrayError() noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyHostArrayError>);

class ConversionOfHostTrustToUnsignedError final : public std::runtime_error {
  public:
    explicit ConversionOfHostTrustToUnsignedError(std::size_t host_index) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ConversionOfHostTrustToUnsignedError>);

class DuplicateHostNameError final : public std::runtime_error {
  public:
    explicit DuplicateHostNameError(std::string_view duplicate_host_name) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(std::string_view duplicate_host_name) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<DuplicateHostNameError>);

class EmptyConstraintError final : public std::runtime_error {
  public:
    explicit EmptyConstraintError(std::size_t constraint_index) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyConstraintError>);

class EmptyConstraintElementNameError final : public std::runtime_error {
  public:
    EmptyConstraintElementNameError(std::size_t constraint_index, std::size_t element_index) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<EmptyConstraintElementNameError>);

class ConstraintElementDoesNotExistInTheAstError final : public std::runtime_error {
  public:
    ConstraintElementDoesNotExistInTheAstError(std::size_t constraint_index,
                                               std::size_t element_index,
                                               std::string_view element_name) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(std::size_t constraint_index,
                                                         std::size_t element_index,
                                                         std::string_view element_name) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ConstraintElementDoesNotExistInTheAstError>);

class DuplicateConstraintElementError final : public std::runtime_error {
  public:
    DuplicateConstraintElementError(std::size_t constraint_index, std::string_view duplicate_element_name) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(std::size_t constraint_index,
                                                         std::string_view duplicate_element_name) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<DuplicateConstraintElementError>);

class ConversionOfConstraintImportanceToUnsignedError final : public std::runtime_error {
  public:
    explicit ConversionOfConstraintImportanceToUnsignedError(std::size_t constraint_index) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<ConversionOfConstraintImportanceToUnsignedError>);

class DuplicateConstraintError final : public std::runtime_error {
  public:
    explicit DuplicateConstraintError(std::size_t duplicate_constraint_index) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<DuplicateConstraintError>);

}

#endif
