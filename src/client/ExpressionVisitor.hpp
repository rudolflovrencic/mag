#ifndef MAG_CLIENT_EXPRESSIONVISITOR_HPP
#define MAG_CLIENT_EXPRESSIONVISITOR_HPP

#include "out/AbstractSyntaxTree.hpp"

#include <MAL.hpp>

namespace MAG::Client {

class ArithmeticExpressionVisitor final {
  public:
    [[nodiscard]] Out::Expression operator()(const MAL::AST::PrefixExpression& pref_expr) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::MultiplicationExpression& mul_expr) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::AdditionExpression& add_expr) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::ComparisonExpression& comp_expr) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::BooleanAndExpression& and_expr) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::BooleanOrExpression& or_expr) const;
};

class PrimaryExpressionVisitor final : public boost::static_visitor<Out::Expression> {
  public:
    [[nodiscard]] Out::Expression operator()(const MAL::AST::VariableReference& v_ref) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::NumericLiteral& num_lit) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::ArrayLiteral& arr_lit) const;
    [[nodiscard]] Out::Expression operator()(const MAL::AST::LenFunctionCall& len_fn_call) const;
};

class LValueVisitor final {
  public:
    [[nodiscard]] Out::VariableDeclaration operator()(const MAL::AST::VariableDeclaration& vd) const;
    [[nodiscard]] Out::VariableDeclaration operator()(const MAL::AST::VariableReference& vr) const;
};

}

#endif
