#ifndef MAG_PROGRAM_OPTIONS_HPP
#define MAG_PROGRAM_OPTIONS_HPP

#include <boost/program_options.hpp>

namespace MAG {

class ProgramOptions {
  private:
    using OptionsDescription = boost::program_options::options_description;
    using VariablesMap       = boost::program_options::variables_map;

  private:
    OptionsDescription options_description;
    VariablesMap variables_map;

  public:
    ProgramOptions();

    void parse_arguments(int argc, char** argv);

    void add_flag(const std::string& flag, const std::string& help_string);

    template<typename T>
    void add_option_with_value(const std::string& option, const std::string& help_string);

    template<typename T>
    void add_option_with_value(const std::string& option, const std::string& help_string, T&& default_value);

    template<typename T>
    void add_multitoken_option(const std::string& option, const std::string& help_string);

    bool is_present(const std::string& option) const;

    template<typename T>
    const T& get_value(const std::string& option) const;

    std::string get_help_string() const;

    friend std::ostream& operator<<(std::ostream& os, const ProgramOptions& po);
};

template<typename T>
void ProgramOptions::add_option_with_value(const std::string& option, const std::string& help_string)
{
    options_description.add_options()(option.c_str(), boost::program_options::value<T>(), help_string.c_str());
}

template<typename T>
void ProgramOptions::add_option_with_value(const std::string& option, const std::string& help_string, T&& default_value)
{
    options_description.add_options()(option.c_str(),
                                      boost::program_options::value<T>()->default_value(std::forward<T>(default_value)),
                                      help_string.c_str());
}

template<typename T>
void ProgramOptions::add_multitoken_option(const std::string& option, const std::string& help_string)
{
    options_description.add_options()(
        option.c_str(), boost::program_options::value<T>()->multitoken(), help_string.c_str());
}

template<typename T>
const T& ProgramOptions::get_value(const std::string& option) const
{
    return variables_map[option].as<T>();
}

}

#endif
