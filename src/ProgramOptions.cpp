#include "ProgramOptions.hpp"

namespace MAG {

ProgramOptions::ProgramOptions() : options_description("Allowed options")
{
    add_flag("help,h", "produce help message");
}

void ProgramOptions::parse_arguments(int argc, char** argv)
{
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, options_description),
                                  variables_map);
    boost::program_options::notify(variables_map);
}

void ProgramOptions::add_flag(const std::string& flag, const std::string& help_string)
{
    options_description.add_options()(flag.c_str(), help_string.c_str());
}

bool ProgramOptions::is_present(const std::string& option) const
{
    return variables_map.count(option);
}

std::string ProgramOptions::get_help_string() const
{
    std::ostringstream oss;
    oss << options_description;
    auto options_str = oss.str();
    options_str.pop_back(); // Remove trailing new line for consistent output.
    return options_str;
}

std::ostream& operator<<(std::ostream& os, const ProgramOptions& po)
{
    return os << po.get_help_string();
}

}
