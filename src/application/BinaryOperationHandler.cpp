#include "application/BinaryOperationHandler.hpp"

#include "Transformations.hpp"
#include "Visitor.hpp"

#include <cassert>
#include <algorithm>
#include <string>
#include <exception>

namespace MAG::Application {

namespace {

enum class ProtocolPart : char { Initial, Intermediate, Final };

[[nodiscard]] std::string_view to_string(ProtocolPart protocol_part) noexcept;

[[nodiscard]] Out::PartialExpressionMap distribute_expression_if_needed(Out::Expression expression,
                                                                        const MAO::HostRefVector& hosts,
                                                                        MAL::AST::TypeKeyword expression_type) noexcept;

[[nodiscard]] Out::BinaryOperator to_out(InBinaryOperator op) noexcept;

[[nodiscard]] MAL::AST::TypeKeyword get_out_type(MAL::AST::TypeKeyword input_type, InBinaryOperator op) noexcept;

[[nodiscard]] std::string
get_protocol_function_name(ProtocolPart protocol_part, InBinaryOperator op, MAL::AST::TypeKeyword type) noexcept;

class ProtocolFunctionNameExtractor final {
  public:
    ProtocolPart protocol_part;
    MAL::AST::TypeKeyword type;

  public:
    [[nodiscard]] std::string operator()(MAL::AST::AdditionOperator op) const noexcept;
    [[nodiscard]] std::string operator()(MAL::AST::MultiplicationOperator op) const noexcept;
    [[nodiscard]] std::string operator()(MAL::AST::ComparisonOperator op) const noexcept;
    [[nodiscard]] std::string operator()(MAL::AST::LogicalOperator op) const noexcept;
};

struct OperationNameExtractor final {
    [[nodiscard]] std::string_view operator()(MAL::AST::AdditionOperator op) const noexcept;
    [[nodiscard]] std::string_view operator()(MAL::AST::MultiplicationOperator op) const noexcept;
    [[nodiscard]] std::string_view operator()(MAL::AST::ComparisonOperator op) const noexcept;
    [[maybe_unused]] [[nodiscard]] std::string_view operator()(MAL::AST::LogicalOperator op) const noexcept;
};

}

MulticloudOperationHandlingResult handle_multicloud_operation(const MAO::Host& lhost,
                                                              Out::Expression lexpr,
                                                              const MAO::Host& rhost,
                                                              Out::Expression rexpr,
                                                              const MAO::HostRefVector& output_hosts,
                                                              InBinaryOperator op,
                                                              MAL::AST::TypeKeyword input_type) noexcept
{
    assert(!output_hosts.empty());
    assert(!MAO::HostEqualTo{}(lhost, rhost));

    MulticloudOperationHandlingResult result;

    const auto& final_host = output_hosts[0].get();

    result.asts[lhost].add(Out::VoidFunctionCall{
        get_protocol_function_name(ProtocolPart::Initial, op, input_type),
        {std::move(lexpr), Out::StringLiteral{rhost.get_name()}, Out::StringLiteral{final_host.get_name()}},
    });
    result.asts[rhost].add(Out::VoidFunctionCall{
        get_protocol_function_name(ProtocolPart::Intermediate, op, input_type),
        {std::move(rexpr), Out::StringLiteral{lhost.get_name()}, Out::StringLiteral{final_host.get_name()}},
    });
    result.partial_expressions = distribute_expression_if_needed(
        Out::FunctionCall{get_protocol_function_name(ProtocolPart::Final, op, input_type),
                          {Out::StringLiteral{lhost.get_name()}, Out::StringLiteral{rhost.get_name()}}},
        output_hosts,
        get_out_type(input_type, op));

    return result;
}

Out::PartialExpressionMap handle_singlecloud_operation([[maybe_unused]] const MAO::Host& host,
                                                       Out::Expression lhs,
                                                       Out::Expression rhs,
                                                       const MAO::HostRefVector& output_hosts,
                                                       InBinaryOperator op,
                                                       MAL::AST::TypeKeyword input_type) noexcept
{
    assert(output_hosts.contains(host));

    const auto result_type = get_out_type(input_type, op);
    return distribute_expression_if_needed(
        Out::BinaryOperation{to_out(op), std::move(lhs), std::move(rhs)}, output_hosts, result_type);
}

namespace {

std::string_view to_string(ProtocolPart protocol_part) noexcept
{
    switch (protocol_part) {
        case ProtocolPart::Initial: return "initial";
        case ProtocolPart::Intermediate: return "intermediate";
        case ProtocolPart::Final: return "final";
    }
    std::terminate();
}

Out::PartialExpressionMap distribute_expression_if_needed(Out::Expression expression,
                                                          const MAO::HostRefVector& hosts,
                                                          MAL::AST::TypeKeyword expression_type) noexcept
{
    assert(!hosts.empty());

    Out::PartialExpressionMap result;
    if (hosts.size() == 1) {
        [[maybe_unused]] const auto [res_it, inserted] = result.insert({hosts.front(), std::move(expression)});
        assert(inserted);
    } else {
        const auto& write_host = hosts.front().get();
        const Out::FunctionCall read_fn_call{"read<" + MAG::to_out(expression_type) + '>',
                                             {Out::StringLiteral{write_host.get_name()}}};
        Out::InitializerList read_hosts_init_list;
        read_hosts_init_list.expressions.reserve(hosts.size() - 1);
        for (auto it = std::next(hosts.cbegin()); it != hosts.cend(); ++it) {
            const auto& read_host                          = it->get();
            [[maybe_unused]] const auto [res_it, inserted] = result.insert({read_host, read_fn_call});
            assert(inserted);
            read_hosts_init_list.expressions.push_back(Out::StringLiteral{read_host.get_name()});
        }
        [[maybe_unused]] const auto [res_it, inserted] = result.insert(
            {write_host,
             Out::FunctionCall{"write_and_return", {std::move(expression), std::move(read_hosts_init_list)}}});
        assert(inserted);
    }
    return result;
}

Out::BinaryOperator to_out(InBinaryOperator op) noexcept
{
    return std::visit(
        Visitor{
            [](MAL::AST::AdditionOperator add_op) noexcept -> Out::BinaryOperator { return MAG::to_out(add_op); },
            [](MAL::AST::MultiplicationOperator mul_op) noexcept -> Out::BinaryOperator { return MAG::to_out(mul_op); },
            [](MAL::AST::ComparisonOperator cmp_op) noexcept -> Out::BinaryOperator { return MAG::to_out(cmp_op); },
            [](MAL::AST::LogicalOperator log_op) noexcept -> Out::BinaryOperator { return MAG::to_out(log_op); },
        },
        op);
}

MAL::AST::TypeKeyword get_out_type(MAL::AST::TypeKeyword input_type, InBinaryOperator op) noexcept
{
    return std::visit(Visitor{
                          [input_type](MAL::AST::AdditionOperator /*add_op*/) noexcept -> MAL::AST::TypeKeyword {
                              return input_type;
                          },
                          [input_type](MAL::AST::MultiplicationOperator /*mul_op*/) noexcept -> MAL::AST::TypeKeyword {
                              return input_type;
                          },
                          [](MAL::AST::ComparisonOperator /*cmp_op*/) noexcept -> MAL::AST::TypeKeyword {
                              return MAL::AST::TypeKeyword::Bool;
                          },
                          [](MAL::AST::LogicalOperator /*log_op*/) noexcept -> MAL::AST::TypeKeyword {
                              return MAL::AST::TypeKeyword::Bool;
                          },
                      },
                      op);
}

std::string
get_protocol_function_name(ProtocolPart protocol_part, InBinaryOperator op, MAL::AST::TypeKeyword type) noexcept
{
    return std::visit(ProtocolFunctionNameExtractor{protocol_part, type}, op);
}

std::string ProtocolFunctionNameExtractor::operator()(MAL::AST::AdditionOperator op) const noexcept
{
    std::ostringstream oss;
    oss << "perform_arithmetic_protocol_" << to_string(protocol_part) << '<' << OperationNameExtractor{}(op) << ", "
        << MAG::to_out(type) << '>';
    return oss.str();
}

std::string ProtocolFunctionNameExtractor::operator()(MAL::AST::MultiplicationOperator op) const noexcept
{
    std::ostringstream oss;
    oss << "perform_arithmetic_protocol_" << to_string(protocol_part) << '<' << OperationNameExtractor{}(op) << ", "
        << MAG::to_out(type) << '>';
    return oss.str();
}

std::string ProtocolFunctionNameExtractor::operator()(MAL::AST::ComparisonOperator op) const noexcept
{
    std::ostringstream oss;
    oss << "perform_";
    switch (op) {
        case MAL::AST::ComparisonOperator::Equal:
        case MAL::AST::ComparisonOperator::NotEqual: oss << "equality"; break;
        default: oss << "comparison"; break;
    }
    oss << "_protocol_" << to_string(protocol_part) << '<';
    if (protocol_part == ProtocolPart::Final) { oss << OperationNameExtractor{}(op) << ", "; }
    oss << MAG::to_out(type) << '>';
    return oss.str();
}

std::string ProtocolFunctionNameExtractor::operator()(MAL::AST::LogicalOperator /*op*/) const noexcept
{
    // Multi-cloud logical operations are not yet supported.
    assert(false);
    std::terminate();
}

std::string_view OperationNameExtractor::operator()(MAL::AST::AdditionOperator op) const noexcept
{
    switch (op) {
        case MAL::AST::AdditionOperator::Plus: return "Add";
        case MAL::AST::AdditionOperator::Minus: return "Subtract";
    }
    std::terminate();
}

std::string_view OperationNameExtractor::operator()(MAL::AST::MultiplicationOperator op) const noexcept
{
    switch (op) {
        case MAL::AST::MultiplicationOperator::Multiply: return "Multiply";
        case MAL::AST::MultiplicationOperator::Divide: return "Divide";
    }
    std::terminate();
}

std::string_view OperationNameExtractor::operator()(MAL::AST::ComparisonOperator op) const noexcept
{
    switch (op) {
        case MAL::AST::ComparisonOperator::Equal: return "Equal";
        case MAL::AST::ComparisonOperator::NotEqual: return "NotEqual";
        case MAL::AST::ComparisonOperator::Less: return "Less";
        case MAL::AST::ComparisonOperator::Greater: return "Greater";
        case MAL::AST::ComparisonOperator::LessOrEqual: return "LessOrEqual";
        case MAL::AST::ComparisonOperator::GreaterOrEqual: return "GreaterOrEqual";
    }
    std::terminate();
}

std::string_view OperationNameExtractor::operator()(MAL::AST::LogicalOperator op) const noexcept
{
    switch (op) {
        case MAL::AST::LogicalOperator::And: return "And";
        case MAL::AST::LogicalOperator::Or: return "Or";
    }
    std::terminate();
}

}

}
