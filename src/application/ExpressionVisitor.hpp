#ifndef MAG_APPLICATION_EXPRESSIONVISITOR_HPP
#define MAG_APPLICATION_EXPRESSIONVISITOR_HPP

#include "AlwaysFalse.hpp"
#include "IsOneOf.hpp"
#include "out/ComponentAbstractSyntaxTree.hpp"
#include "application/BinaryOperationHandler.hpp"

#include <MAO/Mapping.hpp>
#include <MAL.hpp>

#include <variant>
#include <memory>
#include <unordered_map>
#include <functional>
#include <optional>

namespace MAG::Application {

struct ExpressionVisitorResult final {
    Out::ComponentAbstractSyntaxTreeMap asts;
    Out::PartialExpressionMap partial_expressions;
};

class ArithmeticExpressionVisitor final {
  private:
    MAO::HostRefVector required_hosts;
    std::reference_wrapper<const MAO::Mapping> mapping;

  public:
    ArithmeticExpressionVisitor(MAO::HostRefVector required_hosts, const MAO::Mapping& mapping) noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::PrefixExpression& pref_expr,
                                                     bool is_top_level) const noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::MultiplicationExpression& mul_expr,
                                                     bool is_top_level) const noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::AdditionExpression& add_expr,
                                                     bool is_top_level) const noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::ComparisonExpression& comp_expr,
                                                     bool is_top_level) const noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::BooleanAndExpression& and_expr) const noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::BooleanOrExpression& or_expr) const noexcept;

  private:
    template<typename Subexpression, typename Element>
    [[nodiscard]] ExpressionVisitorResult
    handle_first_operation_pair(const Subexpression& lhs, const Element& elem, bool is_final) const noexcept;

    template<typename ElementVectorConstIterator>
    [[nodiscard]] ExpressionVisitorResult handle_other_operation_pairs(ExpressionVisitorResult result,
                                                                       ElementVectorConstIterator second_elem_it,
                                                                       ElementVectorConstIterator end_it,
                                                                       bool is_top_level) const noexcept;

    struct BinaryOperationComputationData final {
        bool is_multicloud_operation;
        std::reference_wrapper<const MAO::Host> lhost;
        std::reference_wrapper<const MAO::Host> rhost;
        MAO::HostRefVector result_hosts;
        Out::ComponentAbstractSyntaxTreeMap asts;
        Out::Expression lexpr;
        Out::Expression rexpr;
    };

    template<typename Subexpression, typename Element>
    [[nodiscard]] BinaryOperationComputationData
    get_binary_operation_computation_data(const Subexpression& lhs, const Element& elem, bool is_final) const noexcept;
};

class PrimaryExpressionVisitor final {
  public:
    static constexpr std::string_view len_variable_sufix{"_len"};

  private:
    MAO::HostRefVector required_hosts;
    std::reference_wrapper<const MAO::Mapping> mapping;

  public:
    PrimaryExpressionVisitor(MAO::HostRefVector required_hosts, const MAO::Mapping& mapping) noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::VariableReference& var_ref) const noexcept;
    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::NumericLiteral& num_lit) const noexcept;
    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::LenFunctionCall& len_fn_call) const noexcept;
};

struct LValueVisitorResult final {
    Out::ComponentAbstractSyntaxTreeMap asts;
    Out::LValueMap lvalues;
};

class LValueVisitor final {
  private:
    std::reference_wrapper<const MAO::Mapping> mapping;

  public:
    explicit LValueVisitor(const MAO::Mapping& mapping) noexcept;

    [[nodiscard]] LValueVisitorResult operator()(const MAL::AST::VariableDeclaration& var_decl) const noexcept;
    [[nodiscard]] LValueVisitorResult operator()(const MAL::AST::VariableReference& var_ref) const noexcept;
};

class AssignmentRightSideVisitor final {
  private:
    MAO::HostRefVector required_hosts;
    std::reference_wrapper<const MAO::Mapping> mapping;

  public:
    AssignmentRightSideVisitor(MAO::HostRefVector required_hosts, const MAO::Mapping& mapping) noexcept;

    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept;
    [[nodiscard]] ExpressionVisitorResult operator()(const MAL::AST::ArrayLiteral& arr_lit) const noexcept;
};

[[nodiscard]] MAO::HostRefVector get_hosts(const Out::PartialExpressionMap partial_expressions) noexcept;

[[nodiscard]] const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                                  const MAL::AST::PrefixExpression& pref_expr) noexcept;
[[nodiscard]] const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                                  const MAL::AST::MultiplicationExpression& mul_expr) noexcept;
[[nodiscard]] const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                                  const MAL::AST::AdditionExpression& add_expr) noexcept;
[[nodiscard]] const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                                  const MAL::AST::ComparisonExpression& comp_expr) noexcept;
[[nodiscard]] const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                                  const MAL::AST::BooleanAndExpression& and_expr) noexcept;
[[nodiscard]] const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                                  const MAL::AST::BooleanOrExpression& or_expr) noexcept;

template<typename Subexpression, typename Element>
ExpressionVisitorResult ArithmeticExpressionVisitor::handle_first_operation_pair(const Subexpression& lhs,
                                                                                 const Element& elem,
                                                                                 bool is_top_level) const noexcept
{
    static_assert((std::is_same_v<Element, MAL::AST::MultiplicationExpression::Element> &&
                   std::is_same_v<Subexpression, MAL::AST::PrefixExpression>) ||
                  (std::is_same_v<Element, MAL::AST::AdditionExpression::Element> &&
                   std::is_same_v<Subexpression, MAL::AST::MultiplicationExpression>) ||
                  (std::is_same_v<Element, MAL::AST::ComparisonExpression::Element> &&
                   std::is_same_v<Subexpression, MAL::AST::AdditionExpression>));

    const auto& [op, rhs] = elem;
    auto [is_multicloud_operation, lhost, rhost, result_hosts, asts, lexpr, rexpr] =
        get_binary_operation_computation_data(lhs, elem, is_top_level);

    ExpressionVisitorResult result{std::move(asts), {}};

    if (is_multicloud_operation) {
        auto multicloud_result = handle_multicloud_operation(
            lhost, std::move(lexpr), rhost, std::move(rexpr), result_hosts, op, lhs.type->type);
        result.asts                = std::move(result.asts) + std::move(multicloud_result.asts);
        result.partial_expressions = std::move(multicloud_result.partial_expressions);
    } else {
        auto singlecloud_result =
            handle_singlecloud_operation(lhost, std::move(lexpr), std::move(rexpr), result_hosts, op, lhs.type->type);
        result.partial_expressions = std::move(singlecloud_result);
    }

    return result;
}

template<typename ElementVectorConstIterator>
ExpressionVisitorResult
ArithmeticExpressionVisitor::handle_other_operation_pairs(ExpressionVisitorResult result,
                                                          ElementVectorConstIterator second_elem_it,
                                                          ElementVectorConstIterator end_it,
                                                          bool is_top_level) const noexcept
{
    static_assert(is_one_of<ElementVectorConstIterator,
                            std::vector<MAL::AST::MultiplicationExpression::Element>::const_iterator,
                            std::vector<MAL::AST::AdditionExpression::Element>::const_iterator,
                            std::vector<MAL::AST::ComparisonExpression::Element>::const_iterator>);

    for (auto it = second_elem_it; it != end_it; ++it) {
        const auto is_last    = std::next(it) == end_it;
        const auto& elem      = *it;
        const auto& [op, rhs] = elem;
        assert(result.partial_expressions.size() == 1);
        auto [lhost, lexpr] = std::move(*result.partial_expressions.begin());

        const auto& elem_hosts = is_top_level && is_last ? required_hosts : MAO::get_hosts(mapping, elem);
        assert(elem_hosts.contains(lhost));

        auto rresult = ArithmeticExpressionVisitor{{lhost}, mapping}(rhs, false);
        assert(rresult.partial_expressions.size() == 1);
        auto rexpr = std::move(rresult.partial_expressions.begin()->second);

        result.asts = std::move(result.asts) + std::move(rresult.asts);

        result.partial_expressions =
            handle_singlecloud_operation(lhost, std::move(lexpr), std::move(rexpr), elem_hosts, op, rhs.type->type);
    }

    return result;
}

template<typename Subexpression, typename Element>
[[nodiscard]] auto ArithmeticExpressionVisitor::get_binary_operation_computation_data(const Subexpression& lhs,
                                                                                      const Element& elem,
                                                                                      bool is_top_level) const noexcept
    -> BinaryOperationComputationData
{
    static_assert((std::is_same_v<Element, MAL::AST::MultiplicationExpression::Element> &&
                   std::is_same_v<Subexpression, MAL::AST::PrefixExpression>) ||
                  (std::is_same_v<Element, MAL::AST::AdditionExpression::Element> &&
                   std::is_same_v<Subexpression, MAL::AST::MultiplicationExpression>) ||
                  (std::is_same_v<Element, MAL::AST::ComparisonExpression::Element> &&
                   std::is_same_v<Subexpression, MAL::AST::AdditionExpression>));

    const auto& [op, rhs]              = elem;
    const auto& lhosts                 = get_hosts(mapping, lhs);
    const auto& rhosts                 = get_hosts(mapping, rhs);
    const auto intersection            = MAO::compute_intersection(lhosts, rhosts);
    const auto is_multicloud_operation = intersection.empty();

    const auto lhost{is_multicloud_operation ? lhosts.front() : intersection.front()};
    const auto rhost{is_multicloud_operation ? rhosts.front() : intersection.front()};

    auto lresult = ArithmeticExpressionVisitor{{lhost}, mapping}(lhs, false);
    auto rresult = ArithmeticExpressionVisitor{{rhost}, mapping}(rhs, false);
    assert(lresult.partial_expressions.size() == 1);
    assert(rresult.partial_expressions.size() == 1);

    auto result_hosts = is_top_level ? required_hosts : MAO::get_hosts(mapping, elem);
    if (!is_multicloud_operation) { result_hosts.push_back(lhost); }

    auto lexpr = std::move(lresult.partial_expressions.begin()->second);
    auto rexpr = std::move(rresult.partial_expressions.begin()->second);

    return BinaryOperationComputationData{is_multicloud_operation,
                                          lhost,
                                          rhost,
                                          result_hosts,
                                          std::move(lresult.asts) + std::move(rresult.asts),
                                          std::move(lexpr),
                                          std::move(rexpr)};
}

}

#endif
