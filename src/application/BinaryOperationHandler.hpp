#ifndef MAG_APPLICATION_BINARYOPERATIONHANDLER_HPP
#define MAG_APPLICATION_BINARYOPERATIONHANDLER_HPP

#include "out/ComponentAbstractSyntaxTree.hpp"

#include <MAL.hpp>
#include <MAO/Mapping.hpp>

#include <variant>

namespace MAG::Application {

using InBinaryOperator = std::variant<MAL::AST::AdditionOperator,
                                      MAL::AST::MultiplicationOperator,
                                      MAL::AST::ComparisonOperator,
                                      MAL::AST::LogicalOperator>;

struct MulticloudOperationHandlingResult final {
    Out::ComponentAbstractSyntaxTreeMap asts;
    Out::PartialExpressionMap partial_expressions;
};

[[nodiscard]] MulticloudOperationHandlingResult
handle_multicloud_comparison_operation(const MAO::Host& lhost,
                                       Out::Expression lexpr,
                                       const MAO::Host& rhost,
                                       Out::Expression rexpr,
                                       const MAO::HostRefVector& output_hosts,
                                       MAL::AST::ComparisonOperator op,
                                       MAL::AST::TypeKeyword expression_type) noexcept;

[[nodiscard]] MulticloudOperationHandlingResult handle_multicloud_operation(const MAO::Host& lhost,
                                                                            Out::Expression lexpr,
                                                                            const MAO::Host& rhost,
                                                                            Out::Expression rexpr,
                                                                            const MAO::HostRefVector& output_hosts,
                                                                            InBinaryOperator op,
                                                                            MAL::AST::TypeKeyword input_type) noexcept;

[[nodiscard]] Out::PartialExpressionMap handle_singlecloud_operation(const MAO::Host& host,
                                                                     Out::Expression lexpr,
                                                                     Out::Expression rexpr,
                                                                     const MAO::HostRefVector& output_hosts,
                                                                     InBinaryOperator op,
                                                                     MAL::AST::TypeKeyword input_type) noexcept;

}

#endif
