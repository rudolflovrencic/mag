#include "application/ExpressionVisitor.hpp"

#include "Transformations.hpp"
#include "out/PrintVisitor.hpp"
#include "Visitor.hpp"

#include <algorithm>
#include <string_view>
#include <cassert>
#include <sstream>
#include <iomanip>

namespace MAG::Application {

namespace {

[[nodiscard]] Out::PartialExpressionMap
create_partial_expresson_map_with_single_expression(const MAO::HostRefVector& hosts,
                                                    Out::Expression partial_expression) noexcept;

}

ArithmeticExpressionVisitor::ArithmeticExpressionVisitor(MAO::HostRefVector required_hosts,
                                                         const MAO::Mapping& mapping) noexcept
        : required_hosts{std::move(required_hosts)}, mapping{mapping}
{
    assert(!this->required_hosts.empty());
}

ExpressionVisitorResult ArithmeticExpressionVisitor::operator()(const MAL::AST::PrefixExpression& pref_expr,
                                                                bool /*is_top_level*/) const noexcept
{
    auto result = boost::apply_visitor(PrimaryExpressionVisitor{required_hosts, mapping}, pref_expr.primary_expression);

    if (pref_expr.prefix_operator) {
        const auto out_pref_op = to_out(*pref_expr.prefix_operator);
        for (auto& [host, partial_expression] : result.partial_expressions) {
            partial_expression = Out::PrefixOperation{out_pref_op, std::move(partial_expression)};
        }
    }

    return result;
}

ExpressionVisitorResult ArithmeticExpressionVisitor::operator()(const MAL::AST::MultiplicationExpression& mul_expr,
                                                                bool is_top_level) const noexcept
{
    if (mul_expr.other_expressions.empty()) { return operator()(mul_expr.first_expression, is_top_level); }

    auto result = handle_first_operation_pair(mul_expr.first_expression,
                                              mul_expr.other_expressions[0],
                                              is_top_level && mul_expr.other_expressions.size() == 1);

    return handle_other_operation_pairs(std::move(result),
                                        std::next(mul_expr.other_expressions.cbegin()),
                                        mul_expr.other_expressions.cend(),
                                        is_top_level);
}

ExpressionVisitorResult ArithmeticExpressionVisitor::operator()(const MAL::AST::AdditionExpression& add_expr,
                                                                bool is_top_level) const noexcept
{
    if (add_expr.other_expressions.empty()) { return operator()(add_expr.first_expression, is_top_level); }

    auto result = handle_first_operation_pair(add_expr.first_expression,
                                              add_expr.other_expressions[0],
                                              is_top_level && add_expr.other_expressions.size() == 1);

    return handle_other_operation_pairs(std::move(result),
                                        std::next(add_expr.other_expressions.cbegin()),
                                        add_expr.other_expressions.cend(),
                                        is_top_level);
}

ExpressionVisitorResult ArithmeticExpressionVisitor::operator()(const MAL::AST::ComparisonExpression& comp_expr,
                                                                bool is_top_level) const noexcept
{
    if (comp_expr.other_expressions.empty()) { return operator()(comp_expr.first_expression, is_top_level); }

    auto result = handle_first_operation_pair(comp_expr.first_expression,
                                              comp_expr.other_expressions[0],
                                              is_top_level && comp_expr.other_expressions.size() == 1);

    return handle_other_operation_pairs(std::move(result),
                                        std::next(comp_expr.other_expressions.cbegin()),
                                        comp_expr.other_expressions.cend(),
                                        is_top_level);
}

ExpressionVisitorResult
ArithmeticExpressionVisitor::operator()(const MAL::AST::BooleanAndExpression& and_expr) const noexcept
{
    if (!and_expr.other_expressions.empty()) {
        std::cerr << "Multiple 'and' expressions not yet supported.\n";
        std::terminate();
    }

    return operator()(and_expr.first_expression, true);
}

ExpressionVisitorResult
ArithmeticExpressionVisitor::operator()(const MAL::AST::BooleanOrExpression& or_expr) const noexcept
{
    if (!or_expr.other_expressions.empty()) {
        std::cerr << "Multiple 'or' expressions not yet supported.\n";
        std::terminate();
    }

    return operator()(or_expr.first_expression);
}

PrimaryExpressionVisitor::PrimaryExpressionVisitor(MAO::HostRefVector required_hosts,
                                                   const MAO::Mapping& mapping) noexcept
        : required_hosts{std::move(required_hosts)}, mapping{mapping}
{
    assert(!this->required_hosts.empty());
}

ExpressionVisitorResult PrimaryExpressionVisitor::operator()(const MAL::AST::VariableReference& var_ref) const noexcept
{
    assert(var_ref.type);
    assert(var_ref.variable_identifier.declaration);
    assert(MAO::is_subset(required_hosts, MAO::get_hosts(mapping, *var_ref.variable_identifier.declaration)));

    ExpressionVisitorResult result;
    if (var_ref.index) {
        auto [index_asts, index_partial_expressions] =
            ArithmeticExpressionVisitor{required_hosts, mapping}(*var_ref.index, true);
        result.asts = std::move(index_asts);
        for (const auto host : required_hosts) {
            const auto it = index_partial_expressions.find(host);
            assert(it != index_partial_expressions.cend());
            auto& index_partial_expression                    = it->second;
            [[maybe_unused]] const auto [result_it, inserted] = result.partial_expressions.insert({
                host,
                Out::VariableReference{std::nullopt, var_ref.variable_identifier, std::move(index_partial_expression)},
            });
            assert(inserted);
        }
    } else {
        result.partial_expressions = create_partial_expresson_map_with_single_expression(
            required_hosts, Out::VariableReference{std::nullopt, var_ref.variable_identifier, std::nullopt});
    }
    return result;
}

ExpressionVisitorResult PrimaryExpressionVisitor::operator()(const MAL::AST::NumericLiteral& num_lit) const noexcept
{
    assert(num_lit.type);
    assert(MAO::is_subset(required_hosts, MAO::get_hosts(mapping, num_lit)));

    auto num_lit_as_string = boost::apply_visitor(
        [](auto numeric_literal) noexcept -> std::string {
            std::ostringstream oss;
            oss << std::boolalpha << numeric_literal;
            return oss.str();
        },
        num_lit);

    return ExpressionVisitorResult{
        Out::ComponentAbstractSyntaxTreeMap{},
        create_partial_expresson_map_with_single_expression(required_hosts, Out::Literal{std::move(num_lit_as_string)}),
    };
}

ExpressionVisitorResult
PrimaryExpressionVisitor::operator()(const MAL::AST::LenFunctionCall& len_fn_call) const noexcept
{
    assert(len_fn_call.variable_reference.variable_identifier.declaration);
    assert(!len_fn_call.variable_reference.index);
    assert(MAO::is_subset(required_hosts, MAO::get_hosts(mapping, MAO::ArrayVariableLength{len_fn_call})));

    const auto& variable_identifier = len_fn_call.variable_reference.variable_identifier;

    return ExpressionVisitorResult{
        Out::ComponentAbstractSyntaxTreeMap{},
        create_partial_expresson_map_with_single_expression(required_hosts,
                                                            Out::VariableReference{
                                                                std::nullopt,
                                                                variable_identifier + std::string{len_variable_sufix},
                                                                std::nullopt,
                                                            }),
    };
}

LValueVisitor::LValueVisitor(const MAO::Mapping& mapping) noexcept : mapping{mapping} {}

LValueVisitorResult LValueVisitor::operator()(const MAL::AST::VariableDeclaration& var_decl) const noexcept
{
    const auto& hosts = MAO::get_hosts(mapping, var_decl);
    const Out::VariableDeclaration out_var_decl{"auto", var_decl.variable_name};
    Out::LValueMap lvalues;
    for (const MAO::Host& host : hosts) { lvalues.insert({host, out_var_decl}); }
    return LValueVisitorResult{Out::ComponentAbstractSyntaxTreeMap{}, std::move(lvalues)};
}

LValueVisitorResult LValueVisitor::operator()(const MAL::AST::VariableReference& vr) const noexcept
{
    assert(vr.variable_identifier.declaration);

    const auto& hosts = MAO::get_hosts(mapping, *vr.variable_identifier.declaration);

    LValueVisitorResult result;
    if (vr.index) {
        auto index_result = ArithmeticExpressionVisitor{hosts, mapping}(*vr.index, true);
        result.asts       = std::move(index_result.asts);
        for (const MAO::Host& host : hosts) {
            const auto it = index_result.partial_expressions.find(host);
            assert(it != index_result.partial_expressions.cend());
            result.lvalues.insert(
                {host, Out::VariableReference{std::nullopt, vr.variable_identifier, std::move(it->second)}});
        }
    } else {
        for (const MAO::Host& host : hosts) {
            result.lvalues.insert({host, Out::VariableReference{std::nullopt, vr.variable_identifier, std::nullopt}});
        }
    }

    return result;
}

AssignmentRightSideVisitor::AssignmentRightSideVisitor(MAO::HostRefVector required_hosts,
                                                       const MAO::Mapping& mapping) noexcept
        : required_hosts{std::move(required_hosts)}, mapping{mapping}
{
    assert(!this->required_hosts.empty());
}

ExpressionVisitorResult AssignmentRightSideVisitor::operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    return ArithmeticExpressionVisitor{std::move(required_hosts), mapping}(or_expr);
}

ExpressionVisitorResult AssignmentRightSideVisitor::operator()(const MAL::AST::ArrayLiteral& arr_lit) const noexcept
{
    assert(arr_lit.type);

    auto value_result = ArithmeticExpressionVisitor{required_hosts, mapping}(arr_lit.value);
    auto size_result  = ArithmeticExpressionVisitor{required_hosts, mapping}(arr_lit.size, true);

    Out::PartialExpressionMap partial_expressions;
    for (const MAO::Host& host : required_hosts) {
        const auto value_it = value_result.partial_expressions.find(host);
        assert(value_it != value_result.partial_expressions.cend());
        const auto size_it = size_result.partial_expressions.find(host);
        assert(size_it != size_result.partial_expressions.cend());
        partial_expressions.insert({
            host,
            Out::FunctionCall{"std::vector<" + to_out(*arr_lit.type) + ">",
                              {std::move(size_it->second), std::move(value_it->second)}},
        });
    }

    return ExpressionVisitorResult{std::move(value_result.asts) + std::move(size_result.asts),
                                   std::move(partial_expressions)};
}

MAO::HostRefVector get_hosts(const Out::PartialExpressionMap partial_expressions) noexcept
{
    MAO::HostRefVector result;
    for (const auto& [host, partial_expression] : partial_expressions) { result.push_back(host); }
    return result;
}

const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping, const MAL::AST::PrefixExpression& pref_expr) noexcept
{
    return boost::apply_visitor(
        Visitor{[&mapping](const MAL::AST::VariableReference& var_ref) noexcept -> const MAO::HostRefVector& {
                    assert(var_ref.variable_identifier.declaration);
                    return MAO::get_hosts(mapping, *var_ref.variable_identifier.declaration);
                },
                [&mapping](const MAL::AST::NumericLiteral& num_lit) noexcept -> const MAO::HostRefVector& {
                    return MAO::get_hosts(mapping, num_lit);
                },
                [&mapping](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> const MAO::HostRefVector& {
                    return MAO::get_hosts(mapping, MAO::ArrayVariableLength{len_fn_call});
                }},
        pref_expr.primary_expression);
}

const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                    const MAL::AST::MultiplicationExpression& mul_expr) noexcept
{
    if (mul_expr.other_expressions.empty()) { return get_hosts(mapping, mul_expr.first_expression); }
    return MAO::get_hosts(mapping, mul_expr.other_expressions.back());
}

const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping, const MAL::AST::AdditionExpression& add_expr) noexcept
{
    if (add_expr.other_expressions.empty()) { return get_hosts(mapping, add_expr.first_expression); }
    return MAO::get_hosts(mapping, add_expr.other_expressions.back());
}

const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                    const MAL::AST::ComparisonExpression& comp_expr) noexcept
{
    if (comp_expr.other_expressions.empty()) { return get_hosts(mapping, comp_expr.first_expression); }
    return MAO::get_hosts(mapping, comp_expr.other_expressions.back());
}

const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping,
                                    const MAL::AST::BooleanAndExpression& and_expr) noexcept
{
    if (and_expr.other_expressions.empty()) { return get_hosts(mapping, and_expr.first_expression); }
    return MAO::get_hosts(mapping, and_expr.other_expressions.back());
}

const MAO::HostRefVector& get_hosts(const MAO::Mapping& mapping, const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    if (or_expr.other_expressions.empty()) { return get_hosts(mapping, or_expr.first_expression); }
    return MAO::get_hosts(mapping, or_expr.other_expressions.back());
}

namespace {

Out::PartialExpressionMap
create_partial_expresson_map_with_single_expression(const MAO::HostRefVector& hosts,
                                                    Out::Expression partial_expression) noexcept
{
    assert(!hosts.empty());

    Out::PartialExpressionMap result;
    for (auto it = hosts.cbegin(); it != std::prev(hosts.cend()); ++it) {
        [[maybe_unused]] const auto& [result_it, inserted] = result.insert({*it, partial_expression});
        assert(inserted);
    }
    [[maybe_unused]] const auto& [result_it, inserted] = result.insert({hosts.back(), std::move(partial_expression)});
    assert(inserted);
    return result;
}

}

}
