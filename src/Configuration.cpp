#include "Configuration.hpp"

namespace MAG {

MAO::ConstraintVector load_constraints(const boost::json::array& constraints_array,
                                       const MAO::TaggedNodeVector& tagged_nodes)
{
    MAO::ConstraintVector res;
    res.reserve(constraints_array.size());

    std::size_t constraint_index{0};
    for (const auto& constraint_value : constraints_array) {
        const auto constraint_key{"constraints[" + std::to_string(constraint_index) + ']'};
        const auto* const constraint_obj = constraint_value.if_object();
        if (!constraint_obj) {
            throw InvalidKeyKindError{constraint_key, constraint_value.kind(), boost::json::kind::object};
        }

        const auto* const elements_value = constraint_obj->if_contains("elements");
        if (!elements_value) { throw MissingKeyError{constraint_key + ".elements"}; }
        const auto* const elements_array = elements_value->if_array();
        if (!elements_array) {
            throw InvalidKeyKindError{constraint_key + ".elements", elements_value->kind(), boost::json::kind::array};
        }
        if (elements_array->empty()) { throw EmptyConstraintError{constraint_index}; }

        MAO::ConstrainedNodeVector elements;
        elements.reserve(elements_array->size());
        std::size_t element_index{0};
        for (const auto& element_value : *elements_array) {
            const auto element_key{constraint_key + ".elements[" + std::to_string(element_index) + ']'};
            const auto* const name = element_value.if_string();
            if (!name) { throw InvalidKeyKindError{element_key, elements_value->kind(), boost::json::kind::string}; }
            if (name->empty()) { throw EmptyConstraintElementNameError{constraint_index, element_index}; }
            const auto tagged_node_it = std::find_if(
                tagged_nodes.cbegin(), tagged_nodes.cend(), [name](MAO::TaggedNode tagged_node) noexcept -> bool {
                    // Currently, only tagged variable declarations can be expressed as constraint elements. Note that
                    // this is the limitation of the configuration description, not MAO implementation.
                    if (const auto* const var_decl =
                            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_node)) {
                        return var_decl->get().variable_name == *name;
                    }
                    return false;
                });
            if (tagged_node_it == tagged_nodes.cend()) {
                throw ConstraintElementDoesNotExistInTheAstError{constraint_index, element_index, *name};
            }
            auto constrained_node_opt = MAO::to_constrained_node(*tagged_node_it);
            assert(constrained_node_opt);
            const auto [it, inserted] = elements.push_back(*constrained_node_opt);
            if (!inserted) { throw DuplicateConstraintElementError{constraint_index, *name}; }
            ++element_index;
        }

        const auto* const importance_value = constraint_obj->if_contains("importance");
        if (!importance_value) { throw MissingKeyError{constraint_key + ".importance"}; }
        boost::json::error_code error_code;
        const auto importance = importance_value->to_number<unsigned>(error_code);
        if (error_code) { throw ConversionOfConstraintImportanceToUnsignedError{constraint_index}; }

        const auto [it, inserted] = res.push_back(MAO::Constraint{std::move(elements), importance});
        if (!inserted) { throw DuplicateConstraintError{constraint_index}; }
        ++constraint_index;
    }

    return res;
}

MAO::HostVector load_hosts(const boost::json::array& hosts_array)
{
    if (hosts_array.empty()) { throw EmptyHostArrayError{}; }

    MAO::HostVector res;
    res.reserve(hosts_array.size());

    std::size_t host_index{0};
    for (const auto& host_value : hosts_array) {
        const auto host_key{"hosts[" + std::to_string(host_index) + ']'};
        const auto* const host_obj = host_value.if_object();
        if (!host_obj) { throw InvalidKeyKindError{host_key, host_value.kind(), boost::json::kind::object}; }

        const auto* const name_value = host_obj->if_contains("name");
        if (!name_value) { throw MissingKeyError{host_key + ".name"}; }
        const auto* const name_str = name_value->if_string();
        if (!name_str) { throw InvalidKeyKindError{host_key + ".name", name_value->kind(), boost::json::kind::string}; }
        if (name_str->empty()) { throw EmptyHostNameError{host_index}; }

        const auto* const trust_value = host_obj->if_contains("trust");
        if (!trust_value) { throw MissingKeyError{host_key + ".trust"}; }
        boost::json::error_code error_code;
        const auto trust = trust_value->to_number<unsigned>(error_code);
        if (error_code) { throw ConversionOfHostTrustToUnsignedError{host_index}; }

        const auto [it, inserted] = res.push_back(MAO::Host{std::string{*name_str}, trust});
        if (!inserted) { throw DuplicateHostNameError{*name_str}; }
        ++host_index;
    }

    return res;
}

MAO::Configuration load_mao_configuration(const boost::json::value& config,
                                          const std::vector<MAL::AST::Statement>& app_ast)
{
    const auto* const config_obj = config.if_object();
    if (!config_obj) { throw TopLevelJsonValueNotAnObjectError{config.kind()}; }

    const auto* const constraints_value = config_obj->if_contains("constraints");
    if (!constraints_value) { throw MissingKeyError{"constraints"}; }
    const auto* const constraints_array = constraints_value->if_array();
    if (!constraints_array) {
        throw InvalidKeyKindError{"constraints", constraints_value->kind(), boost::json::kind::array};
    }

    const auto* const hosts_value = config_obj->if_contains("hosts");
    if (!hosts_value) { throw MissingKeyError{"hosts"}; }
    const auto* const hosts_array = hosts_value->if_array();
    if (!hosts_array) { throw InvalidKeyKindError{"hosts", hosts_value->kind(), boost::json::kind::array}; }

    auto tagged_nodes = MAO::extract_tagged_nodes(app_ast);
    auto constraints  = load_constraints(*constraints_array, tagged_nodes);

    auto equivalences             = MAO::extract_equivalences(app_ast);
    auto trios                    = MAO::extract_trios(app_ast);
    auto flow_control_information = MAO::extract_flow_control_information(app_ast);

    auto nodes_to_optimize    = MAO::get_nodes_to_optimize(constraints, equivalences, trios);
    auto indexing_information = MAO::extract_indexing_information(app_ast, nodes_to_optimize);

    return MAO::Configuration{load_hosts(*hosts_array),
                              std::move(tagged_nodes),
                              std::move(nodes_to_optimize),
                              std::move(constraints),
                              std::move(equivalences),
                              std::move(indexing_information),
                              std::move(trios),
                              std::move(flow_control_information)};
}

TopLevelJsonValueNotAnObjectError::TopLevelJsonValueNotAnObjectError(boost::json::kind encountered_kind) noexcept
        : std::runtime_error{format_what_message(encountered_kind)}
{}

std::string TopLevelJsonValueNotAnObjectError::format_what_message(boost::json::kind encountered_kind) noexcept
{
    assert(encountered_kind != boost::json::kind::object);
    std::ostringstream oss;
    oss << "Top level of the JSON configuration must be of kind " << boost::json::kind::object << " but "
        << encountered_kind << " was encountered.";
    return oss.str();
}

MissingKeyError::MissingKeyError(std::string_view missing_key) noexcept
        : std::runtime_error{format_what_message(missing_key)}
{}

std::string MissingKeyError::format_what_message(std::string_view missing_key) noexcept
{
    std::ostringstream oss;
    oss << "Key " << std::quoted(missing_key) << " missing in the configuration object.";
    return oss.str();
}

InvalidKeyKindError::InvalidKeyKindError(std::string_view key,
                                         boost::json::kind encountered_kind,
                                         boost::json::kind expected_kind) noexcept
        : std::runtime_error{format_what_message(key, encountered_kind, expected_kind)}
{}

std::string InvalidKeyKindError::format_what_message(std::string_view key,
                                                     boost::json::kind encountered_kind,
                                                     boost::json::kind expected_kind) noexcept
{
    assert(expected_kind != encountered_kind);
    std::ostringstream oss;
    oss << "Key " << std::quoted(key) << " was expected to be of kind " << expected_kind << " but " << encountered_kind
        << " was encountered.";
    return oss.str();
}

EmptyHostNameError::EmptyHostNameError(std::size_t host_index) noexcept
        : std::runtime_error{"Empty host name for key host at index " + std::to_string(host_index) + '.'}
{}

EmptyHostArrayError::EmptyHostArrayError() noexcept
        : std::runtime_error{"Configuration must provide at least one host."}
{}

ConversionOfHostTrustToUnsignedError::ConversionOfHostTrustToUnsignedError(std::size_t host_index) noexcept
        : std::runtime_error{"Trust of host at index " + std::to_string(host_index) + " is not a valid unsigend value."}
{}

DuplicateHostNameError::DuplicateHostNameError(std::string_view duplicate_host_name) noexcept
        : std::runtime_error{format_what_message(duplicate_host_name)}
{}

std::string DuplicateHostNameError::format_what_message(std::string_view duplicate_host_name) noexcept
{
    std::ostringstream oss;
    oss << "Multiple hosts with name " << std::quoted(duplicate_host_name) << '.';
    return oss.str();
}

EmptyConstraintError::EmptyConstraintError(std::size_t constraint_index) noexcept
        : std::runtime_error{"Constraint at index " + std::to_string(constraint_index) + " contains no elements."}
{}

EmptyConstraintElementNameError::EmptyConstraintElementNameError(std::size_t constraint_index,
                                                                 std::size_t element_index) noexcept
        : std::runtime_error{"Constraint at index " + std::to_string(constraint_index) + " contains element at index " +
                             std::to_string(element_index) + " that has an empty name."}
{}

ConstraintElementDoesNotExistInTheAstError::ConstraintElementDoesNotExistInTheAstError(
    std::size_t constraint_index, std::size_t element_index, std::string_view element_name) noexcept
        : std::runtime_error{format_what_message(constraint_index, element_index, element_name)}
{}

std::string ConstraintElementDoesNotExistInTheAstError::format_what_message(std::size_t constraint_index,
                                                                            std::size_t element_index,
                                                                            std::string_view element_name) noexcept
{
    std::ostringstream oss;
    oss << "Constraint element at constraints[" << constraint_index << "].elements[" << element_index
        << "] names variable " << std::quoted(element_name)
        << " that does not exist in the application abstract syntax tree.";
    return oss.str();
}

DuplicateConstraintElementError::DuplicateConstraintElementError(std::size_t constraint_index,
                                                                 std::string_view duplicate_element_name) noexcept
        : std::runtime_error{format_what_message(constraint_index, duplicate_element_name)}
{}

std::string DuplicateConstraintElementError::format_what_message(std::size_t constraint_index,
                                                                 std::string_view duplicate_element_name) noexcept
{
    std::ostringstream oss;
    oss << "Constraint at index " << constraint_index << " contains multiple elements "
        << std::quoted(duplicate_element_name) << '.';
    return oss.str();
}

ConversionOfConstraintImportanceToUnsignedError::ConversionOfConstraintImportanceToUnsignedError(
    std::size_t constraint_index) noexcept
        : std::runtime_error{"Importance of constraint at index " + std::to_string(constraint_index) +
                             " is not a valid unsigend value."}
{}

DuplicateConstraintError::DuplicateConstraintError(std::size_t duplicate_constraint_index) noexcept
        : std::runtime_error{"Constraint at index " + std::to_string(duplicate_constraint_index) +
                             " has the same elements as some other provided constraint."}
{}

}
