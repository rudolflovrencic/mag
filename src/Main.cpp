#include <filesystem>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <type_traits>

#include <MAL.hpp>

#include <MAO/Optimizer.hpp>
#include <MAO/Mapping.hpp>

#include "out/PrintVisitor.hpp"
#include "out/QueryVisitor.hpp"

#include "ProgramOptions.hpp"
#include "Configuration.hpp"

#include "application/StatementVisitor.hpp"

namespace {

[[nodiscard]] std::string read_file(const std::filesystem::path& filepath);

void print_component_ast(std::ostream& os,
                         const MAO::Host& host,
                         const MAG::Out::ComponentAbstractSyntaxTree& ast) noexcept;

}

int main(int argc, char** argv)
{
    // Add command-line options and parse the provided command line arguments.
    MAG::ProgramOptions program_options;
    program_options.add_option_with_value<std::filesystem::path>(
        "client", "file containing client MAL code", "../client.mal");
    program_options.add_option_with_value<std::filesystem::path>(
        "app", "file containing application MAL code", "../app.mal");
    program_options.add_option_with_value<std::filesystem::path>("config", "configuration file path", "../config.json");
    program_options.add_option_with_value<std::string>("optimizer", "optimization algorithm to use", "exhaustive");
    program_options.parse_arguments(argc, argv);
    if (program_options.is_present("help")) {
        std::cout << program_options.get_help_string() << '\n';
        return EXIT_SUCCESS;
    }

    try {
        // Read the client and the application MAL code from files.
        const auto client_mal_code = read_file(program_options.get_value<std::filesystem::path>("client"));
        const auto app_mal_code    = read_file(program_options.get_value<std::filesystem::path>("app"));

        // Parse client and application MAL code to create abstract syntax trees.
        const auto client_ast = MAL::parse_and_check_semantics(client_mal_code);
        const auto app_ast    = MAL::parse_and_check_semantics(app_mal_code);

        // Parse the configuration file.
        const auto mao_config = MAG::load_mao_configuration(
            boost::json::parse(read_file(program_options.get_value<std::filesystem::path>("config"))), app_ast);

        // Optimize the multi-cloud application for the provided cloud environment and security constraints.
        const auto optimizer_name = program_options.get_value<std::string>("optimizer");
        auto optimizer            = MAO::Optimizer::create_from_name(optimizer_name);
        const auto genotype       = optimizer->optimize(mao_config);
        std::cout << "Best solution:\n"
                  << MAO::GenotypeLogger{genotype, mao_config.get_nodes_to_optimize(), mao_config.get_hosts()};
        const auto mapping = MAO::convert_genotype_to_mapping(genotype, mao_config);
        std::cout << "Best mapping:\n" << MAO::MappingLogger{mapping} << '\n';

        const auto out_app_asts = MAG::Application::generate(mapping, app_ast);
        for (const auto& [host, ast] : out_app_asts) { print_component_ast(std::cout, host, ast); }

        // Generate client and multi-cloud application components.Output ASTs are C++ ASTs leveraging the
        // multi-cloud application framework (MAF).
        //        MAG::Application::Generator app_gen;
        //        MAG::Client::Generator client_gen;
        //        app_gen.apply_to_all(app_ast);
        //        // client_gen.apply_to_all(client_ast); // TODO: Uncomment later.
        //
        //        // Print out ASTs of all generated components as (unformatted) C++ code.
        //        std::ostringstream oss;
        //        MAG::Out::PrintVisitor{oss}.apply_to_all(client_gen.get_builder().statements);
        //        fmt::print("; // clang-format on\n");
        //        fmt::print("/* {:-^74} */\n{}/* {:-^74} */\n\n\n", " Client ", oss.str(), "");
        //        for (const auto& [component_name, builder] : app_gen.get_builders()) {
        //            std::ostringstream oss;
        //            MAG::Out::PrintVisitor{oss}.apply_to_all(builder.statements);
        //            fmt::print("/* {:-^74} */\n{}/* {:-^74} */\n\n\n", " " + component_name + " ", oss.str(), "");
        //        }
    } catch (const std::exception& err) {
        std::cout << err.what() << '\n';
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

namespace {

class FileReadError final : public std::runtime_error {
  public:
    explicit FileReadError(const std::filesystem::path& filepath) noexcept
            : std::runtime_error{format_what_message(filepath)}
    {}

  private:
    [[nodiscard]] static std::string format_what_message(const std::filesystem::path& filepath) noexcept
    {
        std::ostringstream oss;
        oss << "Failed to open file " << filepath << '.';
        return oss.str();
    }
};
static_assert(std::is_nothrow_copy_constructible_v<FileReadError>);

std::string read_file(const std::filesystem::path& filepath)
{
    if (!std::filesystem::is_regular_file(filepath)) { throw FileReadError{filepath}; }

    std::string content;
    const auto file_size = std::filesystem::file_size(filepath);
    content.reserve(file_size);

    std::ifstream file{filepath};
    if (!file) { throw FileReadError{filepath}; }

    std::copy(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>(), std::back_inserter(content));
    if (content.size() != file_size) { throw FileReadError{filepath}; }

    return content;
}

void print_component_ast(std::ostream& os,
                         const MAO::Host& host,
                         const MAG::Out::ComponentAbstractSyntaxTree& ast) noexcept
{
    static constexpr std::size_t header_width{80};
    static constexpr std::string_view comment_begin{"/* "};
    static constexpr std::string_view comment_end{" */"};
    static constexpr std::size_t padding_width{1}; // Padding on both sides of the host name.
    static constexpr std::size_t dashes_width{header_width - comment_begin.size() - comment_end.size() -
                                              padding_width * 2};

    { // Header.
        const auto n_dashes       = host.get_name().size() > dashes_width ? 0 : dashes_width - host.get_name().size();
        const auto n_dashes_left  = n_dashes / 2;
        const auto n_dashes_right = n_dashes_left + (n_dashes % 2);

        os << comment_begin;
        for (std::size_t i{0}; i < n_dashes_left; ++i) { os << '-'; }
        for (std::size_t i{0}; i < padding_width; ++i) { os << ' '; }
        os << host.get_name();
        for (std::size_t i{0}; i < padding_width; ++i) { os << ' '; }
        for (std::size_t i{0}; i < n_dashes_right; ++i) { os << '-'; }
        os << comment_end << '\n';
    }

    MAG::Out::PrintVisitor{os}.apply_to_all(ast.get_statements());

    { // Footer.
        const auto n_dashes = header_width - comment_begin.size() - comment_end.size();
        os << comment_begin;
        for (std::size_t i{0}; i < n_dashes; ++i) { os << '-'; }
        os << comment_end << '\n';
    }

    os << '\n';
}

}
