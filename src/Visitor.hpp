#ifndef MAG_VISITOR_HPP
#define MAG_VISITOR_HPP

namespace MAG {

template<typename... Base>
struct Visitor final : Base... {
    using Base::operator()...;
};

template<typename... T>
Visitor(T...) -> Visitor<T...>;

}

#endif
