#include <boost/test/unit_test.hpp>

#include "application/StatementVisitor.hpp"

BOOST_AUTO_TEST_SUITE(application);

BOOST_AUTO_TEST_SUITE(statement_visitor);

BOOST_AUTO_TEST_CASE(basic_assignment_results_in_expected_asts)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}};

    static constexpr std::string_view app_mal_code{R"(
        var a int = 42;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto* const a = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    const auto* const lit = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(lit);
    const MAO::Mapping mapping{
        {*a, {hosts[0]}},
        {*lit, {hosts[0]}},
    };

    const auto asts = MAG::Application::generate(mapping, app_ast);

    BOOST_REQUIRE_EQUAL(asts.size(), 1);
    {
        const auto it = asts.find(hosts[0]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 1);
        const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
        BOOST_REQUIRE(assignment);
        {
            const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
            BOOST_REQUIRE(var_decl);
            BOOST_CHECK_EQUAL(var_decl->type, "auto");
            BOOST_CHECK_EQUAL(var_decl->name, "a");
        }
        BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
        {
            const auto* const lit = boost::get<MAG::Out::Literal>(&assignment->rvalue);
            BOOST_REQUIRE(lit);
            BOOST_CHECK_EQUAL(lit->value, "42");
        }
    }
}

BOOST_AUTO_TEST_CASE(multiplication_expression_with_three_variables_results_in_expected_output)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 200}, MAO::Host{"H2", 300}, MAO::Host{"H3", 400}};

    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int, var c int;
        var d int = a * b * c;
        send d;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto* const a = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "a";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    const auto* const b = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "b";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(b);
    const auto* const c = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "c";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(c);
    const auto* const d = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "d";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(d);
    const auto* const mul_expr =
        MAL::AST::QueryVisitor<const MAL::AST::MultiplicationExpression>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(mul_expr);
    BOOST_REQUIRE_EQUAL(mul_expr->other_expressions.size(), 2);
    const auto& el1 = mul_expr->other_expressions[0];
    const auto& el2 = mul_expr->other_expressions[1];
    const MAO::Mapping mapping{
        {*a, {hosts[0]}},
        {*b, {hosts[1]}},
        {el1, {hosts[2]}},
        {*c, {hosts[2]}},
        {*d, {hosts[2], hosts[3]}},
        {el2, {hosts[2], hosts[3]}},
    };

    const auto asts = MAG::Application::generate(mapping, app_ast);

    BOOST_REQUIRE_EQUAL(asts.size(), 4);
    { // H0
        const auto it = asts.find(hosts[0]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            {
                const auto* const mc_add_fn = boost::get<MAG::Out::VoidFunctionCall>(&statements[1]);
                BOOST_REQUIRE(mc_add_fn);
                BOOST_CHECK_EQUAL(mc_add_fn->function_name, "perform_arithmetic_protocol_initial<Multiply, int>");
                BOOST_REQUIRE_EQUAL(mc_add_fn->arguments.size(), 3);
                {
                    const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&mc_add_fn->arguments[0]);
                    BOOST_REQUIRE(var_ref);
                    BOOST_CHECK(!var_ref->struct_name);
                    BOOST_CHECK(!var_ref->index);
                    BOOST_CHECK_EQUAL(var_ref->variable_name, "a");
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[1]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[1].get_name());
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[2]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
                }
            }
        }
    }
    { // H1
        const auto it = asts.find(hosts[1]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "b");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            {
                const auto* const mc_add_fn = boost::get<MAG::Out::VoidFunctionCall>(&statements[1]);
                BOOST_REQUIRE(mc_add_fn);
                BOOST_CHECK_EQUAL(mc_add_fn->function_name, "perform_arithmetic_protocol_intermediate<Multiply, int>");
                BOOST_REQUIRE_EQUAL(mc_add_fn->arguments.size(), 3);
                {
                    const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&mc_add_fn->arguments[0]);
                    BOOST_REQUIRE(var_ref);
                    BOOST_CHECK(!var_ref->struct_name);
                    BOOST_CHECK(!var_ref->index);
                    BOOST_CHECK_EQUAL(var_ref->variable_name, "b");
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[1]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[2]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
                }
            }
        }
    }
    { // H2
        const auto it = asts.find(hosts[2]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "c");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[1]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "d");
            }
            {
                const auto* const write_and_ret_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(write_and_ret_fn_call);
                BOOST_CHECK_EQUAL(write_and_ret_fn_call->function_name, "write_and_return");
                BOOST_REQUIRE_EQUAL(write_and_ret_fn_call->arguments.size(), 2);
                {
                    const auto* const bin_op =
                        boost::get<MAG::Out::BinaryOperation>(&write_and_ret_fn_call->arguments[0]);
                    BOOST_REQUIRE(bin_op);
                    BOOST_CHECK(bin_op->binary_operator == MAG::Out::BinaryOperator::Multiply);
                    {
                        const auto* const mc_add_fn = boost::get<MAG::Out::FunctionCall>(&bin_op->left_side);
                        BOOST_REQUIRE(mc_add_fn);
                        BOOST_CHECK_EQUAL(mc_add_fn->function_name, "perform_arithmetic_protocol_final<Multiply, int>");
                        BOOST_REQUIRE_EQUAL(mc_add_fn->arguments.size(), 2);
                        {
                            const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[0]);
                            BOOST_REQUIRE(lit);
                            BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
                        }
                        {
                            const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[1]);
                            BOOST_REQUIRE(lit);
                            BOOST_CHECK_EQUAL(lit->value, hosts[1].get_name());
                        }
                    }
                    {
                        const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&bin_op->right_side);
                        BOOST_REQUIRE(var_ref);
                        BOOST_CHECK(!var_ref->struct_name);
                        BOOST_CHECK(!var_ref->index);
                        BOOST_CHECK_EQUAL(var_ref->variable_name, "c");
                    }
                    {
                        const auto* const init_list =
                            boost::get<MAG::Out::InitializerList>(&write_and_ret_fn_call->arguments[1]);
                        BOOST_REQUIRE(init_list);
                        BOOST_REQUIRE_EQUAL(init_list->expressions.size(), 1);
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&init_list->expressions[0]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, hosts[3].get_name());
                    }
                }
            }
        }
    }
    { // H3
        const auto it = asts.find(hosts[3]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 1);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "d");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(assignment_with_indexing_results_in_expected_asts)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 100}, MAO::Host{"H2", 300}, MAO::Host{"H3", 400}};

    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int, var c int;
        a[b + c] = 42;
        send a;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto* const a = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "a";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    const auto* const b = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "b";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(b);
    const auto* const c = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "c";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(c);
    const auto* const lit = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(lit);
    const auto* const add = MAL::AST::QueryVisitor<const MAL::AST::AdditionExpression::Element>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(add);
    const MAO::Mapping mapping{
        {*a, {hosts[0], hosts[1]}},
        {*b, {hosts[2]}},
        {*c, {hosts[3]}},
        {*lit, {hosts[0], hosts[1]}},
        {*add, {hosts[0], hosts[1]}},
    };

    const auto asts = MAG::Application::generate(mapping, app_ast);

    BOOST_REQUIRE_EQUAL(asts.size(), 4);
    { // H0
        const auto it = asts.find(hosts[0]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[1]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&assignment->lvalue);
                BOOST_REQUIRE(var_ref);
                BOOST_CHECK_EQUAL(var_ref->variable_name, "a");
                BOOST_CHECK(!var_ref->struct_name);
                BOOST_REQUIRE(var_ref->index);
                const auto* const write_and_return_fn_call = boost::get<MAG::Out::FunctionCall>(&*var_ref->index);
                BOOST_REQUIRE(write_and_return_fn_call);
                BOOST_CHECK_EQUAL(write_and_return_fn_call->function_name, "write_and_return");
                BOOST_REQUIRE_EQUAL(write_and_return_fn_call->arguments.size(), 2);
                {
                    const auto* const mc_add_fn =
                        boost::get<MAG::Out::FunctionCall>(&write_and_return_fn_call->arguments[0]);
                    BOOST_REQUIRE(mc_add_fn);
                    BOOST_CHECK_EQUAL(mc_add_fn->function_name, "perform_arithmetic_protocol_final<Add, int>");
                    BOOST_REQUIRE_EQUAL(mc_add_fn->arguments.size(), 2);
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[0]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn->arguments[1]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, hosts[3].get_name());
                    }
                }
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const lit = boost::get<MAG::Out::Literal>(&assignment->rvalue);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "42");
            }
        }
        {
            const auto* const write_fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statements[2]);
            BOOST_REQUIRE(write_fn_call);
            BOOST_CHECK_EQUAL(write_fn_call->function_name, "write");
            BOOST_REQUIRE_EQUAL(write_fn_call->arguments.size(), 2);
            {
                const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&write_fn_call->arguments[0]);
                BOOST_REQUIRE(var_ref);
                BOOST_CHECK(!var_ref->struct_name);
                BOOST_CHECK(!var_ref->index);
                BOOST_CHECK_EQUAL(var_ref->variable_name, "a");
            }
            {
                const auto* const init_list = boost::get<MAG::Out::InitializerList>(&write_fn_call->arguments[1]);
                BOOST_REQUIRE(init_list);
                BOOST_REQUIRE_EQUAL(init_list->expressions.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&init_list->expressions[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
    }
    { // H1
        const auto it = asts.find(hosts[1]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[1]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&assignment->lvalue);
                BOOST_REQUIRE(var_ref);
                BOOST_CHECK_EQUAL(var_ref->variable_name, "a");
                BOOST_CHECK(!var_ref->struct_name);
                BOOST_REQUIRE(var_ref->index);
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&*var_ref->index);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
                }
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const lit = boost::get<MAG::Out::Literal>(&assignment->rvalue);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "42");
            }
        }
    }
    { // H2
        const auto it = asts.find(hosts[2]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "b");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const mc_add_fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statements[1]);
            BOOST_REQUIRE(mc_add_fn_call);
            BOOST_CHECK_EQUAL(mc_add_fn_call->function_name, "perform_arithmetic_protocol_initial<Add, int>");
            BOOST_REQUIRE_EQUAL(mc_add_fn_call->arguments.size(), 3);
            {
                const auto* const b_ref = boost::get<MAG::Out::VariableReference>(&mc_add_fn_call->arguments[0]);
                BOOST_REQUIRE(b_ref);
                BOOST_CHECK(!b_ref->struct_name);
                BOOST_CHECK(!b_ref->index);
                BOOST_CHECK_EQUAL(b_ref->variable_name, "b");
            }
            {
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn_call->arguments[1]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[3].get_name());
            }
            {
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn_call->arguments[2]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
            }
        }
    }
    { // H3
        const auto it = asts.find(hosts[3]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "c");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const mc_add_fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statements[1]);
            BOOST_REQUIRE(mc_add_fn_call);
            BOOST_CHECK_EQUAL(mc_add_fn_call->function_name, "perform_arithmetic_protocol_intermediate<Add, int>");
            BOOST_REQUIRE_EQUAL(mc_add_fn_call->arguments.size(), 3);
            {
                const auto* const b_ref = boost::get<MAG::Out::VariableReference>(&mc_add_fn_call->arguments[0]);
                BOOST_REQUIRE(b_ref);
                BOOST_CHECK(!b_ref->struct_name);
                BOOST_CHECK(!b_ref->index);
                BOOST_CHECK_EQUAL(b_ref->variable_name, "c");
            }
            {
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn_call->arguments[1]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
            }
            {
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&mc_add_fn_call->arguments[2]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(simple_if_results_in_expected_asts)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 100}, MAO::Host{"H2", 300}, MAO::Host{"H3", 400}};

    static constexpr std::string_view app_mal_code{R"(
        receive var a bool, var b int, var c int, var d int;
        if (a) {
            send b;
        } else {
            var e int = b + c;
            send e;
        }
        send d;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto* const a = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "a";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    const auto* const b = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "b";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(b);
    const auto* const c = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "c";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(c);
    const auto* const d = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "d";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(d);
    const auto* const e = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "e";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(e);
    const auto* const add = MAL::AST::QueryVisitor<const MAL::AST::AdditionExpression::Element>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(add);
    const MAO::Mapping mapping{
        {*a, {hosts[0], hosts[1], hosts[2]}},
        {*b, {hosts[0]}},
        {*c, {hosts[1]}},
        {*add, {hosts[2]}},
        {*e, {hosts[2]}},
        {*d, {hosts[3]}},
    };

    const auto asts = MAG::Application::generate(mapping, app_ast);

    BOOST_REQUIRE_EQUAL(asts.size(), 4);
    { // H0
        const auto it = asts.find(hosts[0]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<bool>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[1]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "b");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const if_statement = boost::get<MAG::Out::IfStatement>(&statements[2]);
            BOOST_REQUIRE(if_statement);
            {
                const auto* const ref = boost::get<MAG::Out::VariableReference>(&if_statement->condition);
                BOOST_REQUIRE(ref);
                BOOST_CHECK(!ref->index);
                BOOST_CHECK(!ref->struct_name);
                BOOST_CHECK_EQUAL(ref->variable_name, "a");
            }
            BOOST_REQUIRE_EQUAL(if_statement->then_statements.size(), 1);
            {
                const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&if_statement->then_statements[0]);
                BOOST_REQUIRE(fn_call);
                BOOST_CHECK_EQUAL(fn_call->function_name, "write");
                BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 2);
                {
                    const auto* const ref = boost::get<MAG::Out::VariableReference>(&fn_call->arguments[0]);
                    BOOST_REQUIRE(ref);
                    BOOST_CHECK(!ref->index);
                    BOOST_CHECK(!ref->struct_name);
                    BOOST_CHECK_EQUAL(ref->variable_name, "b");
                }
                {
                    const auto* const init_list = boost::get<MAG::Out::InitializerList>(&fn_call->arguments[1]);
                    BOOST_REQUIRE(init_list);
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&init_list->expressions[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, "Client");
                }
            }
            BOOST_REQUIRE_EQUAL(if_statement->else_statements.size(), 1);
            {
                const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&if_statement->else_statements[0]);
                BOOST_REQUIRE(fn_call);
                BOOST_CHECK_EQUAL(fn_call->function_name, "perform_arithmetic_protocol_initial<Add, int>");
                BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 3);
                {
                    const auto* const ref = boost::get<MAG::Out::VariableReference>(&fn_call->arguments[0]);
                    BOOST_REQUIRE(ref);
                    BOOST_CHECK(!ref->index);
                    BOOST_CHECK(!ref->struct_name);
                    BOOST_CHECK_EQUAL(ref->variable_name, "b");
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[1]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[1].get_name());
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[2]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
                }
            }
        }
    }
    { // H1
        const auto it = asts.find(hosts[1]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<bool>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[1]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "c");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const if_statement = boost::get<MAG::Out::IfStatement>(&statements[2]);
            BOOST_REQUIRE(if_statement);
            {
                const auto* const ref = boost::get<MAG::Out::VariableReference>(&if_statement->condition);
                BOOST_REQUIRE(ref);
                BOOST_CHECK(!ref->index);
                BOOST_CHECK(!ref->struct_name);
                BOOST_CHECK_EQUAL(ref->variable_name, "a");
            }
            BOOST_REQUIRE(if_statement->then_statements.empty());
            BOOST_REQUIRE_EQUAL(if_statement->else_statements.size(), 1);
            {
                const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&if_statement->else_statements[0]);
                BOOST_REQUIRE(fn_call);
                BOOST_CHECK_EQUAL(fn_call->function_name, "perform_arithmetic_protocol_intermediate<Add, int>");
                BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 3);
                {
                    const auto* const ref = boost::get<MAG::Out::VariableReference>(&fn_call->arguments[0]);
                    BOOST_REQUIRE(ref);
                    BOOST_CHECK(!ref->index);
                    BOOST_CHECK(!ref->struct_name);
                    BOOST_CHECK_EQUAL(ref->variable_name, "c");
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[1]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
                }
                {
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[2]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, hosts[2].get_name());
                }
            }
        }
    }
    { // H2
        const auto it = asts.find(hosts[2]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<bool>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const if_statement = boost::get<MAG::Out::IfStatement>(&statements[1]);
            BOOST_REQUIRE(if_statement);
            {
                const auto* const ref = boost::get<MAG::Out::VariableReference>(&if_statement->condition);
                BOOST_REQUIRE(ref);
                BOOST_CHECK(!ref->index);
                BOOST_CHECK(!ref->struct_name);
                BOOST_CHECK_EQUAL(ref->variable_name, "a");
            }
            BOOST_REQUIRE(if_statement->then_statements.empty());
            BOOST_REQUIRE_EQUAL(if_statement->else_statements.size(), 2);
            {
                const auto* const assignment = boost::get<MAG::Out::Assignment>(&if_statement->else_statements[0]);
                BOOST_REQUIRE(assignment);
                BOOST_REQUIRE(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
                {
                    const auto* const decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                    BOOST_REQUIRE(decl);
                    BOOST_CHECK_EQUAL(decl->type, "auto");
                    BOOST_CHECK_EQUAL(decl->name, "e");
                }
                {
                    const auto* const fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                    BOOST_REQUIRE(fn_call);
                    BOOST_CHECK_EQUAL(fn_call->function_name, "perform_arithmetic_protocol_final<Add, int>");
                    BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 2);
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[0]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, hosts[0].get_name());
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[1]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, hosts[1].get_name());
                    }
                }
            }
            {
                const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&if_statement->else_statements[1]);
                BOOST_REQUIRE(fn_call);
                BOOST_CHECK_EQUAL(fn_call->function_name, "write");
                BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 2);
                {
                    const auto* const ref = boost::get<MAG::Out::VariableReference>(&fn_call->arguments[0]);
                    BOOST_REQUIRE(ref);
                    BOOST_CHECK(!ref->index);
                    BOOST_CHECK(!ref->struct_name);
                    BOOST_CHECK_EQUAL(ref->variable_name, "e");
                }
                {
                    const auto* const init_list = boost::get<MAG::Out::InitializerList>(&fn_call->arguments[1]);
                    BOOST_REQUIRE(init_list);
                    BOOST_CHECK_EQUAL(init_list->expressions.size(), 1);
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&init_list->expressions[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, "Client");
                }
            }
        }
    }
    { // H2
        const auto it = asts.find(hosts[3]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "d");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statements[1]);
            BOOST_REQUIRE(fn_call);
            BOOST_CHECK_EQUAL(fn_call->function_name, "write");
            BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 2);
            {
                const auto* const ref = boost::get<MAG::Out::VariableReference>(&fn_call->arguments[0]);
                BOOST_REQUIRE(ref);
                BOOST_CHECK(!ref->index);
                BOOST_CHECK(!ref->struct_name);
                BOOST_CHECK_EQUAL(ref->variable_name, "d");
            }
            {
                const auto* const init_list = boost::get<MAG::Out::InitializerList>(&fn_call->arguments[1]);
                BOOST_REQUIRE(init_list);
                BOOST_CHECK_EQUAL(init_list->expressions.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&init_list->expressions[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(distributed_condition_in_for_statement_results_in_expected_asts)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 100}, MAO::Host{"H2", 300}};

    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var x int, var n int;
        for (var i int = 0; x > a[i]; i = i + 1) {
            n = n * 2;
        }
        send n;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto* const a = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "a";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    const auto* const x = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "x";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(x);
    const auto* const n = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "n";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(n);
    const auto* const i = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "i";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(i);
    const auto* const add = MAL::AST::QueryVisitor<const MAL::AST::AdditionExpression::Element>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(add);
    const auto* const mul =
        MAL::AST::QueryVisitor<const MAL::AST::MultiplicationExpression::Element>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(mul);
    const auto* const cmp =
        MAL::AST::QueryVisitor<const MAL::AST::ComparisonExpression::Element>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(cmp);
    const auto* const lit0 = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{
        [](const MAL::AST::NumericLiteral& num_lit) noexcept -> bool {
            const auto* const int_lit = boost::get<int>(&num_lit);
            return int_lit && *int_lit == 0;
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(lit0);
    const auto* const lit1 = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{
        [](const MAL::AST::NumericLiteral& num_lit) noexcept -> bool {
            const auto* const int_lit = boost::get<int>(&num_lit);
            return int_lit && *int_lit == 1;
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(lit1);
    const auto* const lit2 = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{
        [](const MAL::AST::NumericLiteral& num_lit) noexcept -> bool {
            const auto* const int_lit = boost::get<int>(&num_lit);
            return int_lit && *int_lit == 2;
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(lit2);
    const MAO::Mapping mapping{
        {*a, {hosts[0]}},
        {*x, {hosts[1]}},
        {*n, {hosts[2]}},
        {*i, {hosts[0]}},
        {*add, {hosts[0]}},
        {*mul, {hosts[2]}},
        {*cmp, {hosts[0], hosts[1], hosts[2]}},
        {*lit0, {hosts[0]}},
        {*lit1, {hosts[0]}},
        {*lit2, {hosts[2]}},
    };

    const auto asts = MAG::Application::generate(mapping, app_ast);

    BOOST_REQUIRE_EQUAL(asts.size(), 3);
    { // H0
        const auto it = asts.find(hosts[0]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "a");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[1]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "i");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const lit = boost::get<MAG::Out::Literal>(&assignment->rvalue);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "0");
            }
        }
        {
            const auto* const while_statement = boost::get<MAG::Out::WhileStatement>(&statements[2]);
            BOOST_REQUIRE(while_statement);
            {
                const auto* const lambda_call = boost::get<MAG::Out::LambdaCall>(&while_statement->condition);
                BOOST_REQUIRE(lambda_call);
                BOOST_CHECK(lambda_call->arguments.empty());
                BOOST_CHECK(lambda_call->lambda.arguments.empty());
                BOOST_REQUIRE_EQUAL(lambda_call->lambda.capture.size(), 1);
                {
                    const auto* const lit = boost::get<MAG::Out::Literal>(&lambda_call->lambda.capture[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, "&");
                }
                BOOST_REQUIRE_EQUAL(lambda_call->lambda.body.size(), 2);
                {
                    const auto* const void_fn_call =
                        boost::get<MAG::Out::VoidFunctionCall>(&lambda_call->lambda.body[0]);
                    BOOST_REQUIRE(void_fn_call);
                    BOOST_CHECK_EQUAL(void_fn_call->function_name, "perform_comparison_protocol_intermediate<int>");
                    BOOST_REQUIRE_EQUAL(void_fn_call->arguments.size(), 3);
                    {
                        const auto* const var_ref =
                            boost::get<MAG::Out::VariableReference>(&void_fn_call->arguments[0]);
                        BOOST_REQUIRE(var_ref);
                        BOOST_CHECK(!var_ref->struct_name);
                        BOOST_CHECK_EQUAL(var_ref->variable_name, "a");
                        BOOST_REQUIRE(var_ref->index);
                        {
                            const auto* const i = boost::get<MAG::Out::VariableReference>(&*var_ref->index);
                            BOOST_CHECK(!i->index);
                            BOOST_CHECK(!i->struct_name);
                            BOOST_CHECK_EQUAL(i->variable_name, "i");
                        }
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&void_fn_call->arguments[1]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H1");
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&void_fn_call->arguments[2]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H2");
                    }
                }
                {
                    const auto* const ret = boost::get<MAG::Out::Return>(&lambda_call->lambda.body[1]);
                    BOOST_REQUIRE(ret);
                    const auto* const fn_call = boost::get<MAG::Out::FunctionCall>(&ret->expression);
                    BOOST_REQUIRE(fn_call);
                    BOOST_CHECK_EQUAL(fn_call->function_name, "read<bool>");
                    BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 1);
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, "H2");
                }
            }
            BOOST_REQUIRE_EQUAL(while_statement->body.size(), 1);
            {
                const auto* const assignment = boost::get<MAG::Out::Assignment>(&while_statement->body[0]);
                BOOST_REQUIRE(assignment);
                {
                    const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&assignment->lvalue);
                    BOOST_REQUIRE(var_ref);
                    BOOST_CHECK(!var_ref->struct_name);
                    BOOST_CHECK(!var_ref->index);
                    BOOST_CHECK_EQUAL(var_ref->variable_name, "i");
                }
                BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
                {
                    const auto* const bin_op = boost::get<MAG::Out::BinaryOperation>(&assignment->rvalue);
                    BOOST_REQUIRE(bin_op);
                    {
                        const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&bin_op->left_side);
                        BOOST_REQUIRE(var_ref);
                        BOOST_CHECK(!var_ref->struct_name);
                        BOOST_CHECK(!var_ref->index);
                        BOOST_CHECK_EQUAL(var_ref->variable_name, "i");
                    }
                    BOOST_CHECK(bin_op->binary_operator == MAG::Out::BinaryOperator::Plus);
                    {
                        const auto* const lit = boost::get<MAG::Out::Literal>(&bin_op->right_side);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "1");
                    }
                }
            }
        }
    }
    { // H1
        const auto it = asts.find(hosts[1]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "x");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const while_statement = boost::get<MAG::Out::WhileStatement>(&statements[1]);
            BOOST_REQUIRE(while_statement);
            {
                const auto* const lambda_call = boost::get<MAG::Out::LambdaCall>(&while_statement->condition);
                BOOST_REQUIRE(lambda_call);
                BOOST_CHECK(lambda_call->arguments.empty());
                BOOST_CHECK(lambda_call->lambda.arguments.empty());
                BOOST_REQUIRE_EQUAL(lambda_call->lambda.capture.size(), 1);
                {
                    const auto* const lit = boost::get<MAG::Out::Literal>(&lambda_call->lambda.capture[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, "&");
                }
                BOOST_REQUIRE_EQUAL(lambda_call->lambda.body.size(), 2);
                {
                    const auto* const void_fn_call =
                        boost::get<MAG::Out::VoidFunctionCall>(&lambda_call->lambda.body[0]);
                    BOOST_REQUIRE(void_fn_call);
                    BOOST_CHECK_EQUAL(void_fn_call->function_name, "perform_comparison_protocol_initial<int>");
                    BOOST_REQUIRE_EQUAL(void_fn_call->arguments.size(), 3);
                    {
                        const auto* const var_ref =
                            boost::get<MAG::Out::VariableReference>(&void_fn_call->arguments[0]);
                        BOOST_REQUIRE(var_ref);
                        BOOST_CHECK(!var_ref->struct_name);
                        BOOST_CHECK(!var_ref->index);
                        BOOST_CHECK_EQUAL(var_ref->variable_name, "x");
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&void_fn_call->arguments[1]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H0");
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&void_fn_call->arguments[2]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H2");
                    }
                }
                {
                    const auto* const ret = boost::get<MAG::Out::Return>(&lambda_call->lambda.body[1]);
                    BOOST_REQUIRE(ret);
                    const auto* const fn_call = boost::get<MAG::Out::FunctionCall>(&ret->expression);
                    BOOST_REQUIRE(fn_call);
                    BOOST_CHECK_EQUAL(fn_call->function_name, "read<bool>");
                    BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 1);
                    const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[0]);
                    BOOST_REQUIRE(lit);
                    BOOST_CHECK_EQUAL(lit->value, "H2");
                }
            }
            BOOST_REQUIRE(while_statement->body.empty());
        }
    }
    { // H2
        const auto it = asts.find(hosts[2]);
        BOOST_REQUIRE(it != asts.cend());
        const auto& statements = it->second.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        {
            const auto* const assignment = boost::get<MAG::Out::Assignment>(&statements[0]);
            BOOST_REQUIRE(assignment);
            {
                const auto* const var_decl = boost::get<MAG::Out::VariableDeclaration>(&assignment->lvalue);
                BOOST_REQUIRE(var_decl);
                BOOST_CHECK_EQUAL(var_decl->type, "auto");
                BOOST_CHECK_EQUAL(var_decl->name, "n");
            }
            BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
            {
                const auto* const read_fn_call = boost::get<MAG::Out::FunctionCall>(&assignment->rvalue);
                BOOST_REQUIRE(read_fn_call);
                BOOST_CHECK_EQUAL(read_fn_call->function_name, "read<int>");
                BOOST_REQUIRE_EQUAL(read_fn_call->arguments.size(), 1);
                const auto* const lit = boost::get<MAG::Out::StringLiteral>(&read_fn_call->arguments[0]);
                BOOST_REQUIRE(lit);
                BOOST_CHECK_EQUAL(lit->value, "Client");
            }
        }
        {
            const auto* const while_statement = boost::get<MAG::Out::WhileStatement>(&statements[1]);
            BOOST_REQUIRE(while_statement);
            {
                const auto* const write_and_ret_call = boost::get<MAG::Out::FunctionCall>(&while_statement->condition);
                BOOST_REQUIRE(write_and_ret_call);
                BOOST_CHECK_EQUAL(write_and_ret_call->function_name, "write_and_return");
                BOOST_REQUIRE_EQUAL(write_and_ret_call->arguments.size(), 2);
                {
                    const auto* const fn_call = boost::get<MAG::Out::FunctionCall>(&write_and_ret_call->arguments[0]);
                    BOOST_REQUIRE(fn_call);
                    BOOST_CHECK_EQUAL(fn_call->function_name, "perform_comparison_protocol_final<Greater, int>");
                    BOOST_REQUIRE_EQUAL(fn_call->arguments.size(), 2);
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[0]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H1");
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&fn_call->arguments[1]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H0");
                    }
                }
                {
                    const auto* const list = boost::get<MAG::Out::InitializerList>(&write_and_ret_call->arguments[1]);
                    BOOST_REQUIRE(list);
                    BOOST_REQUIRE_EQUAL(list->expressions.size(), 2);
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&list->expressions[0]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H0");
                    }
                    {
                        const auto* const lit = boost::get<MAG::Out::StringLiteral>(&list->expressions[1]);
                        BOOST_REQUIRE(lit);
                        BOOST_CHECK_EQUAL(lit->value, "H1");
                    }
                }
                BOOST_REQUIRE_EQUAL(while_statement->body.size(), 1);
                {
                    const auto* const assignment = boost::get<MAG::Out::Assignment>(&while_statement->body[0]);
                    BOOST_REQUIRE(assignment);
                    {
                        const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&assignment->lvalue);
                        BOOST_REQUIRE(var_ref);
                        BOOST_CHECK(!var_ref->struct_name);
                        BOOST_CHECK(!var_ref->index);
                        BOOST_CHECK_EQUAL(var_ref->variable_name, "n");
                    }
                    BOOST_CHECK(assignment->assignment_operator == MAG::Out::AssignmentOperator::Equals);
                    {
                        const auto* const bin_op = boost::get<MAG::Out::BinaryOperation>(&assignment->rvalue);
                        BOOST_REQUIRE(bin_op);
                        {
                            const auto* const var_ref = boost::get<MAG::Out::VariableReference>(&bin_op->left_side);
                            BOOST_REQUIRE(var_ref);
                            BOOST_CHECK(!var_ref->struct_name);
                            BOOST_CHECK(!var_ref->index);
                            BOOST_CHECK_EQUAL(var_ref->variable_name, "n");
                        }
                        BOOST_CHECK(bin_op->binary_operator == MAG::Out::BinaryOperator::Multiply);
                        {
                            const auto* const lit = boost::get<MAG::Out::Literal>(&bin_op->right_side);
                            BOOST_REQUIRE(lit);
                            BOOST_CHECK_EQUAL(lit->value, "2");
                        }
                    }
                }
            }
        }
    }
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
