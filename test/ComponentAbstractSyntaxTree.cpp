#include <boost/test/unit_test.hpp>

#include "out/ComponentAbstractSyntaxTree.hpp"

namespace {

[[nodiscard]] bool is_void_function_call_with_name(const MAG::Out::Statement& statement,
                                                   std::string_view name) noexcept;

}

BOOST_AUTO_TEST_SUITE(component_abstract_syntax_tree);

BOOST_AUTO_TEST_CASE(adding_two_statements_results_in_expected_contents)
{
    MAG::Out::ComponentAbstractSyntaxTree ast;
    ast.add(MAG::Out::VoidFunctionCall{"f", {}});
    ast += MAG::Out::VoidFunctionCall{"g", {}};

    BOOST_REQUIRE_EQUAL(ast.get_statements().size(), 2);

    {
        const auto* const f_call = boost::get<MAG::Out::VoidFunctionCall>(&ast.get_statements()[0]);
        BOOST_REQUIRE(f_call);
        BOOST_CHECK_EQUAL(f_call->function_name, "f");
        BOOST_CHECK(f_call->arguments.empty());
    }
    {
        const auto* const g_call = boost::get<MAG::Out::VoidFunctionCall>(&ast.get_statements()[1]);
        BOOST_REQUIRE(g_call);
        BOOST_CHECK_EQUAL(g_call->function_name, "g");
        BOOST_CHECK(g_call->arguments.empty());
    }
}

BOOST_AUTO_TEST_CASE(addition_of_two_ast_maps_results_in_expected_contents)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 200}, MAO::Host{"H2", 300}};

    const MAG::Out::ComponentAbstractSyntaxTreeMap ast_map1{
        {hosts[0],
         MAG::Out::ComponentAbstractSyntaxTree{MAG::Out::VoidFunctionCall{"a", {}},
                                               MAG::Out::VoidFunctionCall{"b", {}}}},
        {hosts[1], MAG::Out::ComponentAbstractSyntaxTree{MAG::Out::VoidFunctionCall{"c", {}}}},
        {hosts[2], MAG::Out::ComponentAbstractSyntaxTree{MAG::Out::VoidFunctionCall{"d", {}}}},
    };

    const MAG::Out::ComponentAbstractSyntaxTreeMap ast_map2{
        {
            hosts[1],
            MAG::Out::ComponentAbstractSyntaxTree{MAG::Out::VoidFunctionCall{"e", {}},
                                                  MAG::Out::VoidFunctionCall{"f", {}}},
        },
        {hosts[0], MAG::Out::ComponentAbstractSyntaxTree{MAG::Out::VoidFunctionCall{"g", {}}}},
        {hosts[2], MAG::Out::ComponentAbstractSyntaxTree{MAG::Out::VoidFunctionCall{"h", {}}}},
    };

    const auto ast_map_result = ast_map1 + ast_map2;

    BOOST_REQUIRE_EQUAL(ast_map_result.size(), 3);
    {
        const auto it = ast_map_result.find(hosts[0]);
        BOOST_REQUIRE(it != ast_map_result.end());
        const auto& ast        = it->second;
        const auto& statements = ast.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        BOOST_CHECK(is_void_function_call_with_name(statements[0], "a"));
        BOOST_CHECK(is_void_function_call_with_name(statements[1], "b"));
        BOOST_CHECK(is_void_function_call_with_name(statements[2], "g"));
    }
    {
        const auto it = ast_map_result.find(hosts[1]);
        BOOST_REQUIRE(it != ast_map_result.end());
        const auto& ast        = it->second;
        const auto& statements = ast.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 3);
        BOOST_CHECK(is_void_function_call_with_name(statements[0], "c"));
        BOOST_CHECK(is_void_function_call_with_name(statements[1], "e"));
        BOOST_CHECK(is_void_function_call_with_name(statements[2], "f"));
    }
    {
        const auto it = ast_map_result.find(hosts[2]);
        BOOST_REQUIRE(it != ast_map_result.end());
        const auto& ast        = it->second;
        const auto& statements = ast.get_statements();
        BOOST_REQUIRE_EQUAL(statements.size(), 2);
        BOOST_CHECK(is_void_function_call_with_name(statements[0], "d"));
        BOOST_CHECK(is_void_function_call_with_name(statements[1], "h"));
    }
}

BOOST_AUTO_TEST_SUITE_END();

namespace {

bool is_void_function_call_with_name(const MAG::Out::Statement& statement, std::string_view name) noexcept
{
    const auto* const void_fn_call = boost::get<MAG::Out::VoidFunctionCall>(&statement);
    if (!void_fn_call) { return false; }
    return void_fn_call->function_name == name;
}

}
