#include <boost/test/unit_test.hpp>

#include "application/ExpressionVisitor.hpp"

BOOST_AUTO_TEST_SUITE(application);

BOOST_AUTO_TEST_SUITE(primary_expression_visitor);

BOOST_AUTO_TEST_CASE(len_function_call_results_in_len_variable_reference)
{
    const MAO::HostVector hosts{MAO::Host{"H0", 100}, MAO::Host{"H1", 200}};

    static constexpr std::string_view app_mal_code{R"(
        receive var a int[];
        var b int = len(a);
        send b;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto* const a = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "a";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    const auto* const b = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> bool {
            return var_decl.variable_name == "b";
        }}.apply_to_all(app_ast);
    BOOST_REQUIRE(b);
    const auto* const len_fn_call = MAL::AST::QueryVisitor<const MAL::AST::LenFunctionCall>{}.apply_to_all(app_ast);
    BOOST_REQUIRE(len_fn_call);
    const MAO::Mapping mapping{
        {*a, {hosts[0]}},
        {*b, {hosts[1]}},
        {MAO::ArrayVariableLength{*len_fn_call}, {hosts[0], hosts[1]}},
    };

    const auto [asts, partial_expressions] =
        MAG::Application::PrimaryExpressionVisitor{{hosts[0], hosts[1]}, mapping}(*len_fn_call);

    BOOST_CHECK(asts.empty());
    BOOST_REQUIRE_EQUAL(partial_expressions.size(), 2);

    for (const auto& host : hosts) {
        const auto it = partial_expressions.find(host);
        BOOST_REQUIRE(it != partial_expressions.end());
        const auto& partial_expression = it->second;
        const auto* const len_var_ref  = boost::get<MAG::Out::VariableReference>(&partial_expression);
        BOOST_REQUIRE(len_var_ref);
        BOOST_REQUIRE(!len_var_ref->struct_name);
        BOOST_REQUIRE(!len_var_ref->index);
        BOOST_REQUIRE_EQUAL(len_var_ref->variable_name,
                            a->variable_name +
                                std::string{MAG::Application::PrimaryExpressionVisitor::len_variable_sufix});
    }
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
