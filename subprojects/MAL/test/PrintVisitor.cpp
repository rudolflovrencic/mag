#include <boost/test/unit_test.hpp>

#include "MAL/PrintVisitor.hpp"
#include "MAL/Parsing.hpp"

#include <sstream>

BOOST_AUTO_TEST_SUITE(print_visitor);

BOOST_AUTO_TEST_CASE(assignment_results_in_expected_output)
{
    static constexpr std::string_view input{"var x int = 4 + 5;"};

    const auto ast = MAL::parse(input);

    std::ostringstream out_stream;
    MAL::AST::PrintVisitor{out_stream}.apply_to_all(ast);

    static constexpr std::string_view expected{"Assignment\n"
                                               "\tVariableDeclaration int\n"
                                               "\t\tVariableIdentifier x\n"
                                               "\tAssignmentOperator =\n"
                                               "\tBooleanOrExpression\n"
                                               "\t\tBooleanAndExpression\n"
                                               "\t\t\tComparisonExpression\n"
                                               "\t\t\t\tAdditionExpression\n"
                                               "\t\t\t\t\tMultiplicationExpression\n"
                                               "\t\t\t\t\t\tPrefixExpression\n"
                                               "\t\t\t\t\t\t\tNumericLiteral int 4\n"
                                               "\t\t\t\t\tAdditionOperator +\n"
                                               "\t\t\t\t\tMultiplicationExpression\n"
                                               "\t\t\t\t\t\tPrefixExpression\n"
                                               "\t\t\t\t\t\t\tNumericLiteral int 5\n"};

    BOOST_CHECK_EQUAL(out_stream.str(), expected);
}

BOOST_AUTO_TEST_CASE(send_and_receive_result_in_expected_output)
{
    static constexpr std::string_view input{"send x, y;\n"
                                            "receive var res int;"};

    const auto ast = MAL::parse(input);

    std::ostringstream out_stream;
    MAL::AST::PrintVisitor{out_stream}.apply_to_all(ast);

    static constexpr std::string_view expected{"Send\n"
                                               "\tVariableReference\n"
                                               "\t\tVariableIdentifier x\n"
                                               "\tVariableReference\n"
                                               "\t\tVariableIdentifier y\n"
                                               "Receive\n"
                                               "\tVariableDeclaration int\n"
                                               "\t\tVariableIdentifier res\n"};

    BOOST_CHECK_EQUAL(out_stream.str(), expected);
}

BOOST_AUTO_TEST_SUITE_END();
