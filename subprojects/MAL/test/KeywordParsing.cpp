#include <boost/test/unit_test.hpp>

#include "MAL/parser/Configuration.hpp"
#include "MAL/parser/Keyword.hpp"

#include "Common.hpp"

BOOST_AUTO_TEST_SUITE(keyword_parsing);

BOOST_AUTO_TEST_CASE(type_keyword_produces_expected_results)
{
    const auto& rule = MAL::get_type_keyword_rule();

    {
        const MAL::AST::TypeKeyword tk{parse("int", rule)};
        BOOST_CHECK(tk == MAL::AST::TypeKeyword::Int);
    }
    {
        const MAL::AST::TypeKeyword tk{parse("float", rule)};
        BOOST_CHECK(tk == MAL::AST::TypeKeyword::Float);
    }
    {
        const MAL::AST::TypeKeyword tk{parse("bool", rule)};
        BOOST_CHECK(tk == MAL::AST::TypeKeyword::Bool);
    }
    {
        BOOST_CHECK_EXCEPTION(
            [[maybe_unused]] const MAL::AST::TypeKeyword tk{parse("boolean", rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "boolean"; });
    }
}

BOOST_AUTO_TEST_CASE(control_keyword_produces_expected_results)
{
    const auto& rule = MAL::get_main_keyword_rule();
    {
        const MAL::AST::MainKeyword mk{parse("for", rule)};
        BOOST_CHECK(mk == MAL::AST::MainKeyword::For);
    }
    {
        const MAL::AST::MainKeyword mk{parse("if", rule)};
        BOOST_CHECK(mk == MAL::AST::MainKeyword::If);
    }
    {
        const MAL::AST::MainKeyword mk{parse("else", rule)};
        BOOST_CHECK(mk == MAL::AST::MainKeyword::Else);
    }
    {
        BOOST_CHECK_EXCEPTION(
            [[maybe_unused]] const MAL::AST::MainKeyword mk{parse("forif", rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "forif"; });
    }
}

BOOST_AUTO_TEST_SUITE_END();
