#include <boost/test/unit_test.hpp>

#include "MAL/parser/Configuration.hpp"
#include "MAL/parser/Operator.hpp"

#include "Common.hpp"

BOOST_AUTO_TEST_SUITE(operator_parsing);

BOOST_AUTO_TEST_CASE(prefix_operator_produces_expected_results)
{
    const auto& rule = MAL::get_prefix_operator_rule();
    BOOST_CHECK(parse(" !", rule) == MAL::AST::PrefixOperator::Not);

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::PrefixOperator op{parse(" +", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "+"; });

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::PrefixOperator op{parse(" -!", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "-!"; });
}

BOOST_AUTO_TEST_CASE(multiplication_operator_produces_expected_results)
{
    const auto& rule = MAL::get_multiplication_operator_rule();
    BOOST_CHECK(parse(" / ", rule) == MAL::AST::MultiplicationOperator::Divide);
    BOOST_CHECK(parse(" *", rule) == MAL::AST::MultiplicationOperator::Multiply);

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::MultiplicationOperator op{parse(" x42", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "x42"; });
}

BOOST_AUTO_TEST_CASE(addition_operator_produces_expected_results)
{
    const auto& rule = MAL::get_addition_operator_rule();
    BOOST_CHECK(parse(" + ", rule) == MAL::AST::AdditionOperator::Plus);
    BOOST_CHECK(parse("-  ", rule) == MAL::AST::AdditionOperator::Minus);

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::AdditionOperator op{parse(" +! ", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "! "; });
}

BOOST_AUTO_TEST_CASE(comparison_operator_produces_expected_results)
{
    const auto& rule = MAL::get_comparison_operator_rule();
    BOOST_CHECK(parse(" ==", rule) == MAL::AST::ComparisonOperator::Equal);
    BOOST_CHECK(parse("!=", rule) == MAL::AST::ComparisonOperator::NotEqual);
    BOOST_CHECK(parse("<", rule) == MAL::AST::ComparisonOperator::Less);
    BOOST_CHECK(parse("  > ", rule) == MAL::AST::ComparisonOperator::Greater);
    BOOST_CHECK(parse("<= ", rule) == MAL::AST::ComparisonOperator::LessOrEqual);
    BOOST_CHECK(parse(" >=", rule) == MAL::AST::ComparisonOperator::GreaterOrEqual);

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::ComparisonOperator op{parse("!>=", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "!>="; });

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::ComparisonOperator op{parse("<=!a", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "!a"; });
}

BOOST_AUTO_TEST_CASE(logical_operator_produces_expected_results)
{
    const auto& rule = MAL::get_logical_operator_rule();
    BOOST_CHECK(parse(" && ", rule) == MAL::AST::LogicalOperator::And);
    BOOST_CHECK(parse("||", rule) == MAL::AST::LogicalOperator::Or);

    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::LogicalOperator op{parse("&", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "&"; });
    BOOST_CHECK_EXCEPTION([[maybe_unused]] const MAL::AST::LogicalOperator op{parse("  ||abc", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "abc"; });
}

BOOST_AUTO_TEST_SUITE_END();
