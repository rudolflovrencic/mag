#include <boost/test/unit_test.hpp>

#include "MAL/Parsing.hpp"
#include "MAL/QueryVisitor.hpp"
#include "MAL/PrintVisitor.hpp"

#include "MAL/semantic/Declaration.hpp"
#include "MAL/semantic/Expression.hpp"

BOOST_AUTO_TEST_SUITE(variable_pairing);

BOOST_AUTO_TEST_CASE(simple_code_result_in_successful_declaration_pairing)
{
    static constexpr std::string_view app_mal_code{R"(
        var a int = 18;
        var b int = 42;
        var c int = a + b; 
    )"};

    auto app_ast = MAL::parse(app_mal_code);
    MAL::pair_identifiers_and_declarations(app_ast);

    const auto* const a =
        MAL::AST::QueryVisitor<MAL::AST::VariableReference>([](const MAL::AST::VariableReference& vr) noexcept -> bool {
            return vr.variable_identifier == "a";
        }).apply_to_all(app_ast);
    BOOST_REQUIRE(a);
    BOOST_REQUIRE(a->variable_identifier.declaration);
    const auto& a_decl = *a->variable_identifier.declaration;
    BOOST_REQUIRE(!a_decl.is_array_declaration);
    BOOST_CHECK(a_decl.type == MAL::AST::TypeKeyword::Int);

    const auto* const b =
        MAL::AST::QueryVisitor<MAL::AST::VariableReference>([](const MAL::AST::VariableReference& vr) noexcept -> bool {
            return vr.variable_identifier == "b";
        }).apply_to_all(app_ast);
    BOOST_REQUIRE(b);
    BOOST_REQUIRE(b->variable_identifier.declaration);
    const auto& b_decl = *b->variable_identifier.declaration;
    BOOST_REQUIRE(!b_decl.is_array_declaration);
    BOOST_CHECK(b_decl.type == MAL::AST::TypeKeyword::Int);
}

BOOST_AUTO_TEST_CASE(missing_variable_declaration_results_in_an_error)
{
    static constexpr std::string_view app_mal_code{R"(
        var a int = 18;
        var b int = 42;
        var c int = a + x + b ; 
    )"};

    auto app_ast = MAL::parse(app_mal_code);
    BOOST_CHECK_EXCEPTION(
        MAL::pair_identifiers_and_declarations(app_ast),
        MAL::MissingVariableDeclarationError,
        [](const MAL::MissingVariableDeclarationError& err) -> bool { return err.identifier == "x"; });
}

BOOST_AUTO_TEST_CASE(variable_redeclaration_results_in_an_error)
{
    static constexpr std::string_view app_mal_code{R"(
        var a int = 18;
        var b int = 42;
        if (b) {
            var a float = 48;
        } else {
            var c int = 22;
        }
    )"};

    auto app_ast = MAL::parse(app_mal_code);
    BOOST_CHECK_EXCEPTION(MAL::pair_identifiers_and_declarations(app_ast),
                          MAL::VariableRedeclaredError,
                          [](const MAL::VariableRedeclaredError& err) noexcept -> bool {
                              return err.first_declaration.variable_name == "a" &&
                                     err.second_declaration.variable_name == "a";
                          });
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(determining_expression_types);

BOOST_AUTO_TEST_CASE(comparison_epxression_results_in_expected_expression_types)
{
    static auto require_not_array_and_of_required_type =
        [](const std::optional<MAL::AST::ExpressionTypeInformation>& type_info,
           MAL::AST::TypeKeyword required_type) noexcept -> void {
        BOOST_REQUIRE(type_info);
        BOOST_CHECK(!type_info->is_array);
        BOOST_CHECK(type_info->type == required_type);
    };

    auto ast = MAL::parse("var a bool = 10 < 20;");
    MAL::pair_identifiers_and_declarations(ast);
    MAL::determine_expression_types(ast);
    const auto* const assignment = boost::get<MAL::AST::Assignment>(&ast.at(0));
    BOOST_REQUIRE(assignment);
    const auto* const bool_or_expr = boost::get<MAL::AST::BooleanOrExpression>(&assignment->right_side);
    BOOST_REQUIRE(bool_or_expr);
    BOOST_CHECK(bool_or_expr->other_expressions.empty());
    require_not_array_and_of_required_type(bool_or_expr->type, MAL::AST::TypeKeyword::Bool);
    const auto& bool_and_expr = bool_or_expr->first_expression;
    BOOST_CHECK(bool_and_expr.other_expressions.empty());
    require_not_array_and_of_required_type(bool_and_expr.type, MAL::AST::TypeKeyword::Bool);
    BOOST_CHECK(bool_and_expr.other_expressions.empty());
    const auto& comp_expr = bool_and_expr.first_expression;
    BOOST_REQUIRE_EQUAL(comp_expr.other_expressions.size(), 1);
    {
        const auto& add_expr = comp_expr.first_expression;
        BOOST_CHECK(add_expr.other_expressions.empty());
        require_not_array_and_of_required_type(add_expr.type, MAL::AST::TypeKeyword::Int);
        const auto& mul_expr = add_expr.first_expression;
        BOOST_CHECK(mul_expr.other_expressions.empty());
        require_not_array_and_of_required_type(mul_expr.type, MAL::AST::TypeKeyword::Int);
        const auto& pref_expr = mul_expr.first_expression;
        BOOST_CHECK(!pref_expr.prefix_operator.has_value());
        const auto& prim_expr = pref_expr.primary_expression;
        require_not_array_and_of_required_type(prim_expr.type, MAL::AST::TypeKeyword::Int);
        const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
        BOOST_REQUIRE(num_lit);
        BOOST_CHECK(num_lit->type.value() == MAL::AST::TypeKeyword::Int);
        const auto* const num_lit_as_int = boost::get<int>(num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 10);
    }
    {
        const auto& add_expr = comp_expr.other_expressions[0].addition_expression;
        BOOST_CHECK(add_expr.other_expressions.empty());
        require_not_array_and_of_required_type(add_expr.type, MAL::AST::TypeKeyword::Int);
        const auto& mul_expr = add_expr.first_expression;
        BOOST_CHECK(mul_expr.other_expressions.empty());
        require_not_array_and_of_required_type(mul_expr.type, MAL::AST::TypeKeyword::Int);
        const auto& pref_expr = mul_expr.first_expression;
        BOOST_CHECK(!pref_expr.prefix_operator);
        const auto& prim_expr = pref_expr.primary_expression;
        require_not_array_and_of_required_type(prim_expr.type, MAL::AST::TypeKeyword::Int);
        const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
        BOOST_CHECK(num_lit->type.value() == MAL::AST::TypeKeyword::Int);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_int = boost::get<int>(num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 20);
    }
}

BOOST_AUTO_TEST_CASE(array_literal_expression_type_determination_produces_expected_results)
{
    {
        auto ast = MAL::parse("receive var x int[]; var y int[] = [x; 20];");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::ArrayTypeAsArrayValueError);
    }
    {
        auto ast = MAL::parse("receive var x int[]; var y int[] = [20; x];");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::ArrayTypeAsArraySizeError);
    }
    {
        auto ast = MAL::parse("var x int[] = [42; 3.14];");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::NonIntegerTypeAsArraySizeError);
    }
    {
        auto ast = MAL::parse("var x bool[] = [true; 20];");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_NO_THROW(MAL::determine_expression_types(ast));
    }
}

BOOST_AUTO_TEST_CASE(subscript_expression_type_determination_produces_expected_results)
{
    {
        auto ast = MAL::parse("var x bool[] = [false; 2];"
                              "var y bool = true || x[0];");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_NO_THROW(MAL::determine_expression_types(ast));
    }
    {
        auto ast = MAL::parse("var x int = 10;"
                              "var y int = x[0];");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::SubscriptOnNonArrayVariableError);
    }
}

BOOST_AUTO_TEST_CASE(non_boolean_expression_in_if_condition_results_in_an_error)
{
    {
        auto ast = MAL::parse(R"(
             if (10 * 42) {
                var a int = 18; 
             } else {
                var b int = 22; 
             }
        )");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_EXCEPTION(MAL::determine_expression_types(ast),
                              MAL::NonBooleanExpressionInIfConditionError,
                              [](const MAL::NonBooleanExpressionInIfConditionError& err) noexcept -> bool {
                                  const auto& type_info = err.if_statement.condition.type;
                                  return type_info && !type_info->is_array &&
                                         type_info->type == MAL::AST::TypeKeyword::Int;
                              });
    }
    {
        auto ast = MAL::parse(R"(
           var x float = 3.14;
           if (x) {
              var a int = 18; 
           } else {
              var b int = 22; 
           }
        )");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_EXCEPTION(MAL::determine_expression_types(ast),
                              MAL::NonBooleanExpressionInIfConditionError,
                              [](const MAL::NonBooleanExpressionInIfConditionError& err) noexcept -> bool {
                                  const auto& type_info = err.if_statement.condition.type;
                                  return type_info && !type_info->is_array &&
                                         type_info->type == MAL::AST::TypeKeyword::Float;
                              });
    }
}

BOOST_AUTO_TEST_CASE(array_as_part_of_the_addition_expression_results_in_an_error)
{
    auto ast = MAL::parse(R"(
        receive var x int[], var y int[];
        var z int[] = x + y;
        send z;
    )");
    MAL::pair_identifiers_and_declarations(ast);
    BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::AdditionExpressionContainsArrayTypeError);
}

BOOST_AUTO_TEST_CASE(len_function_call_on_non_array_type_results_in_an_error)
{
    {
        auto ast = MAL::parse(R"(
            var x int = 42;
            var l int = len(x);
            send l;
        )");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::LenFunctionCallOnNonArrayError);
    }
    {
        auto ast = MAL::parse(R"(
            receive var x int[];
            var l int = len(x[0]);
            send l;
        )");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::LenFunctionCallOnNonArrayError);
    }
}

BOOST_AUTO_TEST_CASE(subscript_on_non_array_type_results_in_an_error)
{
    {
        auto ast = MAL::parse(R"(
            receive var a int, var b int;
            var x int = a[b];
            send x;
        )");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::SubscriptOnNonArrayVariableError);
    }
    {
        auto ast = MAL::parse(R"(
            receive var a int, var b int;
            send a[b];
        )");
        MAL::pair_identifiers_and_declarations(ast);
        BOOST_CHECK_THROW(MAL::determine_expression_types(ast), MAL::SubscriptOnNonArrayVariableError);
    }
}

BOOST_AUTO_TEST_SUITE_END();
