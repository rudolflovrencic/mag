#ifndef TEST_COMMON_HPP
#define TEST_COMMON_HPP

#include "MAL/parser/Configuration.hpp"

#include <stdexcept>
#include <sstream>

// Thrown when parsing the entire input stream has failed. Contains part of the input stream that has not been parsed.
class LeftoverError final : public std::runtime_error {
  public:
    LeftoverError(MAL::Parser::Iterator begin, MAL::Parser::Iterator end) noexcept
            : std::runtime_error{std::string{begin, end}}
    {}

    [[nodiscard]] std::string_view get_leftover_data() const noexcept { return what(); }
};

template<typename Rule>
[[nodiscard]] typename Rule::attribute_type parse(std::string_view input, const Rule& rule)
{
    namespace x3 = boost::spirit::x3;

    MAL::Parser::Iterator begin{input.begin()};
    MAL::Parser::Iterator end{input.end()};

    std::stringstream error_stream;
    MAL::Parser::ErrorHandler error_handler{begin, end, error_stream};
    const auto parser = x3::with<MAL::Parser::ErrorHandlerTag>(std::ref(error_handler))[rule];

    using ExpectationFailure = boost::spirit::x3::expectation_failure<MAL::Parser::Iterator>;
    typename Rule::attribute_type result;
    try {
        const bool r{x3::phrase_parse(begin, end, parser, x3::space, result)};
        if (r && begin == end) {
            return result;
        } else { // Occurs when the whole input stream has not been consumed.
            throw LeftoverError{begin, end};
        }
    } catch (const ExpectationFailure& exc) {
        throw LeftoverError{exc.where(), end};
    }
}

#endif
