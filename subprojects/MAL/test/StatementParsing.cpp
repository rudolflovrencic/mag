#include <boost/test/unit_test.hpp>

#include "MAL/parser/Configuration.hpp"
#include "MAL/parser/Statement.hpp"

#include "Common.hpp"

BOOST_AUTO_TEST_SUITE(statement_parsing);

BOOST_AUTO_TEST_CASE(assignment_produces_expected_results)
{
    const auto& rule = MAL::get_assignment_rule();
    const MAL::AST::Assignment a{parse("var x int = 42", rule)};

    const auto* const decl = boost::get<MAL::AST::VariableDeclaration>(&a.left_side);
    BOOST_REQUIRE(decl);
    BOOST_CHECK_EQUAL(decl->variable_name, "x");
    BOOST_CHECK(decl->type == MAL::AST::TypeKeyword::Int);

    const auto* const or_expr = boost::get<MAL::AST::BooleanOrExpression>(&a.right_side);
    BOOST_REQUIRE(or_expr);
    BOOST_CHECK(or_expr->other_expressions.empty());

    const auto& and_expr = or_expr->first_expression;
    BOOST_CHECK(and_expr.other_expressions.empty());

    const auto& comp_expr = and_expr.first_expression;
    BOOST_CHECK(comp_expr.other_expressions.empty());

    const auto& add_expr = comp_expr.first_expression;
    BOOST_CHECK(add_expr.other_expressions.empty());

    const auto& mul_expr = add_expr.first_expression;
    BOOST_CHECK(mul_expr.other_expressions.empty());

    const auto& pref_expr = mul_expr.first_expression;
    BOOST_CHECK(!pref_expr.prefix_operator);

    const auto& prim_expr     = pref_expr.primary_expression;
    const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
    BOOST_REQUIRE(num_lit);
    const auto* const num_lit_as_int = boost::get<int>(num_lit);
    BOOST_REQUIRE(num_lit_as_int);
    BOOST_CHECK_EQUAL(*num_lit_as_int, 42);
}

BOOST_AUTO_TEST_CASE(send_produces_expected_results)
{
    const auto& rule = MAL::get_send_rule();

    {
        const MAL::AST::Send s{parse("send x", rule)};

        BOOST_REQUIRE_EQUAL(s.size(), 1);
        BOOST_CHECK(!s[0].index);
        BOOST_CHECK(!s[0].type);
        BOOST_CHECK_EQUAL(s[0].variable_identifier, "x");
    }
    {
        const MAL::AST::Send s{parse("send x, y, z", rule)};

        BOOST_REQUIRE_EQUAL(s.size(), 3);

        BOOST_CHECK(!s[0].index);
        BOOST_CHECK(!s[0].type);
        BOOST_CHECK_EQUAL(s[0].variable_identifier, "x");

        BOOST_CHECK(!s[1].index);
        BOOST_CHECK(!s[1].type);
        BOOST_CHECK_EQUAL(s[1].variable_identifier, "y");

        BOOST_CHECK(!s[2].index);
        BOOST_CHECK(!s[2].type);
        BOOST_CHECK_EQUAL(s[2].variable_identifier, "z");
    }

    BOOST_CHECK_EXCEPTION(const MAL::AST::Send s{parse("send x y, z", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "y, z"; });

    BOOST_CHECK_EXCEPTION(const MAL::AST::Send s{parse("send", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data().empty(); });

    BOOST_CHECK_EXCEPTION(
        const MAL::AST::Send s{parse("var x int = 42", rule)},
        LeftoverError,
        [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "var x int = 42"; });
}

BOOST_AUTO_TEST_CASE(receive_produces_expected_results)
{
    const auto& rule = MAL::get_receive_rule();

    {
        const MAL::AST::Receive r{parse("receive var x int", rule)};

        BOOST_REQUIRE_EQUAL(r.size(), 1);
        BOOST_CHECK_EQUAL(r[0].variable_name, "x");
        BOOST_CHECK(r[0].type == MAL::AST::TypeKeyword::Int);
    }
    {
        const MAL::AST::Receive r{parse("receive var x float, var y int", rule)};

        BOOST_REQUIRE_EQUAL(r.size(), 2);

        BOOST_CHECK_EQUAL(r[0].variable_name, "x");
        BOOST_CHECK(r[0].type == MAL::AST::TypeKeyword::Float);
        BOOST_CHECK_EQUAL(r[1].variable_name, "y");
        BOOST_CHECK(r[1].type == MAL::AST::TypeKeyword::Int);
    }

    BOOST_CHECK_EXCEPTION(
        const MAL::AST::Receive r{parse("receive x float, var y int", rule)},
        LeftoverError,
        [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == " x float, var y int"; });

    BOOST_CHECK_EXCEPTION(
        const MAL::AST::Receive r{parse("receive var x, var y int", rule)},
        LeftoverError,
        [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == ", var y int"; });

    BOOST_CHECK_EXCEPTION(const MAL::AST::Receive r{parse("receive", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data().empty(); });
}

BOOST_AUTO_TEST_CASE(print_produces_expected_results)
{
    const auto& rule = MAL::get_print_rule();

    {
        const MAL::AST::Print p{parse(R"(print "Result: {}", res)", rule)};

        BOOST_CHECK_EQUAL(p.format_string, "Result: {}");
        BOOST_CHECK_EQUAL(p.identifiers.size(), 1);
        BOOST_CHECK_EQUAL(p.identifiers[0], "res");
    }

    BOOST_CHECK_EXCEPTION(const MAL::AST::Print p{parse("print res", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "res"; });

    BOOST_CHECK_EXCEPTION(const MAL::AST::Print p{parse(R"(rint "Result: {}", res)", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool {
                              return exc.get_leftover_data() == R"(rint "Result: {}", res)";
                          });

    BOOST_CHECK_EXCEPTION(const MAL::AST::Print r{parse("print", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data().empty(); });
}

BOOST_AUTO_TEST_CASE(if_produces_expected_results)
{
    const auto& rule = MAL::get_if_statement_rule();

    {
        static constexpr std::string_view input{R"(
            if (1) {
                var i int = 8;
                var a int = 42;
            }
        )"};

        const MAL::AST::IfStatement is{parse(input, rule)};

        BOOST_CHECK(is.condition.other_expressions.empty());
        BOOST_CHECK(is.condition.first_expression.other_expressions.empty());
        const auto* const num_lit =
            boost::get<MAL::AST::NumericLiteral>(&is.condition.first_expression.first_expression.first_expression
                                                      .first_expression.first_expression.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_int = boost::get<int>(num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 1);

        BOOST_REQUIRE_EQUAL(is.then_statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAL::AST::Assignment>(&is.then_statements[0]);
            BOOST_REQUIRE(assignment);
            const auto* const i_decl = boost::get<MAL::AST::VariableDeclaration>(&assignment->left_side);
            BOOST_REQUIRE(i_decl);
            BOOST_CHECK_EQUAL(i_decl->variable_name, "i");
            BOOST_CHECK(i_decl->type == MAL::AST::TypeKeyword::Int);
        }
        {
            const auto* const assignment = boost::get<MAL::AST::Assignment>(&is.then_statements[1]);
            BOOST_REQUIRE(assignment);
            const auto* const a_decl = boost::get<MAL::AST::VariableDeclaration>(&assignment->left_side);
            BOOST_REQUIRE(a_decl);
            BOOST_CHECK_EQUAL(a_decl->variable_name, "a");
            BOOST_CHECK(a_decl->type == MAL::AST::TypeKeyword::Int);
        }
        BOOST_CHECK(is.else_statements.empty());
    }
    {
        static constexpr std::string_view input{R"(
            if (0) {
                var i int = 8;
                var a int = 42;
            } else {
                var x float = 3.14;
            }
        )"};

        const MAL::AST::IfStatement is{parse(input, rule)};

        BOOST_CHECK(is.condition.other_expressions.empty());
        BOOST_CHECK(is.condition.first_expression.other_expressions.empty());
        const auto* const num_lit =
            boost::get<MAL::AST::NumericLiteral>(&is.condition.first_expression.first_expression.first_expression
                                                      .first_expression.first_expression.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_int = boost::get<int>(num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 0);

        BOOST_REQUIRE_EQUAL(is.then_statements.size(), 2);
        {
            const auto* const assignment = boost::get<MAL::AST::Assignment>(&is.then_statements[0]);
            BOOST_REQUIRE(assignment);
            const auto* const i_decl = boost::get<MAL::AST::VariableDeclaration>(&assignment->left_side);
            BOOST_REQUIRE(i_decl);
            BOOST_CHECK_EQUAL(i_decl->variable_name, "i");
            BOOST_CHECK(i_decl->type == MAL::AST::TypeKeyword::Int);
        }
        {
            const auto* const assignment = boost::get<MAL::AST::Assignment>(&is.then_statements[1]);
            BOOST_REQUIRE(assignment);
            const auto* const a_decl = boost::get<MAL::AST::VariableDeclaration>(&assignment->left_side);
            BOOST_REQUIRE(a_decl);
            BOOST_CHECK_EQUAL(a_decl->variable_name, "a");
            BOOST_CHECK(a_decl->type == MAL::AST::TypeKeyword::Int);
        }
        BOOST_REQUIRE_EQUAL(is.else_statements.size(), 1);
        {
            const auto* const assignment = boost::get<MAL::AST::Assignment>(&is.else_statements[0]);
            BOOST_REQUIRE(assignment);
            const auto* const a_decl = boost::get<MAL::AST::VariableDeclaration>(&assignment->left_side);
            BOOST_REQUIRE(a_decl);
            BOOST_CHECK_EQUAL(a_decl->variable_name, "x");
            BOOST_CHECK(a_decl->type == MAL::AST::TypeKeyword::Float);
        }
    }
    {
        static constexpr std::string_view input = {R"(
            if (1) {
            } else {
            }
        )"};

        const MAL::AST::IfStatement is{parse(input, rule)};

        BOOST_CHECK(is.condition.other_expressions.empty());
        BOOST_CHECK(is.condition.first_expression.other_expressions.empty());
        const auto* const num_lit =
            boost::get<MAL::AST::NumericLiteral>(&is.condition.first_expression.first_expression.first_expression
                                                      .first_expression.first_expression.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_int = boost::get<int>(num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 1);

        BOOST_CHECK(is.then_statements.empty());
        BOOST_CHECK(is.else_statements.empty());
    }

    BOOST_CHECK_EXCEPTION(
        const MAL::AST::IfStatement is{parse("if () { var i int = 8; }", rule)},
        LeftoverError,
        [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == ") { var i int = 8; }"; });
}

BOOST_AUTO_TEST_CASE(for_produces_expected_results)
{
    const auto& rule = MAL::get_for_statement_rule();
    {
        static constexpr std::string_view input{R"(
            for (; true;) {}
        )"};

        const MAL::AST::ForStatement fs{parse(input, rule)};

        BOOST_CHECK(!fs.init_statement);
        BOOST_CHECK(!fs.step_statement);
        BOOST_CHECK(fs.body.empty());

        const auto& or_expr = fs.condition;
        BOOST_CHECK(or_expr.other_expressions.empty());
        const auto& and_expr = or_expr.first_expression;
        BOOST_CHECK(and_expr.other_expressions.empty());
        const auto& cmp_expr = and_expr.first_expression;
        BOOST_CHECK(cmp_expr.other_expressions.empty());
        const auto& add_expr = cmp_expr.first_expression;
        BOOST_CHECK(add_expr.other_expressions.empty());
        const auto& mul_expr = add_expr.first_expression;
        BOOST_CHECK(mul_expr.other_expressions.empty());
        const auto& pref_expr = mul_expr.first_expression;
        BOOST_CHECK(mul_expr.other_expressions.empty());
        BOOST_CHECK(!pref_expr.prefix_operator);
        const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const bool_lit = boost::get<bool>(num_lit);
        BOOST_REQUIRE(bool_lit);
        BOOST_CHECK(*bool_lit);
    }
    {
        static constexpr std::string_view input{R"(
            for (var i int = 0; i < 100; i = i + 1) {
                var x int = i;
            }
        )"};

        const MAL::AST::ForStatement fs{parse(input, rule)};

        {
            BOOST_REQUIRE(fs.init_statement);
            const auto* const var_decl = boost::get<MAL::AST::VariableDeclaration>(&fs.init_statement->left_side);
            BOOST_REQUIRE(var_decl);
            BOOST_CHECK_EQUAL(var_decl->variable_name, "i");
        }

        {
            const auto& or_expr = fs.condition;
            BOOST_CHECK(or_expr.other_expressions.empty());
            const auto& and_expr = or_expr.first_expression;
            BOOST_CHECK(and_expr.other_expressions.empty());
            const auto& cmp_expr = and_expr.first_expression;
            BOOST_REQUIRE_EQUAL(cmp_expr.other_expressions.size(), 1);
            {
                const auto& add_expr = cmp_expr.first_expression;
                BOOST_CHECK(add_expr.other_expressions.empty());
                const auto& mul_expr = add_expr.first_expression;
                BOOST_CHECK(mul_expr.other_expressions.empty());
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                const auto* const var_ref = boost::get<boost::spirit::x3::forward_ast<MAL::AST::VariableReference>>(
                    &pref_expr.primary_expression);
                BOOST_REQUIRE(var_ref);
                BOOST_CHECK_EQUAL(var_ref->get().variable_identifier, "i");
            }
            BOOST_CHECK(cmp_expr.other_expressions[0].comparison_operator == MAL::AST::ComparisonOperator::Less);
            {
                const auto& add_expr = cmp_expr.other_expressions[0].addition_expression;
                BOOST_CHECK(add_expr.other_expressions.empty());
                const auto& mul_expr = add_expr.first_expression;
                BOOST_CHECK(mul_expr.other_expressions.empty());
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
                BOOST_REQUIRE(num_lit);
                const auto* const num_lit_as_int = boost::get<int>(num_lit);
                BOOST_REQUIRE(num_lit_as_int);
                BOOST_CHECK_EQUAL(*num_lit_as_int, 100);
            }
        }

        BOOST_REQUIRE(fs.step_statement);

        BOOST_CHECK_EQUAL(fs.body.size(), 1);
    }
}

BOOST_AUTO_TEST_SUITE_END();
