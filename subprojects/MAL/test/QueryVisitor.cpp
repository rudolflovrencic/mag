#include <boost/test/unit_test.hpp>

#include "MAL/QueryVisitor.hpp"
#include "MAL/Parsing.hpp"

BOOST_AUTO_TEST_SUITE(query_visitor);

BOOST_AUTO_TEST_CASE(numeric_literal_seek_produces_expected_results)
{
    static constexpr std::string_view input{"var x int = 4 + 5;"};

    auto ast = MAL::parse(input);

    { // Non-const node seek.
        auto* const num_lit = MAL::AST::QueryVisitor<MAL::AST::NumericLiteral>{
            [](const MAL::AST::NumericLiteral& num_lit) noexcept -> bool {
                const auto* const num_lit_p = boost::get<int>(&num_lit);
                return num_lit_p && *num_lit_p == 5;
            }}.apply_to_all(ast);
        BOOST_CHECK(num_lit);
    }
    { // Const node seek.
        const auto& const_ast     = ast;
        const auto* const num_lit = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{
            [](const MAL::AST::NumericLiteral& num_lit) noexcept -> bool {
                const auto* const num_lit_p = boost::get<int>(&num_lit);
                return num_lit_p && *num_lit_p == 5;
            }}.apply_to_all(const_ast);
        BOOST_CHECK(num_lit);
    }
}

BOOST_AUTO_TEST_CASE(addition_expression_seek_produces_expected_results)
{
    static constexpr std::string_view input{"var x int = 2 - 1; var y int = 4 + 3;"};

    const auto ast = MAL::parse(input);

    const auto* const ae = MAL::AST::QueryVisitor<const MAL::AST::AdditionExpression>{
        [](const MAL::AST::AdditionExpression& ae) noexcept -> bool {
            return ae.other_expressions.size() == 1 &&
                   ae.other_expressions[0].addition_operator == MAL::AST::AdditionOperator::Plus;
        }}.apply_to_all(ast);
    BOOST_REQUIRE(ae);

    const auto& prim_expr            = ae->first_expression.first_expression.primary_expression;
    const auto& num_lit              = boost::get<MAL::AST::NumericLiteral>(prim_expr);
    const auto* const num_lit_as_int = boost::get<int>(&num_lit);
    BOOST_REQUIRE(num_lit_as_int);
    BOOST_CHECK_EQUAL(*num_lit_as_int, 4);
}

BOOST_AUTO_TEST_CASE(receive_seek_produces_expected_results)
{
    static constexpr std::string_view input{R"(
        var x int = 2 - 1;
        receive var a int;
        send x;
        receive var b int;
        print "{}", b;)"};

    const auto ast = MAL::parse(input);

    const auto* const recv =
        MAL::AST::QueryVisitor<const MAL::AST::Receive>{[](const MAL::AST::Receive& r) noexcept -> bool {
            return r.size() == 1 && r[0].variable_name == "b";
        }}.apply_to_all(ast);
    BOOST_REQUIRE(recv);
}

BOOST_AUTO_TEST_CASE(if_seek_produces_expected_results)
{
    static constexpr std::string_view input{R"(
        receive var a int;
        if (1) {
            var x int = 2 - 1;
        } else {
            var y int   = 8;
            var z float = 3.14;
        }
        print "{}", a;
    )"};

    const auto ast = MAL::parse(input);
    {
        const auto* const is = MAL::AST::QueryVisitor<const MAL::AST::IfStatement>{}.apply_to_all(ast);
        BOOST_REQUIRE(is);
        BOOST_CHECK_EQUAL(is->then_statements.size(), 1);
        BOOST_CHECK_EQUAL(is->else_statements.size(), 2);
    }
    { // Find variable declaration in the "else" branch.
        const auto* const vd = MAL::AST::QueryVisitor<const MAL::AST::VariableDeclaration>{
            [](const MAL::AST::VariableDeclaration& vd) noexcept -> bool {
                return vd.variable_name == "z";
            }}.apply_to_all(ast);
        BOOST_CHECK(vd);
    }
}

BOOST_AUTO_TEST_CASE(array_literal_seek_produces_expected_results)
{
    static constexpr std::string_view input{R"(
        var a int[] = [42; 3];
    )"};

    const auto ast       = MAL::parse(input);
    const auto* const al = MAL::AST::QueryVisitor<const MAL::AST::ArrayLiteral>{}.apply_to_all(ast);
    BOOST_REQUIRE(al);

    {
        const auto* const value = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{}(al->value);
        BOOST_REQUIRE(value);
        const auto* const value_as_int = boost::get<int>(value);
        BOOST_REQUIRE(value_as_int);
        BOOST_CHECK_EQUAL(*value_as_int, 42);
    }

    {
        const auto* const size = MAL::AST::QueryVisitor<const MAL::AST::NumericLiteral>{}(al->size);
        BOOST_REQUIRE(size);
        const auto* const size_as_int = boost::get<int>(size);
        BOOST_REQUIRE(size_as_int);
        BOOST_CHECK_EQUAL(*size_as_int, 3);
    }
}

BOOST_AUTO_TEST_SUITE_END();
