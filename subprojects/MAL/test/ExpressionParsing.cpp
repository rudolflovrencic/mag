#include <boost/test/unit_test.hpp>

#include "MAL/parser/Configuration.hpp"
#include "MAL/parser/Expression.hpp"

#include <boost/variant/get.hpp>

#include "Common.hpp"

namespace x3 = boost::spirit::x3;

BOOST_AUTO_TEST_SUITE(expression_parsing);

BOOST_AUTO_TEST_CASE(string_literal_produces_expected_results)
{
    const auto& rule = MAL::get_string_literal_rule();

    {
        static constexpr std::string_view input{R"("Hello World")"};
        const MAL::AST::StringLiteral sl{parse(input, rule)};
        BOOST_CHECK_EQUAL(sl, "Hello World");
    }
    {
        static constexpr std::string_view input{R"("Hello "World")"};
        BOOST_CHECK_EXCEPTION(
            const MAL::AST::StringLiteral sl{parse(input, rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "World\""; });
    }
    {
        static constexpr std::string_view input{R"("")"};
        BOOST_CHECK_EXCEPTION(
            const MAL::AST::StringLiteral sl{parse(input, rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "\""; });
    }
}

BOOST_AUTO_TEST_CASE(numeric_literal_produces_expected_results)
{
    const auto& rule = MAL::get_numeric_literal_rule();

    {
        const MAL::AST::NumericLiteral nl{parse("42", rule)};
        const auto* const num_lit_as_int = boost::get<int>(&nl);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 42);
    }
    {
        BOOST_CHECK_EXCEPTION(
            const MAL::AST::NumericLiteral nl{parse("4a2", rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "a2"; });
    }
    {
        const MAL::AST::NumericLiteral nl{parse("3.14", rule)};
        const auto* const num_lit_as_double = boost::get<double>(&nl);
        BOOST_REQUIRE(num_lit_as_double);
        BOOST_CHECK_EQUAL(*num_lit_as_double, 3.14);
    }
    {
        const MAL::AST::NumericLiteral nl{parse(" true", rule)};
        const auto* const num_lit_as_bool = boost::get<bool>(&nl);
        BOOST_REQUIRE(num_lit_as_bool);
        BOOST_CHECK(*num_lit_as_bool);
    }
    {
        const MAL::AST::NumericLiteral nl{parse("false", rule)};
        const auto* const num_lit_as_bool = boost::get<bool>(&nl);
        BOOST_REQUIRE(num_lit_as_bool);
        BOOST_CHECK(!*num_lit_as_bool);
    }
    {
        for (const std::string_view sv : {"True", "tRue", "faLsE", "False"}) {
            BOOST_CHECK_EXCEPTION(
                const MAL::AST::NumericLiteral nl{parse(sv, rule)},
                LeftoverError,
                [sv](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == sv; });
        }
    }
}

BOOST_AUTO_TEST_CASE(variable_identifier_produces_expected_results)
{
    const auto& rule = MAL::get_variable_identifier_rule();

    for (const std::string_view valid_identifier : {"x", "some_variable", "x1", "True"}) {
        const MAL::AST::VariableIdentifier i{parse(valid_identifier, rule)};
        BOOST_REQUIRE_EQUAL(i, valid_identifier);
    }

    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableIdentifier i{parse("1x", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "1x"; });

    // Keywords are not allowed as variable identifiers.
    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableIdentifier i{parse("int", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "int"; });
    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableIdentifier i{parse("true", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "true"; });
    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableIdentifier i{parse("false", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "false"; });
}

BOOST_AUTO_TEST_CASE(primary_expression_produces_expected_results)
{
    const auto& rule = MAL::get_primary_expression_rule();

    {
        const MAL::AST::PrimaryExpression prim_expr{parse("42", rule)};
        const auto& num_lit              = boost::get<MAL::AST::NumericLiteral>(prim_expr);
        const auto* const num_lit_as_int = boost::get<int>(&num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, 42);
    }
    {
        const MAL::AST::PrimaryExpression prim_expr{parse("some_identifier", rule)};
        const auto* const vr = boost::get<x3::forward_ast<MAL::AST::VariableReference>>(&prim_expr);
        BOOST_REQUIRE(vr);
        BOOST_CHECK_EQUAL(vr->get().variable_identifier, "some_identifier");
    }
    {
        const MAL::AST::PrimaryExpression prim_expr{parse("len(xyz)", rule)};
        const auto* const lfc = boost::get<x3::forward_ast<MAL::AST::LenFunctionCall>>(&prim_expr);
        BOOST_CHECK(lfc);
    }
}

BOOST_AUTO_TEST_CASE(prefix_expression_produces_expected_results)
{
    const auto& rule = MAL::get_prefix_expression_rule();

    {
        const MAL::AST::PrefixExpression pref_expr{parse(" true", rule)};
        BOOST_CHECK(!pref_expr.prefix_operator);
        const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_bool = boost::get<bool>(num_lit);
        BOOST_REQUIRE(num_lit_as_bool);
        BOOST_CHECK(*num_lit_as_bool);
    }
    {
        const MAL::AST::PrefixExpression pref_expr{parse("!false", rule)};
        BOOST_REQUIRE(pref_expr.prefix_operator);
        BOOST_CHECK(*pref_expr.prefix_operator == MAL::AST::PrefixOperator::Not);
        const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_bool = boost::get<bool>(num_lit);
        BOOST_REQUIRE(num_lit_as_bool);
        BOOST_CHECK(!*num_lit_as_bool);
    }
    { // The parser should be able to parse this, but semantic analysis should reject such expression.
        const MAL::AST::PrefixExpression pref_expr{parse("!-42", rule)};
        BOOST_REQUIRE(pref_expr.prefix_operator);
        BOOST_CHECK(*pref_expr.prefix_operator == MAL::AST::PrefixOperator::Not);
        const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
        BOOST_REQUIRE(num_lit);
        const auto* const num_lit_as_int = boost::get<int>(num_lit);
        BOOST_REQUIRE(num_lit_as_int);
        BOOST_CHECK_EQUAL(*num_lit_as_int, -42);
    }
}

BOOST_AUTO_TEST_CASE(variable_reference_produces_expected_results)
{
    const auto& rule = MAL::get_variable_reference_rule();

    for (const std::string_view valid_identifier : {"x", "some_variable", "x1"}) {
        const MAL::AST::VariableReference vr{parse(valid_identifier, rule)};
        BOOST_CHECK_EQUAL(vr.variable_identifier, valid_identifier);
        BOOST_CHECK(!vr.index);
    }
    {
        static constexpr std::string_view input{"some_member[i + 1]"};
        const MAL::AST::VariableReference vr{parse(input, rule)};
        BOOST_CHECK_EQUAL(vr.variable_identifier, "some_member");
        BOOST_REQUIRE(vr.index);
        const auto& add_expr = *vr.index;
        BOOST_REQUIRE_EQUAL(add_expr.other_expressions.size(), 1);
        BOOST_CHECK(add_expr.other_expressions[0].addition_operator == MAL::AST::AdditionOperator::Plus);
        { // Left-hand side.
            const auto& mul_expr = add_expr.first_expression;
            BOOST_CHECK(mul_expr.other_expressions.empty());
            const auto& pref_expr = mul_expr.first_expression;
            BOOST_CHECK(!pref_expr.prefix_operator);
            const auto& prim_expr     = pref_expr.primary_expression;
            const auto* const var_ref = boost::get<x3::forward_ast<MAL::AST::VariableReference>>(&prim_expr);
            BOOST_REQUIRE(var_ref);
            BOOST_CHECK_EQUAL(var_ref->get().variable_identifier, "i");
            BOOST_CHECK(!var_ref->get().index);
        }
        { // Left-hand side.
            const auto& mul_expr = add_expr.other_expressions[0].multiplication_expression;
            BOOST_CHECK(mul_expr.other_expressions.empty());
            const auto& pref_expr = mul_expr.first_expression;
            BOOST_CHECK(!pref_expr.prefix_operator);
            const auto& prim_expr     = pref_expr.primary_expression;
            const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
            BOOST_REQUIRE(num_lit);
            const auto* const int_lit = boost::get<int>(num_lit);
            BOOST_REQUIRE(int_lit);
            BOOST_CHECK_EQUAL(*int_lit, 1);
        }
    }

    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableReference vr{parse(".x", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == ".x"; });

    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableReference vr{parse("some_struct_var.", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "."; });

    // Struct variable cannot be a keyword.
    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableReference vr{parse("int.x", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "int.x"; });

    // Member variable cannot be a keyword.
    BOOST_CHECK_EXCEPTION(const MAL::AST::VariableReference vr{parse("some_struct.int", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == ".int"; });
}

BOOST_AUTO_TEST_CASE(variable_declaration_produces_expected_results)
{
    const auto& rule = MAL::get_variable_declaration_rule();

    {
        const MAL::AST::VariableDeclaration vd{parse("var some_var int", rule)};
        BOOST_CHECK_EQUAL(vd.variable_name, "some_var");
        BOOST_CHECK(vd.type == MAL::AST::TypeKeyword::Int);
    }
    {
        const MAL::AST::VariableDeclaration vd{parse("var x float", rule)};
        BOOST_CHECK_EQUAL(vd.variable_name, "x");
        BOOST_CHECK(vd.type == MAL::AST::TypeKeyword::Float);
    }
    {
        const MAL::AST::VariableDeclaration vd{parse("var boolean_variable bool ", rule)};
        BOOST_CHECK_EQUAL(vd.variable_name, "boolean_variable");
        BOOST_CHECK(vd.type == MAL::AST::TypeKeyword::Bool);
    }
    {
        static constexpr std::string_view input{"const long_const_name SomeType"};
        BOOST_CHECK_EXCEPTION(
            const MAL::AST::VariableDeclaration vd{parse(input, rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == input; });
    }
    {
        static constexpr std::string_view input{"vaar x float"};
        BOOST_CHECK_EXCEPTION(
            const MAL::AST::VariableDeclaration vd{parse(input, rule)},
            LeftoverError,
            [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == input; });
    }
}

BOOST_AUTO_TEST_CASE(multiplication_expression_produces_expected_results)
{
    const auto& rule = MAL::get_multiplication_expression_rule();

    {
        static constexpr std::string_view input{"42 * 2"};
        const MAL::AST::MultiplicationExpression mul_expr{parse(input, rule)};

        BOOST_REQUIRE_EQUAL(mul_expr.other_expressions.size(), 1);

        const auto& pref_expr = mul_expr.first_expression;
        BOOST_CHECK(!pref_expr.prefix_operator);
        {
            const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
            BOOST_REQUIRE(num_lit);
            const auto* const num_lit_as_int = boost::get<int>(num_lit);
            BOOST_REQUIRE(num_lit_as_int);
            BOOST_CHECK_EQUAL(*num_lit_as_int, 42);
        }
        {
            const auto el            = mul_expr.other_expressions[0];
            const auto& el_pref_expr = el.prefix_expression;
            BOOST_CHECK(!el_pref_expr.prefix_operator);
            BOOST_CHECK(el.multiplication_operator == MAL::AST::MultiplicationOperator::Multiply);
            const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&el_pref_expr.primary_expression);
            BOOST_REQUIRE(num_lit);
            const auto* const num_lit_as_int = boost::get<int>(num_lit);
            BOOST_REQUIRE(num_lit_as_int);
            BOOST_CHECK_EQUAL(*num_lit_as_int, 2);
        }
    }

    BOOST_CHECK_EXCEPTION(const MAL::AST::MultiplicationExpression me{parse("42 + 2", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == "+ 2"; });

    {
        static constexpr std::string_view input{"some_variable"};
        const MAL::AST::MultiplicationExpression mul_expr{parse(input, rule)};

        BOOST_CHECK(mul_expr.other_expressions.empty());
        const auto& pref_expr = mul_expr.first_expression;
        BOOST_CHECK(!pref_expr.prefix_operator);
        const auto* const var_ref =
            boost::get<x3::forward_ast<MAL::AST::VariableReference>>(&pref_expr.primary_expression);
        BOOST_REQUIRE(var_ref);
        BOOST_CHECK_EQUAL(var_ref->get().variable_identifier, "some_variable");
    }
}

BOOST_AUTO_TEST_CASE(addition_expression_produces_expected_results)
{
    const auto& rule = MAL::get_addition_expression_rule();

    {
        static constexpr std::string_view input{"42 + 2 * 3"};
        const MAL::AST::AdditionExpression add_expr{parse(input, rule)};

        BOOST_REQUIRE_EQUAL(add_expr.other_expressions.size(), 1);
        {
            const auto& mul_expr = add_expr.first_expression;
            BOOST_CHECK(mul_expr.other_expressions.empty());
            const auto& pref_expr = mul_expr.first_expression;
            BOOST_CHECK(!pref_expr.prefix_operator);
            const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
            BOOST_REQUIRE(num_lit);
            const auto* const num_lit_as_int = boost::get<int>(num_lit);
            BOOST_REQUIRE(num_lit_as_int);
            BOOST_CHECK_EQUAL(*num_lit_as_int, 42);
        }
        BOOST_CHECK(add_expr.other_expressions.front().addition_operator == MAL::AST::AdditionOperator::Plus);
        {
            const auto& mul_expr = add_expr.other_expressions.front().multiplication_expression;
            BOOST_REQUIRE_EQUAL(mul_expr.other_expressions.size(), 1);
            {
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
                BOOST_REQUIRE(num_lit);
                const auto* const num_lit_as_int = boost::get<int>(num_lit);
                BOOST_REQUIRE(num_lit_as_int);
                BOOST_CHECK_EQUAL(*num_lit_as_int, 2);
            }
            BOOST_CHECK(mul_expr.other_expressions.front().multiplication_operator ==
                        MAL::AST::MultiplicationOperator::Multiply);
            {
                const auto& pref_expr     = mul_expr.other_expressions.front().prefix_expression;
                const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
                BOOST_REQUIRE(num_lit);
                const auto* const num_lit_as_int = boost::get<int>(num_lit);
                BOOST_REQUIRE(num_lit_as_int);
                BOOST_CHECK_EQUAL(*num_lit_as_int, 3);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(comparison_expression_produces_expected_results)
{
    const auto& rule = MAL::get_comparison_expression_rule();

    {
        static constexpr std::string_view input{"a  <=  42 + b/c"};
        const MAL::AST::ComparisonExpression comp_expr{parse(input, rule)};

        BOOST_REQUIRE_EQUAL(comp_expr.other_expressions.size(), 1);
        {
            const auto& add_expr = comp_expr.first_expression;
            BOOST_CHECK(add_expr.other_expressions.empty());
            const auto& mul_expr = add_expr.first_expression;
            BOOST_CHECK(mul_expr.other_expressions.empty());
            const auto& pref_expr = mul_expr.first_expression;
            BOOST_REQUIRE(!pref_expr.prefix_operator);
            const auto* const var_ref =
                boost::get<x3::forward_ast<MAL::AST::VariableReference>>(&pref_expr.primary_expression);
            BOOST_REQUIRE(var_ref);
            BOOST_CHECK_EQUAL(var_ref->get().variable_identifier, "a");
        }
        BOOST_CHECK(comp_expr.other_expressions.front().comparison_operator ==
                    MAL::AST::ComparisonOperator::LessOrEqual);
        {
            const auto& add_expr = comp_expr.other_expressions.front().addition_expression;
            BOOST_REQUIRE_EQUAL(add_expr.other_expressions.size(), 1);
            {
                const auto& mul_expr = add_expr.first_expression;
                BOOST_CHECK(mul_expr.other_expressions.empty());
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&pref_expr.primary_expression);
                BOOST_REQUIRE(num_lit);
                const auto* const num_lit_as_int = boost::get<int>(num_lit);
                BOOST_REQUIRE(num_lit_as_int);
                BOOST_CHECK_EQUAL(*num_lit_as_int, 42);
            }
            {
                const auto& mul_expr  = add_expr.other_expressions.front().multiplication_expression;
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                {
                    const auto* const var_ref =
                        boost::get<x3::forward_ast<MAL::AST::VariableReference>>(&pref_expr.primary_expression);
                    BOOST_REQUIRE(var_ref);
                    BOOST_CHECK_EQUAL(var_ref->get().variable_identifier, "b");
                }
                BOOST_REQUIRE_EQUAL(mul_expr.other_expressions.size(), 1);
                BOOST_CHECK(mul_expr.other_expressions.front().multiplication_operator ==
                            MAL::AST::MultiplicationOperator::Divide);
                const auto& el_pref_expr = mul_expr.other_expressions.front().prefix_expression;
                BOOST_CHECK(!el_pref_expr.prefix_operator);
                {
                    const auto* const var_ref =
                        boost::get<x3::forward_ast<MAL::AST::VariableReference>>(&el_pref_expr.primary_expression);
                    BOOST_REQUIRE(var_ref);
                    BOOST_CHECK_EQUAL(var_ref->get().variable_identifier, "c");
                }
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(array_literal_produces_expected_results)
{
    const auto& rule = MAL::get_array_literal_rule();

    {
        const MAL::AST::ArrayLiteral al{parse("[10 * -42; len(x) + 1]", rule)};
        {
            const auto& bool_or_expr = al.value;
            BOOST_CHECK(bool_or_expr.other_expressions.empty());
            const auto& bool_and_expr = bool_or_expr.first_expression;
            BOOST_CHECK(bool_and_expr.other_expressions.empty());
            const auto& comp_expr = bool_and_expr.first_expression;
            BOOST_CHECK(comp_expr.other_expressions.empty());
            const auto& add_expr = comp_expr.first_expression;
            BOOST_CHECK(add_expr.other_expressions.empty());
            const auto& mul_expr = add_expr.first_expression;
            BOOST_REQUIRE_EQUAL(mul_expr.other_expressions.size(), 1);
            BOOST_CHECK(mul_expr.other_expressions[0].multiplication_operator ==
                        MAL::AST::MultiplicationOperator::Multiply);
            {
                { // Left-hand side.
                    const auto& pref_expr = mul_expr.first_expression;
                    BOOST_CHECK(!pref_expr.prefix_operator);
                    const auto& prim_expr     = pref_expr.primary_expression;
                    const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
                    BOOST_REQUIRE(num_lit);
                    const auto* const int_lit = boost::get<int>(num_lit);
                    BOOST_REQUIRE(int_lit);
                    BOOST_CHECK_EQUAL(*int_lit, 10);
                }
                { // Right-hand side.
                    const auto& pref_expr = mul_expr.other_expressions[0].prefix_expression;
                    BOOST_CHECK(!pref_expr.prefix_operator);
                    const auto& prim_expr     = pref_expr.primary_expression;
                    const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
                    BOOST_REQUIRE(num_lit);
                    const auto* const int_lit = boost::get<int>(num_lit);
                    BOOST_REQUIRE(int_lit);
                    BOOST_CHECK_EQUAL(*int_lit, -42);
                }
            }
        }
        {
            const auto& add_expr = al.size;
            BOOST_REQUIRE_EQUAL(add_expr.other_expressions.size(), 1);
            BOOST_CHECK(add_expr.other_expressions[0].addition_operator == MAL::AST::AdditionOperator::Plus);

            { // Left-hand side.
                const auto& mul_expr = add_expr.first_expression;
                BOOST_CHECK(mul_expr.other_expressions.empty());
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                const auto& prim_expr         = pref_expr.primary_expression;
                const auto* const len_fn_call = boost::get<x3::forward_ast<MAL::AST::LenFunctionCall>>(&prim_expr);
                BOOST_REQUIRE(len_fn_call);
                const auto& variable_reference = len_fn_call->get().variable_reference;
                BOOST_CHECK_EQUAL(variable_reference.variable_identifier, "x");
            }
            { // Right-hand side.
                const auto& mul_expr = add_expr.other_expressions[0].multiplication_expression;
                BOOST_CHECK(mul_expr.other_expressions.empty());
                const auto& pref_expr = mul_expr.first_expression;
                BOOST_CHECK(!pref_expr.prefix_operator);
                const auto& prim_expr     = pref_expr.primary_expression;
                const auto* const num_lit = boost::get<MAL::AST::NumericLiteral>(&prim_expr);
                BOOST_REQUIRE(num_lit);
                const auto* const int_lit = boost::get<int>(num_lit);
                BOOST_REQUIRE(int_lit);
                BOOST_CHECK_EQUAL(*int_lit, 1);
            }
        }
    }

    BOOST_CHECK_EXCEPTION(const MAL::AST::ArrayLiteral arr_lit{parse("[42: 3]", rule)},
                          LeftoverError,
                          [](const LeftoverError& exc) noexcept -> bool { return exc.get_leftover_data() == ": 3]"; });
}

BOOST_AUTO_TEST_CASE(len_function_call_produces_expected_results)
{
    const auto& rule = MAL::get_len_function_call_rule();
    const MAL::AST::LenFunctionCall lfc{parse("len(some_long_variable_name)", rule)};
    BOOST_CHECK_EQUAL(lfc.variable_reference.variable_identifier, "some_long_variable_name");
}

BOOST_AUTO_TEST_SUITE_END();
