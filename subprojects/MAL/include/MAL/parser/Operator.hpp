#ifndef MAL_PARSER_OPERATOR_HPP
#define MAL_PARSER_OPERATOR_HPP

#include <boost/spirit/home/x3.hpp>
#include "MAL/parser/Annotators.hpp"

#include "MAL/AbstractSyntaxTree.hpp"

namespace MAL::Parser {

namespace x3 = boost::spirit::x3;

struct PrefixOperatorRID final : AnnotateOnSuccess {};
using PrefixOperatorRule = x3::rule<PrefixOperatorRID, AST::PrefixOperator>;

struct MultiplicationOperatorRID final : AnnotateOnSuccess {};
using MultiplicationOperatorRule = x3::rule<MultiplicationOperatorRID, AST::MultiplicationOperator>;

struct AdditionOperatorRID final : AnnotateOnSuccess {};
using AdditionOperatorRule = x3::rule<AdditionOperatorRID, AST::AdditionOperator>;

struct ComparisonOperatorRID final : AnnotateOnSuccess {};
using ComparisonOperatorRule = x3::rule<ComparisonOperatorRID, AST::ComparisonOperator>;

struct LogicalOperatorRID final : AnnotateOnSuccess {};
using LogicalOperatorRule = x3::rule<LogicalOperatorRID, AST::LogicalOperator>;

struct AssignmentOperatorRID final : AnnotateOnSuccess {};
using AssignmentOperatorRule = x3::rule<AssignmentOperatorRID, AST::AssignmentOperator>;

BOOST_SPIRIT_DECLARE(PrefixOperatorRule,
                     MultiplicationOperatorRule,
                     AdditionOperatorRule,
                     ComparisonOperatorRule,
                     LogicalOperatorRule,
                     AssignmentOperatorRule)

}

namespace MAL {

[[nodiscard]] const Parser::PrefixOperatorRule& get_prefix_operator_rule() noexcept;

[[nodiscard]] const Parser::MultiplicationOperatorRule& get_multiplication_operator_rule() noexcept;

[[nodiscard]] const Parser::AdditionOperatorRule& get_addition_operator_rule() noexcept;

[[nodiscard]] const Parser::ComparisonOperatorRule& get_comparison_operator_rule() noexcept;

[[nodiscard]] const Parser::LogicalOperatorRule& get_logical_operator_rule() noexcept;

[[nodiscard]] const Parser::AssignmentOperatorRule& get_assignment_operator_rule() noexcept;

}

#endif
