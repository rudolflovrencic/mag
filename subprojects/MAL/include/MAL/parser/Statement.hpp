#ifndef MAL_PARSER_STATEMENT_HPP
#define MAL_PARSER_STATEMENT_HPP

#include <boost/spirit/home/x3.hpp>

#include "MAL/parser/Annotators.hpp"
#include "MAL/AbstractSyntaxTree.hpp"

namespace MAL::Parser {

namespace x3 = boost::spirit::x3;

struct AssignmentRID final : AnnotateOnSuccess {};
using AssignmentRule = x3::rule<AssignmentRID, AST::Assignment>;

struct SendRID final : AnnotateOnSuccess {};
using SendRule = x3::rule<SendRID, AST::Send>;

struct ReceiveRID final : AnnotateOnSuccess {};
using ReceiveRule = x3::rule<ReceiveRID, AST::Receive>;

struct PrintRID final : AnnotateOnSuccess {};
using PrintRule = x3::rule<PrintRID, AST::Print>;

struct IfStatementRID final : AnnotateOnSuccess {};
using IfStatementRule = x3::rule<IfStatementRID, AST::IfStatement>;

struct ForStatementRID final : AnnotateOnSuccess {};
using ForStatementRule = x3::rule<ForStatementRID, AST::ForStatement>;

struct StatementRID final : AnnotateOnError, AnnotateOnSuccess {};
using StatementRule = x3::rule<StatementRID, AST::Statement>;

BOOST_SPIRIT_DECLARE(AssignmentRule, SendRule, ReceiveRule, PrintRule, IfStatementRule, ForStatementRule, StatementRule)

}

namespace MAL {

[[nodiscard]] const Parser::AssignmentRule& get_assignment_rule() noexcept;

[[nodiscard]] const Parser::SendRule& get_send_rule() noexcept;

[[nodiscard]] const Parser::ReceiveRule& get_receive_rule() noexcept;

[[nodiscard]] const Parser::PrintRule& get_print_rule() noexcept;

[[nodiscard]] const Parser::IfStatementRule& get_if_statement_rule() noexcept;

[[nodiscard]] const Parser::ForStatementRule& get_for_statement_rule() noexcept;

[[nodiscard]] const Parser::StatementRule& get_statement_rule() noexcept;

}

#endif
