#ifndef MAL_PARSER_ANNOTATORS_HPP
#define MAL_PARSER_ANNOTATORS_HPP

#include <boost/spirit/home/x3/support/ast/variant.hpp>
#include <boost/spirit/home/x3/support/utility/lambda_visitor.hpp>

#include "MAL/parser/Configuration.hpp"

namespace MAL::Parser {

namespace x3 = boost::spirit::x3;

struct AnnotateOnSuccess {
    // Position annotates the provided node.
    template<typename T, typename Iterator, typename Context>
    void on_success(const Iterator& first, const Iterator& last, T& ast, const Context& context);

    // Visits a variant to find the exact type, and then annotates by using the
    // exact type.
    template<typename Iterator, typename Context, typename... Types>
    void on_success(const Iterator& first, const Iterator& last, x3::variant<Types...>& ast, const Context& context);
};

struct AnnotateOnError {
    template<typename Iterator, typename Exception, typename Context>
    x3::error_handler_result
    on_error(Iterator& /*first*/, const Iterator& /*last*/, const Exception& exc, const Context& context);
};

template<typename T, typename Iterator, typename Context>
void AnnotateOnSuccess::on_success(const Iterator& first, const Iterator& last, T& ast, const Context& context)
{
    ErrorHandler& error_handler = x3::get<ErrorHandlerTag>(context).get();
    error_handler.tag(ast, first, last);
}

template<typename Iterator, typename Context, typename... Types>
void AnnotateOnSuccess::on_success(const Iterator& first,
                                   const Iterator& last,
                                   x3::variant<Types...>& ast,
                                   const Context& context)
{
    ast.apply_visitor(x3::make_lambda_visitor<void>([&](auto& node) { this->on_success(first, last, node, context); }));
}

template<typename Iterator, typename Exception, typename Context>
[[nodiscard]] x3::error_handler_result
AnnotateOnError::on_error(Iterator& /*first*/, const Iterator& /*last*/, const Exception& exc, const Context& context)
{
    ErrorHandler& error_handler = x3::get<ErrorHandlerTag>(context).get();
    std::string message         = "Error! Expecting " + exc.which() + " here:";
    error_handler(exc.where(), message);
    return x3::error_handler_result::fail;
}

}

#endif
