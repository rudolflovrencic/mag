#ifndef MAL_PARSER_EXPRESSION_HPP
#define MAL_PARSER_EXPRESSION_HPP

#include <boost/spirit/home/x3.hpp>

#include "MAL/parser/Annotators.hpp"
#include "MAL/AbstractSyntaxTree.hpp"

namespace MAL::Parser {

namespace x3 = boost::spirit::x3;

class IdentifierRID final : AnnotateOnSuccess {};
using IdentifierRule = x3::rule<IdentifierRID, std::string>;

struct VariableIdentifierRID final : AnnotateOnSuccess {};
using VariableIdentifierRule = x3::rule<VariableIdentifierRID, AST::VariableIdentifier>;

struct NumericLiteralRID final : AnnotateOnSuccess {};
using NumericLiteralRule = x3::rule<NumericLiteralRID, AST::NumericLiteral>;

struct ArrayLiteralRID final : AnnotateOnSuccess {};
using ArrayLiteralRule = x3::rule<ArrayLiteralRID, AST::ArrayLiteral>;

struct LenFunctionCallRID final : AnnotateOnSuccess {};
using LenFunctionCallRule = x3::rule<LenFunctionCallRID, AST::LenFunctionCall>;

struct StringLiteralRID final : AnnotateOnSuccess {};
using StringLiteralRule = x3::rule<StringLiteralRID, AST::StringLiteral>;

struct VariableDeclarationRID final : AnnotateOnSuccess {};
using VariableDeclarationRule = x3::rule<VariableDeclarationRID, AST::VariableDeclaration>;

struct VariableReferenceRID final : AnnotateOnSuccess {};
using VariableReferenceRule = x3::rule<VariableIdentifierRID, AST::VariableReference>;

struct PrimaryExpressionRID final : AnnotateOnSuccess {};
using PrimaryExpressionRule = x3::rule<PrimaryExpressionRID, AST::PrimaryExpression>;

struct PrefixExpressionRID final : AnnotateOnSuccess {};
using PrefixExpressionRule = x3::rule<PrefixExpressionRID, AST::PrefixExpression>;

struct MultiplicationExpressionRID final : AnnotateOnSuccess {};
using MultiplicationExpressionRule = x3::rule<MultiplicationExpressionRID, AST::MultiplicationExpression>;

struct AdditionExpressionRID final : AnnotateOnSuccess {};
using AdditionExpressionRule = x3::rule<AdditionExpressionRID, AST::AdditionExpression>;

struct ComparisonExpressionRID final : AnnotateOnSuccess {};
using ComparisonExpressionRule = x3::rule<ComparisonExpressionRID, AST::ComparisonExpression>;

struct BooleanAndExpressionRID final : AnnotateOnSuccess {};
using BooleanAndExpressionRule = x3::rule<BooleanAndExpressionRID, AST::BooleanAndExpression>;

struct BooleanOrExpressionRID final : AnnotateOnSuccess {};
using BooleanOrExpressionRule = x3::rule<BooleanOrExpressionRID, AST::BooleanOrExpression>;

BOOST_SPIRIT_DECLARE(IdentifierRule,
                     VariableIdentifierRule,
                     NumericLiteralRule,
                     ArrayLiteralRule,
                     LenFunctionCallRule,
                     StringLiteralRule,
                     VariableDeclarationRule,
                     VariableReferenceRule,
                     PrimaryExpressionRule,
                     PrefixExpressionRule,
                     MultiplicationExpressionRule,
                     AdditionExpressionRule,
                     ComparisonExpressionRule,
                     BooleanAndExpressionRule,
                     BooleanOrExpressionRule)

}

namespace MAL {

[[nodiscard]] const Parser::IdentifierRule& get_identifier_rule() noexcept;

[[nodiscard]] const Parser::VariableIdentifierRule& get_variable_identifier_rule() noexcept;

[[nodiscard]] const Parser::NumericLiteralRule& get_numeric_literal_rule() noexcept;

[[nodiscard]] const Parser::ArrayLiteralRule& get_array_literal_rule() noexcept;

[[nodiscard]] const Parser::LenFunctionCallRule& get_len_function_call_rule() noexcept;

[[nodiscard]] const Parser::StringLiteralRule& get_string_literal_rule() noexcept;

[[nodiscard]] const Parser::VariableDeclarationRule& get_variable_declaration_rule() noexcept;

[[nodiscard]] const Parser::VariableReferenceRule& get_variable_reference_rule() noexcept;

[[nodiscard]] const Parser::PrimaryExpressionRule& get_primary_expression_rule() noexcept;

[[nodiscard]] const Parser::PrefixExpressionRule& get_prefix_expression_rule() noexcept;

[[nodiscard]] const Parser::MultiplicationExpressionRule& get_multiplication_expression_rule() noexcept;

[[nodiscard]] const Parser::AdditionExpressionRule& get_addition_expression_rule() noexcept;

[[nodiscard]] const Parser::ComparisonExpressionRule& get_comparison_expression_rule() noexcept;

[[nodiscard]] const Parser::BooleanAndExpressionRule& get_boolean_and_expression_rule() noexcept;

[[nodiscard]] const Parser::BooleanOrExpressionRule& get_boolean_or_expression_rule() noexcept;

}

#endif
