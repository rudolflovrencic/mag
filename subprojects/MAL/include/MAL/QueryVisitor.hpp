#ifndef MAL_QUERYVISITOR_HPP
#define MAL_QUERYVISITOR_HPP

#include "MAL/AbstractSyntaxTree.hpp"

#include <cassert>

namespace MAL::AST {

template<typename T, typename... U>
constexpr bool is_any_of_v = (std::is_same_v<T, U> || ...);

template<typename DesiredNodeType>
class QueryVisitor final {
  public:
    using DesiredNodeTypeNoConst = std::remove_const_t<DesiredNodeType>;
    static_assert(is_any_of_v<DesiredNodeTypeNoConst,
                              PrefixOperator,
                              AdditionOperator,
                              MultiplicationOperator,
                              ComparisonOperator,
                              AssignmentOperator,
                              TypeKeyword,
                              VariableIdentifier,
                              NumericLiteral,
                              ArrayLiteral,
                              LenFunctionCall,
                              StringLiteral,
                              VariableReference,
                              PrimaryExpression,
                              PrefixExpression,
                              VariableDeclaration,
                              MultiplicationExpression,
                              MultiplicationExpression::Element,
                              AdditionExpression,
                              AdditionExpression::Element,
                              ComparisonExpression,
                              ComparisonExpression::Element,
                              BooleanAndExpression,
                              BooleanAndExpression::Element,
                              BooleanOrExpression,
                              BooleanOrExpression::Element,
                              Assignment,
                              Send,
                              Receive,
                              Print,
                              IfStatement,
                              ForStatement,
                              Statement>,
                  "Invalid DesiredNodeType for the QueryVisitor.");

    // Invoked when the current node is of the desired type (e.g. Identifier) in order to check weather or not the
    // current node fulfills some arbitrary requirements (e.g. identifier is named 'x').
    using Predicate = std::function<bool(const DesiredNodeType&)>;

  private:
    // If the desired node type is const, return the provided type as const. This enables a single query visitor over
    // the const and non-const AST.
    template<typename T>
    using C = std::conditional_t<std::is_const_v<DesiredNodeType>, const T, T>;

    // Invoked when the current node is not of the desired type or the current node does not satisfy the predicate
    // function in order to enable deeper search of the AST. (e.g. desired node is Identifier, current node is
    // Assignment - the function should visit the left and the right side of the Assignemnt to search for the Identifier
    // node).
    template<typename CurrentNodeType>
    using AdvanceAction = std::function<DesiredNodeType*(C<CurrentNodeType>&)>;

  private:
    Predicate predicate_function;

  public:
    explicit QueryVisitor(Predicate predicate_function = [](const DesiredNodeType& /*node*/) noexcept -> bool {
        return true;
    }) noexcept;

    [[nodiscard]] DesiredNodeType* operator()(C<PrefixOperator>& pref_op) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<AdditionOperator>& add_op) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<MultiplicationOperator>& mul_op) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ComparisonOperator>& comp_op) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<AssignmentOperator>& assign_op) const noexcept;

    [[nodiscard]] DesiredNodeType* operator()(C<TypeKeyword>& type_keyword) const noexcept;

    [[nodiscard]] DesiredNodeType* operator()(C<VariableIdentifier>& var_id) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<VariableReference>& var_ref) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<NumericLiteral>& num_literal) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ArrayLiteral>& array_literal) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<LenFunctionCall>& len_func_call) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<VariableDeclaration>& var_decl) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<StringLiteral>& str_literal) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<PrimaryExpression>& prim_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<PrefixExpression>& pref_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<MultiplicationExpression>& mul_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<MultiplicationExpression::Element>& mul_el) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<AdditionExpression>& add_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<AdditionExpression::Element>& add_el) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ComparisonExpression>& comp_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ComparisonExpression::Element>& comp_el) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<BooleanAndExpression>& and_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<BooleanAndExpression::Element>& and_el) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<BooleanOrExpression>& or_expr) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<BooleanOrExpression::Element>& or_el) const noexcept;

    [[nodiscard]] DesiredNodeType* operator()(C<Assignment>& assignment) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Send>& send) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Receive>& receive) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Print>& print) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<IfStatement>& if_statement) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<ForStatement>& for_statement) const noexcept;
    [[nodiscard]] DesiredNodeType* operator()(C<Statement>& statement) const noexcept;

    [[nodiscard]] DesiredNodeType* apply_to_all(C<std::vector<Statement>>& statements) const noexcept;

  private:
    // Returns a pointer to the current node if it is of the desired type and it satisfies the predicate function.
    // Otherwise, the provided non-matching action is invoked with the current node which allows further search or a
    // simple nullptr return if the current node is a leaf of the AST.
    template<typename CurrentNodeType>
    [[nodiscard]] DesiredNodeType* match(C<CurrentNodeType>& current_node,
                                         AdvanceAction<CurrentNodeType> non_matching_action) const noexcept;
};

template<typename DesiredNodeType>
QueryVisitor<DesiredNodeType>::QueryVisitor(Predicate predicate_function) noexcept
        : predicate_function{std::move(predicate_function)}
{
    assert(this->predicate_function);
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<PrefixOperator>& pref_op) const noexcept
{
    return match<PrefixOperator>(pref_op,
                                 [](C<PrefixOperator>& /*pref_op*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<AdditionOperator>& add_op) const noexcept
{
    return match<AdditionOperator>(
        add_op, [](C<AdditionOperator>& /*add_op*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<MultiplicationOperator>& mul_op) const noexcept
{
    return match<MultiplicationOperator>(
        mul_op, [](C<MultiplicationOperator>& /*mul_op*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ComparisonOperator>& comp_op) const noexcept
{
    return match<ComparisonOperator>(
        comp_op, [](C<ComparisonOperator>& /*comp_op*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<AssignmentOperator>& assign_op) const noexcept
{
    return match<AssignmentOperator>(
        assign_op, [](C<AssignmentOperator>& /*op*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<TypeKeyword>& type_keyword) const noexcept
{
    return match<TypeKeyword>(type_keyword,
                              [](C<TypeKeyword>& /*type_keyword*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<VariableIdentifier>& var_id) const noexcept
{
    return match<VariableIdentifier>(
        var_id, [](C<VariableIdentifier>& /*var_id*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<VariableReference>& var_ref) const noexcept
{
    return match<VariableReference>(var_ref, [this](C<VariableReference>& var_ref) noexcept -> DesiredNodeType* {
        if (auto* const res = operator()(var_ref.variable_identifier)) { return res; }
        if (var_ref.index) {
            if (auto* const res = operator()(*var_ref.index)) { return res; }
        }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<NumericLiteral>& num_literal) const noexcept
{
    return match<NumericLiteral>(num_literal,
                                 [](C<NumericLiteral>& /*lit*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ArrayLiteral>& array_literal) const noexcept
{
    return match<ArrayLiteral>(array_literal, [this](C<ArrayLiteral>& arr_lit) noexcept -> DesiredNodeType* {
        if (auto* const res = operator()(arr_lit.value)) { return res; }
        return operator()(arr_lit.size);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<LenFunctionCall>& len_func_call) const noexcept
{
    return match<LenFunctionCall>(len_func_call,
                                  [this](C<LenFunctionCall>& len_func_call) noexcept -> DesiredNodeType* {
                                      return operator()(len_func_call.variable_reference);
                                  });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<VariableDeclaration>& var_decl) const noexcept
{
    return match<VariableDeclaration>(var_decl, [this](C<VariableDeclaration>& var_decl) noexcept -> DesiredNodeType* {
        if (auto* const res = operator()(var_decl.variable_name)) { return res; }
        if (auto* const res = operator()(var_decl.type)) { return res; }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<StringLiteral>& str_literal) const noexcept
{
    return match<StringLiteral>(str_literal,
                                [](C<StringLiteral>& /*lit*/) noexcept -> DesiredNodeType* { return nullptr; });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<PrimaryExpression>& prim_expr) const noexcept
{
    return match<PrimaryExpression>(prim_expr, [this](C<PrimaryExpression>& prim_expr) noexcept -> DesiredNodeType* {
        return boost::apply_visitor(*this, prim_expr);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<PrefixExpression>& pref_expr) const noexcept
{
    return match<PrefixExpression>(pref_expr, [this](C<PrefixExpression>& pref_expr) noexcept -> DesiredNodeType* {
        if (pref_expr.prefix_operator) {
            auto* const res = operator()(*pref_expr.prefix_operator);
            if (res) { return res; }
        }
        return boost::apply_visitor(*this, pref_expr.primary_expression);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<MultiplicationExpression>& mul_expr) const noexcept
{
    return match<MultiplicationExpression>(mul_expr,
                                           [this](C<MultiplicationExpression>& mul_expr) noexcept -> DesiredNodeType* {
                                               auto* const res = operator()(mul_expr.first_expression);
                                               if (res) { return res; }
                                               for (auto& el : mul_expr.other_expressions) {
                                                   if (auto* const res = operator()(el)) { return res; }
                                               }
                                               return nullptr;
                                           });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<MultiplicationExpression::Element>& el) const noexcept
{
    return match<MultiplicationExpression::Element>(
        el, [this](C<MultiplicationExpression::Element>& el) noexcept -> DesiredNodeType* {
            auto* res = operator()(el.multiplication_operator);
            if (res) { return res; }
            res = operator()(el.prefix_expression);
            if (res) { return res; }
            return nullptr;
        });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<AdditionExpression>& add_expr) const noexcept
{
    return match<AdditionExpression>(add_expr, [this](C<AdditionExpression>& add_expr) noexcept -> DesiredNodeType* {
        auto* const res = operator()(add_expr.first_expression);
        if (res) { return res; }
        for (auto& el : add_expr.other_expressions) {
            if (auto* const res = operator()(el)) { return res; }
        }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<AdditionExpression::Element>& el) const noexcept
{
    return match<AdditionExpression::Element>(el,
                                              [this](C<AdditionExpression::Element>& el) noexcept -> DesiredNodeType* {
                                                  auto* res = operator()(el.addition_operator);
                                                  if (res) { return res; }
                                                  res = operator()(el.multiplication_expression);
                                                  if (res) { return res; }
                                                  return nullptr;
                                              });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ComparisonExpression>& comp_expr) const noexcept
{
    return match<ComparisonExpression>(comp_expr,
                                       [this](C<ComparisonExpression>& comp_expr) noexcept -> DesiredNodeType* {
                                           auto* const res = operator()(comp_expr.first_expression);
                                           if (res) { return res; }
                                           for (auto& el : comp_expr.other_expressions) {
                                               if (auto* const res = operator()(el)) { return res; }
                                           }
                                           return nullptr;
                                       });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ComparisonExpression::Element>& el) const noexcept
{
    return match<ComparisonExpression::Element>(
        el, [this](C<ComparisonExpression::Element>& el) noexcept -> DesiredNodeType* {
            auto* res = operator()(el.comparison_operator);
            if (res) { return res; }
            res = operator()(el.addition_expression);
            if (res) { return res; }
            return nullptr;
        });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<BooleanAndExpression>& and_expr) const noexcept
{
    return match<BooleanAndExpression>(and_expr,
                                       [this](C<BooleanAndExpression>& and_expr) noexcept -> DesiredNodeType* {
                                           auto* const res = operator()(and_expr.first_expression);
                                           if (res) { return res; }
                                           for (auto& el : and_expr.other_expressions) {
                                               if (auto* res = operator()(el)) { return res; }
                                           }
                                           return nullptr;
                                       });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<BooleanAndExpression::Element>& el) const noexcept
{
    return match<BooleanAndExpression::Element>(
        el, [this](C<BooleanAndExpression::Element>& el) noexcept -> DesiredNodeType* {
            auto* const res = operator()(el.comparison_expression);
            if (res) { return res; }
            return nullptr;
        });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<BooleanOrExpression>& or_expr) const noexcept
{
    return match<BooleanOrExpression>(or_expr, [this](C<BooleanOrExpression>& or_expr) noexcept -> DesiredNodeType* {
        auto* const res = operator()(or_expr.first_expression);
        if (res) { return res; }
        for (auto& el : or_expr.other_expressions) {
            if (auto* const res = operator()(el)) { return res; }
        }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<BooleanOrExpression::Element>& el) const noexcept
{
    return match<BooleanOrExpression::Element>(
        el, [this](C<BooleanOrExpression::Element>& el) noexcept -> DesiredNodeType* {
            auto* const res = operator()(el.boolean_and_expression);
            if (res) { return res; }
            return nullptr;
        });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Assignment>& assignment) const noexcept
{
    return match<Assignment>(assignment, [this](C<Assignment>& assignment) noexcept -> DesiredNodeType* {
        auto* res = boost::apply_visitor(*this, assignment.left_side);
        if (res) { return res; }
        res = operator()(assignment.assignment_operator);
        if (res) { return res; }
        res = boost::apply_visitor(*this, assignment.right_side);
        if (res) { return res; }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Send>& send) const noexcept
{
    return match<Send>(send, [this](C<Send>& send) noexcept -> DesiredNodeType* {
        for (auto& identifier : send) {
            auto* const res = operator()(identifier);
            if (res) { return res; }
        }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Receive>& receive) const noexcept
{
    return match<Receive>(receive, [this](C<Receive>& receive) noexcept -> DesiredNodeType* {
        for (auto& variable_declaration : receive) {
            auto* const res = operator()(variable_declaration);
            if (res) { return res; }
        }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Print>& print) const noexcept
{
    return match<Print>(print, [this](C<Print>& print) noexcept -> DesiredNodeType* {
        auto* const res = operator()(print.format_string);
        if (res) { return res; }
        for (auto& identifier : print.identifiers) {
            auto* const res = operator()(identifier);
            if (res) { return res; }
        }
        return nullptr;
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<IfStatement>& if_statement) const noexcept
{
    return match<IfStatement>(if_statement, [this](C<IfStatement>& if_statement) noexcept -> DesiredNodeType* {
        if (auto* const res = operator()(if_statement.condition)) { return res; }
        if (auto* const res = apply_to_all(if_statement.then_statements)) { return res; }
        return apply_to_all(if_statement.else_statements);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<ForStatement>& for_statement) const noexcept
{
    return match<ForStatement>(for_statement, [this](C<ForStatement>& for_statement) noexcept -> DesiredNodeType* {
        if (for_statement.init_statement) {
            if (auto* const res = operator()(*for_statement.init_statement)) { return res; }
        }
        if (auto* const res = operator()(for_statement.condition)) { return res; }
        if (for_statement.step_statement) {
            if (auto* const res = operator()(*for_statement.step_statement)) { return res; }
        }
        return apply_to_all(for_statement.body);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::operator()(C<Statement>& statement) const noexcept
{
    return match<Statement>(statement, [this](C<Statement>& statement) noexcept -> DesiredNodeType* {
        return boost::apply_visitor(*this, statement);
    });
}

template<typename DesiredNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::apply_to_all(C<std::vector<Statement>>& statements) const noexcept
{
    for (auto& s : statements) {
        if (auto* const res = boost::apply_visitor(*this, s)) { return res; }
    }
    return nullptr;
}

template<typename DesiredNodeType>
template<typename CurrentNodeType>
DesiredNodeType* QueryVisitor<DesiredNodeType>::match(C<CurrentNodeType>& current_node,
                                                      AdvanceAction<CurrentNodeType> advance_action) const noexcept
{
    if constexpr (std::is_same_v<DesiredNodeTypeNoConst, CurrentNodeType>) {
        return predicate_function(current_node) ? &current_node : advance_action(current_node);
    } else {
        return advance_action(current_node);
    }
}

}

#endif
