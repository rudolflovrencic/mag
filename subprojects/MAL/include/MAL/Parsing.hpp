#ifndef MAL_PARSING_HPP
#define MAL_PARSING_HPP

#include "MAL/AbstractSyntaxTree.hpp"

#include <stdexcept>
#include <string_view>
#include <vector>

namespace MAL {

[[nodiscard]] std::vector<AST::Statement> parse(std::string_view input);

class ParseError final : public std::runtime_error {
  public:
    explicit ParseError(std::string_view error_message) noexcept;
};

}

#endif
