#ifndef MAL_ABSTRACTSYNTAXTREEADAPTED_HPP
#define MAL_ABSTRACTSYNTAXTREEADAPTED_HPP

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/std_pair.hpp>

#include "MAL/AbstractSyntaxTree.hpp"

/* Adapt all AST structures with Boost::Fusion to be able to use them during parsing and whenever a Fusion traversial of
 * the AST is required. All adaptations must be in the global scope. */

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::VariableDeclaration, variable_name, type, is_array_declaration)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::IfStatement, condition, then_statements, else_statements)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::ForStatement, init_statement, condition, step_statement, body)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::VariableReference, variable_identifier, index)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::ArrayLiteral, value, size)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::LenFunctionCall, variable_reference)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::PrefixExpression, prefix_operator, primary_expression)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::MultiplicationExpression, first_expression, other_expressions)
BOOST_FUSION_ADAPT_STRUCT(MAL::AST::MultiplicationExpression::Element, multiplication_operator, prefix_expression)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::AdditionExpression, first_expression, other_expressions)
BOOST_FUSION_ADAPT_STRUCT(MAL::AST::AdditionExpression::Element, addition_operator, multiplication_expression)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::ComparisonExpression, first_expression, other_expressions)
BOOST_FUSION_ADAPT_STRUCT(MAL::AST::ComparisonExpression::Element, comparison_operator, addition_expression)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::BooleanAndExpression, first_expression, other_expressions)
BOOST_FUSION_ADAPT_STRUCT(MAL::AST::BooleanAndExpression::Element, comparison_expression)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::BooleanOrExpression, first_expression, other_expressions)
BOOST_FUSION_ADAPT_STRUCT(MAL::AST::BooleanOrExpression::Element, boolean_and_expression)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::Assignment, left_side, assignment_operator, right_side)

BOOST_FUSION_ADAPT_STRUCT(MAL::AST::Print, format_string, identifiers)

#endif
