#ifndef MAL_ABSTRACTSYNTAXTREE_HPP
#define MAL_ABSTRACTSYNTAXTREE_HPP

#include <boost/spirit/home/x3/support/ast/variant.hpp>
#include <boost/spirit/home/x3/support/ast/position_tagged.hpp>

#include <string>
#include <unordered_set>
#include <optional>

namespace MAL::AST {

namespace x3 = boost::spirit::x3;

// Operators

enum class PrefixOperator : bool { Not };
enum class MultiplicationOperator : bool { Multiply, Divide };
enum class AdditionOperator : bool { Plus, Minus };
enum class ComparisonOperator : unsigned char { Equal, NotEqual, Less, Greater, LessOrEqual, GreaterOrEqual };
enum class LogicalOperator : bool { And, Or };
enum class AssignmentOperator : bool { Equals };

// Keywords

enum class MainKeyword : unsigned char { Var, Send, Receive, Print, If, Else, For };
enum class TypeKeyword : unsigned char { Int, Float, Bool };
enum class BooleanLiteralKeyword : bool { True, False };

struct Keyword final : x3::variant<MainKeyword, TypeKeyword, BooleanLiteralKeyword>, x3::position_tagged {
    using base_type::base_type;
    using base_type::operator=;
};

// Expressions

struct VariableDeclaration;

struct VariableIdentifier final : std::string, x3::position_tagged {
    using std::string::string;
    using std::string::operator=;

    VariableDeclaration* declaration{nullptr};
};

struct ExpressionTypeInformation final {
    TypeKeyword type;
    bool is_array;

    [[nodiscard]] bool operator==(ExpressionTypeInformation other) const noexcept;
    [[nodiscard]] bool operator!=(ExpressionTypeInformation other) const noexcept;
};

struct VariableDeclaration final : x3::position_tagged {
    VariableIdentifier variable_name;
    TypeKeyword type;
    bool is_array_declaration;
};

struct StringLiteral final : std::string, x3::position_tagged {
    using std::string::string;
    using std::string::operator=;
};

struct NumericLiteral final : x3::variant<double, int, bool>, x3::position_tagged {
    using base_type::base_type;
    using base_type::operator=;

    std::optional<TypeKeyword> type;
};

struct LenFunctionCall;
struct VariableReference;
struct ArrayLiteral;

struct PrimaryExpression final
        : x3::variant<NumericLiteral, x3::forward_ast<LenFunctionCall>, x3::forward_ast<VariableReference>>,
          x3::position_tagged {
    using base_type::base_type;
    using base_type::operator=;

    std::optional<ExpressionTypeInformation> type;
};

struct PrefixExpression final {
    std::optional<PrefixOperator> prefix_operator;
    PrimaryExpression primary_expression;

    std::optional<ExpressionTypeInformation> type;
};

struct MultiplicationExpression final : x3::position_tagged {
    struct Element final {
        MultiplicationOperator multiplication_operator;
        PrefixExpression prefix_expression;
    };

    PrefixExpression first_expression;
    std::vector<Element> other_expressions;

    std::optional<ExpressionTypeInformation> type;
};

struct AdditionExpression final : x3::position_tagged {
    struct Element final {
        AdditionOperator addition_operator;
        MultiplicationExpression multiplication_expression;
    };

    MultiplicationExpression first_expression;
    std::vector<Element> other_expressions;

    std::optional<ExpressionTypeInformation> type;
};

struct ComparisonExpression final : x3::position_tagged {
    struct Element final {
        ComparisonOperator comparison_operator;
        AdditionExpression addition_expression;
    };

    AdditionExpression first_expression;
    std::vector<Element> other_expressions;

    std::optional<ExpressionTypeInformation> type;
};

struct BooleanAndExpression final : x3::position_tagged {
    struct Element final {
        static constexpr auto logical_operator = LogicalOperator::And;
        ComparisonExpression comparison_expression;
    };

    ComparisonExpression first_expression;
    std::vector<Element> other_expressions;

    std::optional<ExpressionTypeInformation> type;
};

struct BooleanOrExpression final : x3::position_tagged {
    struct Element final {
        static constexpr auto logical_operator = LogicalOperator::Or;
        BooleanAndExpression boolean_and_expression;
    };

    BooleanAndExpression first_expression;
    std::vector<Element> other_expressions;

    std::optional<ExpressionTypeInformation> type;
};

struct ArrayLiteral final : x3::position_tagged {
    BooleanOrExpression value;
    AdditionExpression size;

    std::optional<TypeKeyword> type;
};

struct VariableReference final {
    VariableIdentifier variable_identifier;
    std::optional<AdditionExpression> index;

    std::optional<ExpressionTypeInformation> type;
};

struct LenFunctionCall final {
    VariableReference variable_reference;

    static constexpr ExpressionTypeInformation type{TypeKeyword::Int, false};
};

// Statements

struct Assignment final : x3::position_tagged {
    x3::variant<VariableDeclaration, VariableReference> left_side;
    AssignmentOperator assignment_operator;
    x3::variant<BooleanOrExpression, ArrayLiteral> right_side;
};

struct Receive;
struct Send final : x3::position_tagged, std::vector<VariableReference> {
    using std::vector<VariableReference>::vector;
    using std::vector<VariableReference>::operator=;
};

struct Receive final : x3::position_tagged, std::vector<VariableDeclaration> {
    using std::vector<VariableDeclaration>::vector;
    using std::vector<VariableDeclaration>::operator=;
};

struct Print final : x3::position_tagged {
    StringLiteral format_string;
    std::vector<VariableIdentifier> identifiers;
};

struct Statement;
struct IfStatement final : x3::position_tagged {
    BooleanOrExpression condition;
    std::vector<Statement> then_statements;
    std::vector<Statement> else_statements;
};

struct ForStatement final : x3::position_tagged {
    std::optional<Assignment> init_statement;
    BooleanOrExpression condition;
    std::optional<Assignment> step_statement;
    std::vector<Statement> body;
};

struct Statement final : x3::variant<Assignment, Send, Receive, Print, IfStatement, ForStatement>, x3::position_tagged {
    using base_type::base_type;
    using base_type::operator=;
};

}

#endif
