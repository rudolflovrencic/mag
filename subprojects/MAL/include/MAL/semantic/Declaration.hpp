#ifndef MAL_SEMANTIC_DECLARATION_HPP
#define MAL_SEMANTIC_DECLARATION_HPP

#include "MAL/AbstractSyntaxTree.hpp"

#include <stdexcept>
#include <vector>
#include <string>

namespace MAL {

void pair_identifiers_and_declarations(std::vector<AST::Statement>& app_ast);

class MissingVariableDeclarationError final : public std::runtime_error {
  public:
    const AST::VariableIdentifier& identifier;

  public:
    explicit MissingVariableDeclarationError(const AST::VariableIdentifier& identifier) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(const AST::VariableIdentifier& identifier) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingVariableDeclarationError>);

class VariableRedeclaredError final : public std::runtime_error {
  public:
    const AST::VariableDeclaration& first_declaration;
    const AST::VariableDeclaration& second_declaration;

  public:
    VariableRedeclaredError(const AST::VariableDeclaration& first_declaration,
                            const AST::VariableDeclaration& second_declaration) noexcept;

  private:
    [[nodiscard]] static std::string format_what_message(const AST::VariableDeclaration& first_declaration,
                                                         const AST::VariableDeclaration& second_declaration) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<VariableRedeclaredError>);

}

#endif
