#ifndef MAL_HPP
#define MAL_HPP

#include "MAL/Parsing.hpp"

#include "MAL/PrintVisitor.hpp"
#include "MAL/QueryVisitor.hpp"

#include "MAL/semantic/Declaration.hpp"
#include "MAL/semantic/Expression.hpp"

namespace MAL {

[[nodiscard]] inline std::vector<AST::Statement> parse_and_check_semantics(std::string_view input)
{
    auto ast = parse(input);
    pair_identifiers_and_declarations(ast);
    determine_expression_types(ast);
    return ast;
}

}

#endif
