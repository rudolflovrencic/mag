#include "MAL/PrintVisitor.hpp"

#include "MAL/Visitor.hpp"

#include <cassert>

namespace MAL::AST {

PrintVisitor::Indenter::Indenter(unsigned indent) noexcept : indent{indent} {}

PrintVisitor::Indenter PrintVisitor::Indenter::operator+(int increase) const noexcept
{
    return Indenter{indent + increase};
}

PrintVisitor::PrintVisitor(std::ostream& os, unsigned indent) noexcept : os{os}, indent{indent} {}

void PrintVisitor::operator()(PrefixOperator op) const noexcept
{
    os << indent << "PrefixOperator " << op;
}

void PrintVisitor::operator()(AdditionOperator op) const noexcept
{
    os << indent << "AdditionOperator " << op;
}

void PrintVisitor::operator()(MultiplicationOperator op) const noexcept
{
    os << indent << "MultiplicationOperator " << op;
}

void PrintVisitor::operator()(ComparisonOperator op) const noexcept
{
    os << indent << "ComparisonOperator " << op;
}

void PrintVisitor::operator()(LogicalOperator op) const noexcept
{
    os << indent << "LogicalOperator " << op;
}

void PrintVisitor::operator()(AssignmentOperator op) const noexcept
{
    os << indent << "AssignmentOperator " << op;
}

void PrintVisitor::operator()(MainKeyword main_keyword) const noexcept
{
    os << indent << "Keyword " << main_keyword;
}

void PrintVisitor::operator()(TypeKeyword type_keyword) const noexcept
{
    os << type_keyword;
}

void PrintVisitor::operator()(const VariableIdentifier& var_id) const noexcept
{
    os << indent << "VariableIdentifier " << var_id << '\n';
}

void PrintVisitor::operator()(const VariableReference& var_ref) const noexcept
{
    os << indent << "VariableReference" << '\n';
    PrintVisitor print{os, indent + 1};
    print(var_ref.variable_identifier);
    if (var_ref.index) { print(*var_ref.index); }
}

void PrintVisitor::operator()(const NumericLiteral& nl) const noexcept
{
    os << indent << "NumericLiteral ";
    boost::apply_visitor(Visitor{[this](int value) noexcept -> void { os << "int " << value; },
                                 [this](double value) noexcept -> void { os << "double " << value; },
                                 [this](bool value) noexcept -> void { os << "bool " << std::boolalpha << value; }},
                         nl);
    os << '\n';
}

void PrintVisitor::operator()(const ArrayLiteral& al) const noexcept
{
    os << indent << "ArrayLiteral\n";
    PrintVisitor print{os, indent + 1};
    print(al.value);
    print(al.size);
}

void PrintVisitor::operator()(const LenFunctionCall& lfc) const noexcept
{
    os << indent << "LenFunctionCall\n";
    PrintVisitor print{os, indent + 1};
    print(lfc.variable_reference);
}

void PrintVisitor::operator()(const VariableDeclaration& decl) const noexcept
{
    os << indent << "VariableDeclaration " << decl.type << '\n';
    PrintVisitor print{os, indent + 1};
    print(decl.variable_name);
}

void PrintVisitor::operator()(const StringLiteral& sl) const noexcept
{
    os << indent << "StringLiteral " << sl;
}

void PrintVisitor::operator()(const PrimaryExpression& pe) const noexcept
{
    boost::apply_visitor(*this, pe);
}

void PrintVisitor::operator()(const PrefixExpression& pe) const noexcept
{
    os << indent << "PrefixExpression\n";
    PrintVisitor print{os, indent + 1};
    if (pe.prefix_operator) { print(*pe.prefix_operator); }
    print(pe.primary_expression);
}

void PrintVisitor::operator()(const MultiplicationExpression& me) const noexcept
{
    os << indent << "MultiplicationExpression\n";

    PrintVisitor print{os, indent + 1};
    print(me.first_expression);
    for (const auto& el : me.other_expressions) {
        print(el.multiplication_operator);
        os << '\n';
        print(el.prefix_expression);
    }
}

void PrintVisitor::operator()(const AdditionExpression& ae) const noexcept
{
    os << indent << "AdditionExpression\n";

    PrintVisitor print{os, indent + 1};
    print(ae.first_expression);
    for (const auto& el : ae.other_expressions) {
        print(el.addition_operator);
        os << '\n';
        print(el.multiplication_expression);
    }
}

void PrintVisitor::operator()(const ComparisonExpression& ce) const noexcept
{
    os << indent << "ComparisonExpression\n";

    PrintVisitor print{os, indent + 1};
    print(ce.first_expression);
    for (const auto& el : ce.other_expressions) {
        print(el.comparison_operator);
        os << '\n';
        print(el.addition_expression);
    }
}

void PrintVisitor::operator()(const BooleanAndExpression& bae) const noexcept
{
    os << indent << "BooleanAndExpression\n";

    PrintVisitor print{os, indent + 1};
    print(bae.first_expression);
    for (const auto& el : bae.other_expressions) {
        print(el.logical_operator);
        os << '\n';
        print(el.comparison_expression);
    }
}

void PrintVisitor::operator()(const BooleanOrExpression& boe) const noexcept
{
    os << indent << "BooleanOrExpression\n";

    PrintVisitor print{os, indent + 1};
    print(boe.first_expression);
    for (const auto& el : boe.other_expressions) {
        print(el.logical_operator);
        os << '\n';
        print(el.boolean_and_expression);
    }
}

void PrintVisitor::operator()(const Assignment& a) const noexcept
{
    os << indent << "Assignment\n";

    PrintVisitor print{os, indent + 1};
    boost::apply_visitor(print, a.left_side);
    print(a.assignment_operator);
    os << '\n';
    boost::apply_visitor(print, a.right_side);
}

void PrintVisitor::operator()(const Send& s) const noexcept
{
    os << indent << "Send\n";

    PrintVisitor print{os, indent + 1};
    for (const auto& identifier : s) { print(identifier); }
}

void PrintVisitor::operator()(const Receive& r) const noexcept
{
    os << indent << "Receive\n";

    PrintVisitor print{os, indent + 1};
    for (const auto& declaration : r) { print(declaration); }
}

void PrintVisitor::operator()(const Print& p) const noexcept
{
    os << indent << "Print\n";

    PrintVisitor print{os, indent + 1};
    print(p.format_string);
    for (const auto& identifier : p.identifiers) { print(identifier); }
}

void PrintVisitor::operator()(const IfStatement& is) const noexcept
{
    os << indent << "IfStatement\n" << indent << "condition:\n";
    PrintVisitor print{os, indent + 1};
    print(is.condition);
    os << indent << "then statements:\n";
    print.apply_to_all(is.then_statements);
    os << indent << "else statements:\n";
    print.apply_to_all(is.else_statements);
}

void PrintVisitor::operator()(const ForStatement& fs) const noexcept
{
    os << indent << "ForStatement\n" << indent << "init_statement:\n";
    PrintVisitor print{os, indent + 1};
    if (fs.init_statement) { print(*fs.init_statement); }
    os << indent << "condition:\n";
    print(fs.condition);
    os << indent << "step_statement:\n";
    if (fs.step_statement) { print(*fs.step_statement); }
    os << indent << "body:\n";
    print.apply_to_all(fs.body);
}

void PrintVisitor::apply_to_all(const std::vector<Statement>& statements) const noexcept
{
    for (const auto& s : statements) { boost::apply_visitor(*this, s); }
}

PrintVisitor::PrintVisitor(std::ostream& os, Indenter indenter) noexcept : os(os), indent(std::move(indenter)) {}

std::ostream& operator<<(std::ostream& os, const PrintVisitor::Indenter& in) noexcept
{
    for (unsigned i = 0; i < in.indent; i++) { os << '\t'; }
    return os;
}

std::ostream& operator<<(std::ostream& os, MainKeyword main_keyword) noexcept
{
    switch (main_keyword) {
        case MainKeyword::Send: return os << "send";
        case MainKeyword::Receive: return os << "receive";
        case MainKeyword::Print: return os << "print";
        case MainKeyword::If: return os << "if";
        case MainKeyword::Else: return os << "else";
        case MainKeyword::For: return os << "for";
        case MainKeyword::Var: return os << "var";
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, TypeKeyword type_keyword) noexcept
{
    switch (type_keyword) {
        case TypeKeyword::Int: return os << "int";
        case TypeKeyword::Float: return os << "float";
        case TypeKeyword::Bool: return os << "bool";
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, PrefixOperator op) noexcept
{
    switch (op) {
        case PrefixOperator::Not: return os << '!';
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, AdditionOperator op) noexcept
{
    switch (op) {
        case AdditionOperator::Plus: return os << '+';
        case AdditionOperator::Minus: return os << '-';
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, MultiplicationOperator op) noexcept
{
    switch (op) {
        case MultiplicationOperator::Multiply: return os << '*';
        case MultiplicationOperator::Divide: return os << '/';
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, ComparisonOperator op) noexcept
{
    switch (op) {
        case ComparisonOperator::Equal: return os << "==";
        case ComparisonOperator::NotEqual: return os << "!=";
        case ComparisonOperator::Less: return os << '<';
        case ComparisonOperator::Greater: return os << '>';
        case ComparisonOperator::LessOrEqual: return os << "<=";
        case ComparisonOperator::GreaterOrEqual: return os << "<=";
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, LogicalOperator op) noexcept
{
    switch (op) {
        case LogicalOperator::And: return os << "&&";
        case LogicalOperator::Or: return os << "||";
    }
    assert(false);
    std::terminate();
}

std::ostream& operator<<(std::ostream& os, AssignmentOperator op) noexcept
{
    switch (op) {
        case AssignmentOperator::Equals: return os << '=';
    }
    assert(false);
    std::terminate();
}

}
