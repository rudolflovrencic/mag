#include "MAL/AbstractSyntaxTree.hpp"

namespace MAL::AST {

bool ExpressionTypeInformation::operator==(ExpressionTypeInformation other) const noexcept
{
    return type == other.type && is_array == other.is_array;
}

bool ExpressionTypeInformation::operator!=(ExpressionTypeInformation other) const noexcept
{
    return !(*this == other);
}

}
