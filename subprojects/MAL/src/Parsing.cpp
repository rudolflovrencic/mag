#include "MAL/Parsing.hpp"

#include "MAL/parser/Statement.hpp"
#include "MAL/parser/Configuration.hpp"

#include <sstream>

namespace MAL {

std::vector<AST::Statement> parse(std::string_view input)
{
    namespace x3 = boost::spirit::x3;

    MAL::Parser::Iterator begin{input.begin()};
    MAL::Parser::Iterator end{input.end()};

    std::stringstream error_stream;
    MAL::Parser::ErrorHandler error_handler(begin, end, error_stream);
    const auto parser = x3::with<MAL::Parser::ErrorHandlerTag>(std::ref(error_handler))[*MAL::get_statement_rule()];

    static constexpr std::size_t reserve_size{16};
    std::vector<MAL::AST::Statement> result;
    result.reserve(reserve_size);
    const bool r{x3::phrase_parse(begin, end, parser, x3::space, result)};
    if (r && begin == end) {
        return result;
    } else {
        throw ParseError{error_stream.str()};
    }
}

ParseError::ParseError(std::string_view error_message) noexcept
        : std::runtime_error{std::string{"[Parse error] "}.append(error_message)}
{}

}
