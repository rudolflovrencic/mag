#include "MAL/semantic/Declaration.hpp"

#include "MAL/QueryVisitor.hpp"

#include <iomanip>
#include <cassert>
#include <sstream>

namespace MAL {

namespace {

[[nodiscard]] std::unordered_set<AST::VariableDeclaration*>
get_variable_declarations(std::vector<AST::Statement>& app_ast);

[[nodiscard]] std::unordered_set<AST::VariableIdentifier*>
get_variable_identifiers(std::vector<AST::Statement>& app_ast);

}

void pair_identifiers_and_declarations(std::vector<AST::Statement>& app_ast)
{
    const auto declarations = get_variable_declarations(app_ast);
    const auto identifiers  = get_variable_identifiers(app_ast);
    for (auto* const i : identifiers) {
        const auto it = std::find_if(
            declarations.begin(), declarations.end(), [i](const AST::VariableDeclaration* decl) noexcept -> bool {
                return decl->variable_name == *i;
            });
        if (it == declarations.end()) { throw MissingVariableDeclarationError{*i}; }
        i->declaration = *it;
    }
}

MissingVariableDeclarationError::MissingVariableDeclarationError(const AST::VariableIdentifier& identifier) noexcept
        : std::runtime_error{format_what_message(identifier)}, identifier(identifier)
{}

std::string MissingVariableDeclarationError::format_what_message(const AST::VariableIdentifier& identifier) noexcept
{
    std::ostringstream oss;
    oss << "Missing declaration for variable " << std::quoted(identifier) << '.';
    return oss.str();
}

VariableRedeclaredError::VariableRedeclaredError(const AST::VariableDeclaration& first_declaration,
                                                 const AST::VariableDeclaration& second_declaration) noexcept
        : std::runtime_error{format_what_message(first_declaration, second_declaration)},
          first_declaration(first_declaration),
          second_declaration(second_declaration)
{}

std::string
VariableRedeclaredError::format_what_message(const AST::VariableDeclaration& first_declaration,
                                             const AST::VariableDeclaration& /*second_declaration*/) noexcept
{
    std::ostringstream oss;
    oss << "Variable " << std::quoted(first_declaration.variable_name) << " has been redeclared.";
    return oss.str();
}

namespace {

std::unordered_set<AST::VariableDeclaration*> get_variable_declarations(std::vector<AST::Statement>& app_ast)
{
    using namespace AST;

    std::unordered_set<VariableDeclaration*> declarations;
    constexpr std::size_t reserve_size = 20;
    declarations.reserve(reserve_size);
    QueryVisitor<VariableDeclaration> qv([&declarations](const VariableDeclaration& var_decl) {
        return std::find_if(declarations.begin(),
                            declarations.end(),
                            [&var_decl](const VariableDeclaration* vd) noexcept -> bool { return vd == &var_decl; }) ==
               declarations.end();
    });

    while (auto* d = qv.apply_to_all(app_ast)) {
        const auto it = std::find_if(
            declarations.cbegin(), declarations.cend(), [d](const VariableDeclaration* vd) noexcept -> bool {
                return vd->variable_name == d->variable_name;
            });
        if (it != declarations.end()) { throw VariableRedeclaredError{**it, *d}; }

        [[maybe_unused]] const auto [iter, inserted] = declarations.insert(d);
        assert(inserted);
    }

    return declarations;
}

std::unordered_set<AST::VariableIdentifier*> get_variable_identifiers(std::vector<AST::Statement>& app_ast)
{
    using namespace AST;

    std::unordered_set<VariableIdentifier*> identifiers;
    constexpr std::size_t reserve_size = 20;
    identifiers.reserve(reserve_size);
    QueryVisitor<VariableIdentifier> qv([&identifiers](const VariableIdentifier& var_id) {
        return std::find_if(identifiers.begin(), identifiers.end(), [&var_id](VariableIdentifier* vi) noexcept -> bool {
                   return vi == &var_id;
               }) == identifiers.end();
    });

    while (auto* i = qv.apply_to_all(app_ast)) {
        [[maybe_unused]] auto [it, inserted] = identifiers.insert(i);
        assert(inserted);
    }

    return identifiers;
}

}

}
