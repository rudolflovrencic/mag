#include "MAL/semantic/Expression.hpp"

#include "MAL/Visitor.hpp"

namespace MAL {

namespace {

class VariableTypeExtractor final {
  public:
    [[nodiscard]] AST::ExpressionTypeInformation operator()(const AST::VariableDeclaration& declaration) const;
    AST::ExpressionTypeInformation operator()(AST::VariableReference& reference) const;
};

}

void ExpressionTypeSetter::operator()(MAL::AST::Assignment& assignment) const
{
    const auto right_side_type =
        boost::apply_visitor(Visitor{
                                 [](AST::BooleanOrExpression& or_expr) -> AST::ExpressionTypeInformation {
                                     return determine_expression_types(or_expr);
                                 },
                                 [](AST::ArrayLiteral& al) -> AST::ExpressionTypeInformation {
                                     const auto value_type = determine_expression_types(al.value);
                                     if (value_type.is_array) { throw ArrayTypeAsArrayValueError{al}; }
                                     const auto size_type = determine_expression_types(al.size);
                                     if (size_type.type != AST::TypeKeyword::Int) {
                                         throw NonIntegerTypeAsArraySizeError{al};
                                     }
                                     if (size_type.is_array) { throw ArrayTypeAsArraySizeError{al}; }
                                     al.type = value_type.type;
                                     return AST::ExpressionTypeInformation{*al.type, true};
                                 },
                             },
                             assignment.right_side);
    const auto left_side_type = boost::apply_visitor(VariableTypeExtractor{}, assignment.left_side);
    if (left_side_type != right_side_type) { throw NonMatchingTypesInAssignmentStatementError{assignment}; }
}

void ExpressionTypeSetter::operator()(MAL::AST::Send& send) const
{
    for (auto& var_ref : send) { VariableTypeExtractor{}(var_ref); }
}

void ExpressionTypeSetter::operator()(MAL::AST::Receive& /*receive*/) const noexcept {}

void ExpressionTypeSetter::operator()(MAL::AST::Print& /*print*/) const noexcept {}

void ExpressionTypeSetter::operator()(MAL::AST::IfStatement& if_statement) const
{
    const auto condition_type_info = determine_expression_types(if_statement.condition);
    if (condition_type_info.type != MAL::AST::TypeKeyword::Bool) {
        throw NonBooleanExpressionInIfConditionError{if_statement};
    }
    if (condition_type_info.is_array) { throw ArrayExpressionInIfConditionError{if_statement}; }
    apply_to_all(if_statement.then_statements);
    apply_to_all(if_statement.else_statements);
}

void ExpressionTypeSetter::operator()(MAL::AST::ForStatement& for_statement) const
{
    if (for_statement.init_statement) { this->operator()(*for_statement.init_statement); }
    determine_expression_types(for_statement.condition);
    if (for_statement.step_statement) { this->operator()(*for_statement.step_statement); }
    apply_to_all(for_statement.body);
}

void ExpressionTypeSetter::apply_to_all(std::vector<AST::Statement>& ast) const
{
    for (auto& statement : ast) { boost::apply_visitor(*this, statement); }
}

void determine_expression_types(std::vector<AST::Statement>& ast)
{
    ExpressionTypeSetter{}.apply_to_all(ast);
}

AST::TypeKeyword determine_expression_types(AST::NumericLiteral& nl) noexcept
{
    return *(nl.type = boost::apply_visitor(
                 Visitor{
                     [](int /*int_lit*/) noexcept -> AST::TypeKeyword { return AST::TypeKeyword::Int; },
                     [](double /*double_lit*/) noexcept -> AST::TypeKeyword { return AST::TypeKeyword::Float; },
                     [](bool /*bool_lit*/) noexcept -> AST::TypeKeyword { return AST::TypeKeyword::Bool; },
                 },
                 nl));
}

AST::ExpressionTypeInformation determine_expression_types(AST::PrimaryExpression& pe)
{
    return *(pe.type = boost::apply_visitor(
                 Visitor{
                     [](AST::NumericLiteral& num_lit) -> AST::ExpressionTypeInformation {
                         return AST::ExpressionTypeInformation{determine_expression_types(num_lit), false};
                     },
                     [](AST::VariableReference& vr) -> AST::ExpressionTypeInformation {
                         return VariableTypeExtractor{}(vr);
                     },
                     [](AST::LenFunctionCall& len_fn_call) -> AST::ExpressionTypeInformation {
                         const auto type_info = VariableTypeExtractor{}(len_fn_call.variable_reference);
                         if (!type_info.is_array) { throw LenFunctionCallOnNonArrayError{len_fn_call}; }
                         return len_fn_call.type;
                     },
                 },
                 pe));
}

AST::ExpressionTypeInformation determine_expression_types(AST::PrefixExpression& pe)
{
    const auto primary_expression_type_info = determine_expression_types(pe.primary_expression);
    if (pe.prefix_operator) {
        if (*pe.prefix_operator == AST::PrefixOperator::Not &&
            primary_expression_type_info.type != AST::TypeKeyword::Bool) {
            throw NonBooleanPrimaryExpressionInPrefixNotExpression{pe};
        }
    }
    return primary_expression_type_info;
}

AST::ExpressionTypeInformation determine_expression_types(AST::MultiplicationExpression& me)
{
    const auto first_expression_type_info = determine_expression_types(me.first_expression);
    if (first_expression_type_info.is_array && !me.other_expressions.empty()) {
        throw MultiplicationExpressionContainsArrayTypeError{me};
    }
    for (auto& other_expression : me.other_expressions) {
        const auto other_expression_type_info = determine_expression_types(other_expression.prefix_expression);
        if (other_expression_type_info.is_array) { throw MultiplicationExpressionContainsArrayTypeError{me}; }
        if (first_expression_type_info.type != other_expression_type_info.type) {
            throw NonMatchingTypesInMultiplicationExpressionError{me};
        }
    }

    return *(me.type = first_expression_type_info);
}

AST::ExpressionTypeInformation determine_expression_types(AST::AdditionExpression& ae)
{
    const auto first_expression_type_info = determine_expression_types(ae.first_expression);
    if (first_expression_type_info.is_array && !ae.other_expressions.empty()) {
        throw AdditionExpressionContainsArrayTypeError{ae};
    }
    for (auto& other_expression : ae.other_expressions) {
        const auto other_expression_type_info = determine_expression_types(other_expression.multiplication_expression);
        if (other_expression_type_info.is_array) { throw AdditionExpressionContainsArrayTypeError{ae}; }
        if (first_expression_type_info.type != other_expression_type_info.type) {
            throw NonMatchingTypesInAdditionExpressionError{ae};
        }
    }

    return *(ae.type = first_expression_type_info);
}

AST::ExpressionTypeInformation determine_expression_types(AST::ComparisonExpression& ce)
{
    const auto first_expression_type_info = determine_expression_types(ce.first_expression);
    if (ce.other_expressions.empty()) {
        ce.type = first_expression_type_info;
        return first_expression_type_info;
    }
    if (first_expression_type_info.is_array) { throw ComparisonExpressionContainsArrayTypeError{ce}; }
    if (first_expression_type_info.type == AST::TypeKeyword::Bool) { throw BooleanPartOfComparisonExpressionError{ce}; }

    auto lhs = std::cref(ce.first_expression);
    for (auto& other_expression : ce.other_expressions) {
        const auto other_expression_type_info = determine_expression_types(other_expression.addition_expression);
        if (other_expression_type_info.is_array) { throw ComparisonExpressionContainsArrayTypeError{ce}; }
        if (other_expression_type_info.type == AST::TypeKeyword::Bool) {
            throw BooleanPartOfComparisonExpressionError{ce};
        }
        lhs = other_expression.addition_expression;
    }

    return *(ce.type = AST::ExpressionTypeInformation{AST::TypeKeyword::Bool, false});
}

AST::ExpressionTypeInformation determine_expression_types(AST::BooleanAndExpression& bae)
{
    const auto first_expression_type_info = determine_expression_types(bae.first_expression);
    if (bae.other_expressions.empty()) {
        bae.type = first_expression_type_info;
        return first_expression_type_info;
    }

    if (first_expression_type_info.is_array) { throw BooleanAndExpressionContainsArrayTypeError{bae}; }
    if (first_expression_type_info.type != AST::TypeKeyword::Bool) {
        throw NonBooleanPartOfBooleanAndExpressionError{bae};
    }

    auto lhs = std::cref(bae.first_expression);
    for (auto& other_expression : bae.other_expressions) {
        const auto other_expression_type_info = determine_expression_types(other_expression.comparison_expression);
        if (other_expression_type_info.is_array) { throw BooleanAndExpressionContainsArrayTypeError{bae}; }
        if (other_expression_type_info.type != AST::TypeKeyword::Bool) {
            throw NonBooleanPartOfBooleanAndExpressionError{bae};
        }
        lhs = other_expression.comparison_expression;
    }

    return *(bae.type = AST::ExpressionTypeInformation{AST::TypeKeyword::Bool, false});
}

AST::ExpressionTypeInformation determine_expression_types(AST::BooleanOrExpression& boe)
{
    const auto first_expression_type_info = determine_expression_types(boe.first_expression);
    if (boe.other_expressions.empty()) {
        boe.type = first_expression_type_info;
        return first_expression_type_info;
    }

    if (first_expression_type_info.is_array) { throw BooleanOrExpressionContainsArrayTypeError{boe}; }
    if (first_expression_type_info.type != AST::TypeKeyword::Bool) {
        throw NonBooleanPartOfBooleanOrExpressionError{boe};
    }

    auto lhs = std::cref(boe.first_expression);
    for (auto& other_expression : boe.other_expressions) {
        const auto other_expression_type_info = determine_expression_types(other_expression.boolean_and_expression);
        if (other_expression_type_info.is_array) { throw BooleanOrExpressionContainsArrayTypeError{boe}; }
        if (other_expression_type_info.type != AST::TypeKeyword::Bool) {
            throw NonBooleanPartOfBooleanOrExpressionError{boe};
        }
        lhs = other_expression.boolean_and_expression;
    }

    return *(boe.type = AST::ExpressionTypeInformation{AST::TypeKeyword::Bool, false});
}

NonMatchingTypesInMultiplicationExpressionError::NonMatchingTypesInMultiplicationExpressionError(
    const AST::MultiplicationExpression& multiplication_expression) noexcept
        : multiplication_expression{multiplication_expression}
{}

const char* NonMatchingTypesInMultiplicationExpressionError::what() const noexcept
{
    return "NonMatchingTypesInMultiplicationExpressionError";
}

MultiplicationExpressionContainsArrayTypeError::MultiplicationExpressionContainsArrayTypeError(
    const AST::MultiplicationExpression& multiplication_expression) noexcept
        : multiplication_expression{multiplication_expression}
{}

const char* MultiplicationExpressionContainsArrayTypeError::what() const noexcept
{
    return "MultiplicationExpressionContainsArrayTypeError";
}

NonMatchingTypesInAdditionExpressionError::NonMatchingTypesInAdditionExpressionError(
    const AST::AdditionExpression& addition_expression) noexcept
        : addition_expression{addition_expression}
{}

const char* NonMatchingTypesInAdditionExpressionError::what() const noexcept
{
    return "NonMatchingTypesInAdditionExpressionError";
}

AdditionExpressionContainsArrayTypeError::AdditionExpressionContainsArrayTypeError(
    const AST::AdditionExpression& addition_expression) noexcept
        : addition_expression{addition_expression}
{}

const char* AdditionExpressionContainsArrayTypeError::what() const noexcept
{
    return "AdditionExpressionContainsArrayTypeError";
}

ComparisonExpressionContainsArrayTypeError::ComparisonExpressionContainsArrayTypeError(
    const AST::ComparisonExpression& comparison_expression) noexcept
        : comparison_expression{comparison_expression}
{}

const char* ComparisonExpressionContainsArrayTypeError::what() const noexcept
{
    return "ComparisonExpressionContainsArrayTypeError";
}

BooleanAndExpressionContainsArrayTypeError::BooleanAndExpressionContainsArrayTypeError(
    const AST::BooleanAndExpression& boolean_and_expression) noexcept
        : boolean_and_expression{boolean_and_expression}
{}

const char* BooleanAndExpressionContainsArrayTypeError::what() const noexcept
{
    return "BooleanAndExpressionContainsArrayTypeError";
}

BooleanOrExpressionContainsArrayTypeError::BooleanOrExpressionContainsArrayTypeError(
    const AST::BooleanOrExpression& boolean_or_expression) noexcept
        : boolean_or_expression{boolean_or_expression}
{}

const char* BooleanOrExpressionContainsArrayTypeError::what() const noexcept
{
    return "BooleanOrExpressionContainsArrayTypeError";
}

BooleanPartOfComparisonExpressionError::BooleanPartOfComparisonExpressionError(
    const AST::ComparisonExpression& comparison_expression) noexcept
        : comparison_expression{comparison_expression}
{}

const char* BooleanPartOfComparisonExpressionError::what() const noexcept
{
    return "BooleanPartOfComparisonExpressionError";
}

NonBooleanPartOfBooleanAndExpressionError::NonBooleanPartOfBooleanAndExpressionError(
    const AST::BooleanAndExpression& boolean_and_expression) noexcept
        : boolean_and_expression{boolean_and_expression}
{}

const char* NonBooleanPartOfBooleanAndExpressionError::what() const noexcept
{
    return "NonBooleanPartOfBooleanAndExpressionError";
}

NonBooleanPartOfBooleanOrExpressionError::NonBooleanPartOfBooleanOrExpressionError(
    const AST::BooleanOrExpression& boolean_or_expression) noexcept
        : boolean_or_expression{boolean_or_expression}
{}

const char* NonBooleanPartOfBooleanOrExpressionError::what() const noexcept
{
    return "NonBooleanPartOfBooleanOrExpressionError";
}

NonMatchingTypesInAssignmentStatementError::NonMatchingTypesInAssignmentStatementError(
    const AST::Assignment& assignment) noexcept
        : assignment{assignment}
{}

const char* NonMatchingTypesInAssignmentStatementError::what() const noexcept
{
    return "NonMatchingTypesInAssignmentStatementError";
}

NonBooleanExpressionInIfConditionError::NonBooleanExpressionInIfConditionError(
    const AST::IfStatement& if_statement) noexcept
        : if_statement{if_statement}
{}

const char* NonBooleanExpressionInIfConditionError::what() const noexcept
{
    return "NonBooleanExpressionInIfConditionError";
}

ArrayExpressionInIfConditionError::ArrayExpressionInIfConditionError(const AST::IfStatement& if_statement) noexcept
        : if_statement{if_statement}
{}

const char* ArrayExpressionInIfConditionError::what() const noexcept
{
    return "ArrayExpressionInIfConditionError";
}

NonBooleanPrimaryExpressionInPrefixNotExpression::NonBooleanPrimaryExpressionInPrefixNotExpression(
    const AST::PrefixExpression& prefix_expression) noexcept
        : prefix_expression{prefix_expression}
{}

const char* NonBooleanPrimaryExpressionInPrefixNotExpression::what() const noexcept
{
    return "NonBooleanPrimaryExpressionInPrefixNotExpression";
}

ArrayTypeAsArrayValueError::ArrayTypeAsArrayValueError(const AST::ArrayLiteral& array_literal) noexcept
        : array_literal{array_literal}
{}

const char* ArrayTypeAsArrayValueError::what() const noexcept
{
    return "ArrayTypeAsArrayValueError";
}

ArrayTypeAsArraySizeError::ArrayTypeAsArraySizeError(const AST::ArrayLiteral& array_literal) noexcept
        : array_literal{array_literal}
{}

const char* ArrayTypeAsArraySizeError::what() const noexcept
{
    return "ArrayTypeAsArraySizeError";
}

NonIntegerTypeAsArraySizeError::NonIntegerTypeAsArraySizeError(const AST::ArrayLiteral& array_literal) noexcept
        : array_literal{array_literal}
{}

const char* NonIntegerTypeAsArraySizeError::what() const noexcept
{
    return "NonIntegerTypeAsArraySizeError";
}

SubscriptOnNonArrayVariableError::SubscriptOnNonArrayVariableError(
    const AST::VariableReference& variable_reference) noexcept
        : variable_reference{variable_reference}
{
    assert(variable_reference.index);
    assert(variable_reference.variable_identifier.declaration);
}

const char* SubscriptOnNonArrayVariableError::what() const noexcept
{
    return "SubscriptOnNonArrayVariableError";
}

LenFunctionCallOnNonArrayError::LenFunctionCallOnNonArrayError(const AST::LenFunctionCall& len_function_call) noexcept
        : len_function_call{len_function_call}
{}

const char* LenFunctionCallOnNonArrayError::what() const noexcept
{
    return "LenFunctionCallOnNonArrayError";
}

NonIntegerSubscriptError::NonIntegerSubscriptError(const AST::VariableReference& variable_reference) noexcept
        : variable_reference{variable_reference}
{}

const char* NonIntegerSubscriptError::what() const noexcept
{
    return "NonIntegerSubscriptError";
}

namespace {

AST::ExpressionTypeInformation VariableTypeExtractor::operator()(const AST::VariableDeclaration& declaration) const
{
    return AST::ExpressionTypeInformation{declaration.type, declaration.is_array_declaration};
}

AST::ExpressionTypeInformation VariableTypeExtractor::operator()(AST::VariableReference& reference) const
{
    assert(reference.variable_identifier.declaration);
    reference.type = operator()(*reference.variable_identifier.declaration);
    if (reference.index) {
        if (!reference.type->is_array) { throw SubscriptOnNonArrayVariableError{reference}; }
        const auto type = determine_expression_types(*reference.index);
        if (type.is_array || type.type != AST::TypeKeyword::Int) { throw NonIntegerSubscriptError{reference}; }
        return AST::ExpressionTypeInformation{reference.type->type, false};
    }
    return *reference.type;
}

}

}
