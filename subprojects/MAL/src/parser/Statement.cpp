#include "MAL/parser/Statement.hpp"

#include "MAL/AbstractSyntaxTreeAdapted.hpp"

#include "MAL/parser/Expression.hpp"
#include "MAL/parser/Operator.hpp"
#include "MAL/parser/Keyword.hpp"

namespace MAL::Parser {

using x3::omit;

namespace {

const auto& assignment_operator   = get_assignment_operator_rule();
const auto& variable_declaration  = get_variable_declaration_rule();
const auto& variable_reference    = get_variable_reference_rule();
const auto& identifier            = get_identifier_rule();
const auto& variable_identifier   = get_variable_identifier_rule();
const auto& string_literal        = get_string_literal_rule();
const auto& boolean_or_expression = get_boolean_or_expression_rule();
const auto& keyword_send          = get_keyword_send_rule();
const auto& keyword_receive       = get_keyword_receive_rule();
const auto& keyword_print         = get_keyword_print_rule();
const auto& keyword_if            = get_keyword_if_rule();
const auto& keyword_for           = get_keyword_for_rule();
const auto& keyword_else          = get_keyword_else_rule();
const auto& array_literal         = get_array_literal_rule();
const auto& statement_rule        = get_statement_rule();

}

const AssignmentRule assignment{"assignment"};
const auto assignment_def = (variable_declaration | variable_reference) > assignment_operator >
                            (boolean_or_expression | array_literal);

const SendRule send{"send"};
const auto send_def = omit[keyword_send] > (variable_reference % ',');

const ReceiveRule receive{"receive"};
const auto receive_def = omit[keyword_receive] > (variable_declaration % ',');

const PrintRule print{"print"};
const auto print_def = omit[keyword_print] > string_literal > ',' > (variable_identifier % ',');

const IfStatementRule if_statement{"if_statement"};
const auto if_statement_def = omit[keyword_if] > '(' > boolean_or_expression > ')' > '{' > *statement_rule > '}' >>
                              -(omit[keyword_else] > '{' > *statement_rule > '}');

const ForStatementRule for_statement{"for_statement"};
const auto for_statement_def = omit[keyword_for] > '(' > -assignment > ';' > boolean_or_expression > ';' > -assignment >
                               ')' > '{' > *statement_rule > '}';

const StatementRule statement{"statement"};
const auto statement_def =
    (assignment > ';') | (send > ';') | (receive > ';') | (print > ';') | if_statement | for_statement;

BOOST_SPIRIT_DEFINE(assignment, send, receive, print, if_statement, for_statement, statement)

}

namespace MAL {

const Parser::AssignmentRule& get_assignment_rule() noexcept
{
    return Parser::assignment;
}

const Parser::SendRule& get_send_rule() noexcept
{
    return Parser::send;
}

const Parser::ReceiveRule& get_receive_rule() noexcept
{
    return Parser::receive;
}

const Parser::PrintRule& get_print_rule() noexcept
{
    return Parser::print;
}

const Parser::IfStatementRule& get_if_statement_rule() noexcept
{
    return Parser::if_statement;
}

const Parser::ForStatementRule& get_for_statement_rule() noexcept
{
    return Parser::for_statement;
}

const Parser::StatementRule& get_statement_rule() noexcept
{
    return Parser::statement;
}

}

#include "MAL/parser/Configuration.hpp"
namespace MAL::Parser {
BOOST_SPIRIT_INSTANTIATE(AssignmentRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(SendRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(ReceiveRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(PrintRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(IfStatementRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(ForStatementRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(StatementRule, Iterator, Context)
}
