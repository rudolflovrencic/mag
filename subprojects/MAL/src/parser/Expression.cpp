#include "MAL/parser/Expression.hpp"

#include "MAL/AbstractSyntaxTreeAdapted.hpp"

#include "MAL/parser/Keyword.hpp"
#include "MAL/parser/Operator.hpp"

namespace MAL::Parser {

using x3::alnum;
using x3::alpha;
using x3::char_;
using x3::double_;
using x3::int_;
using x3::bool_;
using x3::lexeme;
using x3::lit;
using x3::matches;

namespace {

const auto& keyword                 = get_keyword_rule();
const auto& type_keyword            = get_type_keyword_rule();
const auto& prefix_operator         = get_prefix_operator_rule();
const auto& addition_operator       = get_addition_operator_rule();
const auto& comparison_operator     = get_comparison_operator_rule();
const auto& multiplication_operator = get_multiplication_operator_rule();

const auto& keyword_var                = get_keyword_var_rule();
const auto& addition_expression_rule   = get_addition_expression_rule();
const auto& boolean_or_expression_rule = get_boolean_or_expression_rule();

}

const IdentifierRule identifier{"identifier"};
const auto identifier_def = lexeme[(alpha | char_('_')) >> *(alnum | char_('_'))] - keyword;

const VariableIdentifierRule variable_identifier{"variable_identifier"};
const auto variable_identifier_def = identifier;

const NumericLiteralRule numeric_literal{"numeric_literal"};
const auto numeric_literal_def = (&(int_ >> !lit('.')) > int_) | double_ | bool_;

const ArrayLiteralRule array_literal{"array_literal"};
const auto array_literal_def = ('[' >> boolean_or_expression_rule) > ';' > addition_expression_rule > ']';

const StringLiteralRule string_literal{"string_literal"};
const auto string_literal_def = lexeme['"' > +(char_ - '"') > '"'];

const VariableDeclarationRule variable_declaration = "variable_declaration";
const auto optional_array_symbol                   = matches[lit("[]")] >> -lit("[]");
const auto variable_declaration_def =
    x3::omit[keyword_var] > variable_identifier > type_keyword > optional_array_symbol;

const VariableReferenceRule variable_reference{"variable_reference"};
const auto variable_reference_def = variable_identifier >> -('[' > addition_expression_rule >> ']');

const LenFunctionCallRule len_function_call{"len_function_call"};
const auto len_function_call_def = lit("len") > lit('(') > variable_reference > lit(')');

const PrimaryExpressionRule primary_expression{"primary_expression"};
const auto primary_expression_def = numeric_literal | len_function_call | variable_reference;

const PrefixExpressionRule prefix_expression{"prefix_expression"};
const auto prefix_expression_def = -prefix_operator >> primary_expression;

const MultiplicationExpressionRule multiplication_expression{"multiplication_expression"};
const auto multiplication_expression_def = prefix_expression >> *(multiplication_operator > prefix_expression);

const AdditionExpressionRule addition_expression{"addition_expression"};
const auto addition_expression_def = multiplication_expression >> *(addition_operator > multiplication_expression);

const ComparisonExpressionRule comparison_expression{"comparison_expression"};
const auto comparison_expression_def = addition_expression >> *(comparison_operator > addition_expression);

const BooleanAndExpressionRule boolean_and_expression{"boolean_and_expression"};
const auto boolean_and_expression_def = comparison_expression >> *(lit("&&") > comparison_expression);

const BooleanOrExpressionRule boolean_or_expression{"boolean_or_expression"};
const auto boolean_or_expression_def = boolean_and_expression >> *(lit("||") > boolean_and_expression);

BOOST_SPIRIT_DEFINE(identifier,
                    variable_identifier,
                    numeric_literal,
                    array_literal,
                    len_function_call,
                    string_literal,
                    variable_declaration,
                    variable_reference,
                    primary_expression,
                    prefix_expression,
                    multiplication_expression,
                    addition_expression,
                    comparison_expression,
                    boolean_and_expression,
                    boolean_or_expression)
}

namespace MAL {

const Parser::IdentifierRule& get_identifier_rule() noexcept
{
    return Parser::identifier;
}

const Parser::VariableIdentifierRule& get_variable_identifier_rule() noexcept
{
    return Parser::variable_identifier;
}

const Parser::PrefixExpressionRule& get_prefix_expression_rule() noexcept
{
    return Parser::prefix_expression;
}

const Parser::PrimaryExpressionRule& get_primary_expression_rule() noexcept
{
    return Parser::primary_expression;
}

const Parser::NumericLiteralRule& get_numeric_literal_rule() noexcept
{
    return Parser::numeric_literal;
}

const Parser::ArrayLiteralRule& get_array_literal_rule() noexcept
{
    return Parser::array_literal;
}

const Parser::LenFunctionCallRule& get_len_function_call_rule() noexcept
{
    return Parser::len_function_call;
}

const Parser::StringLiteralRule& get_string_literal_rule() noexcept
{
    return Parser::string_literal;
}

const Parser::VariableDeclarationRule& get_variable_declaration_rule() noexcept
{
    return Parser::variable_declaration;
}

const Parser::VariableReferenceRule& get_variable_reference_rule() noexcept
{
    return Parser::variable_reference;
}

const Parser::MultiplicationExpressionRule& get_multiplication_expression_rule() noexcept
{
    return Parser::multiplication_expression;
}

const Parser::AdditionExpressionRule& get_addition_expression_rule() noexcept
{
    return Parser::addition_expression;
}

const Parser::ComparisonExpressionRule& get_comparison_expression_rule() noexcept
{
    return Parser::comparison_expression;
}

const Parser::BooleanAndExpressionRule& get_boolean_and_expression_rule() noexcept
{
    return Parser::boolean_and_expression;
}

const Parser::BooleanOrExpressionRule& get_boolean_or_expression_rule() noexcept
{
    return Parser::boolean_or_expression;
}

}

#include "MAL/parser/Configuration.hpp"
namespace MAL::Parser {
BOOST_SPIRIT_INSTANTIATE(IdentifierRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(VariableIdentifierRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(NumericLiteralRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(ArrayLiteralRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(LenFunctionCallRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(StringLiteralRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(VariableDeclarationRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(VariableReferenceRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(PrimaryExpressionRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(PrefixExpressionRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(MultiplicationExpressionRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(AdditionExpressionRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(ComparisonExpressionRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(BooleanAndExpressionRule, Iterator, Context)
BOOST_SPIRIT_INSTANTIATE(BooleanOrExpressionRule, Iterator, Context)
}
