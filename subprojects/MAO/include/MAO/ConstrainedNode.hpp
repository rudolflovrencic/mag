#ifndef MAO_CONSTRAINEDNODE_HPP
#define MAO_CONSTRAINEDNODE_HPP

#include "MAO/UniqueValueVector.hpp"

#include <MAL.hpp>

#include <ostream>
#include <variant>
#include <optional>
#include <functional>
#include <type_traits>

namespace MAO {

struct ArrayVariableLength final {
  private:
    std::reference_wrapper<const MAL::AST::VariableDeclaration> variable_declaration;

  public:
    explicit ArrayVariableLength(const MAL::AST::VariableDeclaration& variable_declaration) noexcept;
    explicit ArrayVariableLength(const MAL::AST::LenFunctionCall& len_fn_call) noexcept;

    [[nodiscard]] const MAL::AST::VariableDeclaration& get_variable_declaration() const noexcept;
};

using ConstrainedNode = std::variant<std::reference_wrapper<const MAL::AST::VariableDeclaration>, ArrayVariableLength>;
static_assert(std::is_trivially_copyable_v<ConstrainedNode>);

std::ostream& operator<<(std::ostream& os, ConstrainedNode constrained_node) noexcept;

struct ConstrainedNodeEqualTo final {
    [[nodiscard]] bool operator()(ConstrainedNode lhs, ConstrainedNode rhs) const noexcept;
};

struct ConstrainedNodeHasher final {
    [[nodiscard]] std::size_t operator()(ConstrainedNode constrained_node) const noexcept;
};

using ConstrainedNodeVector = UniqueValueVector<ConstrainedNode, ConstrainedNodeEqualTo>;

[[nodiscard]] std::size_t get_node_index(const ConstrainedNodeVector& constrained_nodes, ConstrainedNode node) noexcept;

struct VariableDeclarationReferenceWrapperEqualTo final {
    [[nodiscard]] bool operator()(std::reference_wrapper<const MAL::AST::VariableDeclaration> lhs,
                                  std::reference_wrapper<const MAL::AST::VariableDeclaration> rhs) const noexcept;
};

struct VariableDeclarationReferenceWrapperHasher final {
    [[nodiscard]] std::size_t operator()(std::reference_wrapper<const MAL::AST::VariableDeclaration> vd) const noexcept;
};

[[nodiscard]] std::optional<ConstrainedNode>
get_top_level_constrained_node(const MAL::AST::BooleanOrExpression& or_expr) noexcept;

[[nodiscard]] std::optional<ConstrainedNode>
get_top_level_constrained_node(const MAL::AST::BooleanAndExpression& ae) noexcept;

[[nodiscard]] std::optional<ConstrainedNode>
get_top_level_constrained_node(const MAL::AST::ComparisonExpression& ce) noexcept;

[[nodiscard]] std::optional<ConstrainedNode>
get_top_level_constrained_node(const MAL::AST::AdditionExpression& ae) noexcept;

[[nodiscard]] std::optional<ConstrainedNode>
get_top_level_constrained_node(const MAL::AST::MultiplicationExpression& me) noexcept;

[[nodiscard]] std::optional<ConstrainedNode>
get_top_level_constrained_node(const MAL::AST::PrefixExpression& pref_expr) noexcept;

}

#endif
