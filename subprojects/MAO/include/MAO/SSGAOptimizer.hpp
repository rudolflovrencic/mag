#ifndef MAO_SSGAOPTIMIZER_HPP
#define MAO_SSGAOPTIMIZER_HPP

#include "MAO/Optimizer.hpp"

#include <SHO/SO/SSGA.hpp>

#include <random>

namespace MAO {

class SSGAOptimizer final : public Optimizer {
  public:
    using Algorithm = SHO::SO::SSGA<Genotype>;

    class UniformCrossover final : public SHO::Crossover<Genotype> {
      private:
        std::mt19937 rng;
        std::reference_wrapper<TransformerTuple> transformers;

      public:
        explicit UniformCrossover(TransformerTuple& transformers) noexcept;

        [[nodiscard]] Genotype mate(const Genotype& first_parent, const Genotype& second_parent) final;
    };

    class SinglePointCrossover final : public SHO::Crossover<Genotype> {
      private:
        std::mt19937 rng;
        std::reference_wrapper<TransformerTuple> transformers;

      public:
        explicit SinglePointCrossover(TransformerTuple& transformers) noexcept;

        [[nodiscard]] Genotype mate(const Genotype& first_parent, const Genotype& second_parent) final;
    };

    class RandomMutation final : public SHO::Mutation<Genotype> {
      private:
        std::mt19937 rng;
        std::reference_wrapper<TransformerTuple> transformers;

      public:
        explicit RandomMutation(TransformerTuple& transformers) noexcept;

        void mutate(Genotype& solution) final;
    };

    class RandomFlipMutation final : public SHO::Mutation<Genotype> {
      private:
        std::mt19937 rng;
        std::reference_wrapper<TransformerTuple> transformers;

      public:
        explicit RandomFlipMutation(TransformerTuple& transformers) noexcept;

        void mutate(Genotype& solution) final;
    };

  public:
    [[nodiscard]] Genotype optimize(const Configuration& configuration) final;

  private:
    [[nodiscard]] static double compute_fitness_variance(const Algorithm::Population& population) noexcept;
    [[nodiscard]] static double compute_fitness_standard_deviation(const Algorithm::Population& population) noexcept;
};

}

#endif
