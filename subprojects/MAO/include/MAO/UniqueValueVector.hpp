#ifndef MAO_UNIQUEVALUEVECTOR_HPP
#define MAO_UNIQUEVALUEVECTOR_HPP

#include <utility>
#include <vector>
#include <algorithm>
#include <cassert>

namespace MAO {

// Ensures uniqueness of values and preserves the insertion order.
template<typename T, typename Equal = std::equal_to<T>, typename Allocator = std::allocator<T>>
class UniqueValueVector final {
  public:
    using AllocatorType = Allocator;
    using ConstIterator = typename std::vector<T>::const_iterator;

  private:
    std::vector<T, Allocator> storage;

  public:
    UniqueValueVector() noexcept = default;
    UniqueValueVector(std::initializer_list<T> init) noexcept;

    void reserve(std::size_t n) noexcept;

    [[nodiscard]] std::size_t size() const noexcept;
    [[nodiscard]] bool empty() const noexcept;

    void pop_back() noexcept;
    void clear() noexcept;

    template<typename UnaryPredicate>
    ConstIterator erase_if(UnaryPredicate predicate) noexcept;

    [[nodiscard]] const T& front() const noexcept;
    [[nodiscard]] const T& back() const noexcept;

    [[nodiscard]] ConstIterator find(const T& t) const noexcept;
    [[nodiscard]] bool contains(const T& t) const noexcept;

    [[nodiscard]] const T& at(std::size_t i) const;
    [[nodiscard]] const T& operator[](std::size_t i) const noexcept;

    std::pair<ConstIterator, bool> push_back(const T& t) noexcept;
    std::pair<ConstIterator, bool> push_back(T&& t) noexcept;

    [[nodiscard]] ConstIterator begin() const noexcept;
    [[nodiscard]] ConstIterator end() const noexcept;
    [[nodiscard]] ConstIterator cbegin() const noexcept;
    [[nodiscard]] ConstIterator cend() const noexcept;

    [[nodiscard]] const T* data() const noexcept;

    [[nodiscard]] std::size_t max_size() const noexcept;

    [[nodiscard]] const std::vector<T, Allocator>& get() const& noexcept;
    [[nodiscard]] std::vector<T, Allocator>&& get() && noexcept;
};

template<typename T, typename Equal, typename Allocator>
UniqueValueVector<T, Equal, Allocator>::UniqueValueVector(std::initializer_list<T> init) noexcept
{
    reserve(init.size());
    for (const auto& value : init) { push_back(value); }
}

template<typename T, typename Equal, typename Allocator>
void UniqueValueVector<T, Equal, Allocator>::reserve(std::size_t n) noexcept
{
    storage.reserve(n);
}

template<typename T, typename Equal, typename Allocator>
std::size_t UniqueValueVector<T, Equal, Allocator>::size() const noexcept
{
    return storage.size();
}

template<typename T, typename Equal, typename Allocator>
bool UniqueValueVector<T, Equal, Allocator>::empty() const noexcept
{
    return storage.empty();
}

template<typename T, typename Equal, typename Allocator>
void UniqueValueVector<T, Equal, Allocator>::pop_back() noexcept
{
    storage.pop_back();
}

template<typename T, typename Equal, typename Allocator>
void UniqueValueVector<T, Equal, Allocator>::clear() noexcept
{
    storage.clear();
}

template<typename T, typename Equal, typename Allocator>
template<typename UnaryPredicate>
auto UniqueValueVector<T, Equal, Allocator>::erase_if(UnaryPredicate predicate) noexcept -> ConstIterator
{
    const auto it = std::remove_if(storage.begin(), storage.end(), std::forward<UnaryPredicate>(predicate));
    return storage.erase(it, storage.cend());
}

template<typename T, typename Equal, typename Allocator>
const T& UniqueValueVector<T, Equal, Allocator>::front() const noexcept
{
    return storage.front();
}

template<typename T, typename Equal, typename Allocator>
const T& UniqueValueVector<T, Equal, Allocator>::back() const noexcept
{
    return storage.back();
}

template<typename T, typename Equal, typename Allocator>
const T& UniqueValueVector<T, Equal, Allocator>::at(std::size_t i) const
{
    return storage.at(i);
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::find(const T& t) const noexcept -> ConstIterator
{
    return std::find_if(storage.cbegin(), storage.cend(), [&t = std::as_const(t)](const T& i) noexcept -> bool {
        return Equal{}(t, i);
    });
}

template<typename T, typename Equal, typename Allocator>
bool UniqueValueVector<T, Equal, Allocator>::contains(const T& t) const noexcept
{
    return find(t) != storage.end();
}

template<typename T, typename Equal, typename Allocator>
const T& UniqueValueVector<T, Equal, Allocator>::operator[](std::size_t i) const noexcept
{
    assert(i < storage.size());
    return storage[i];
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::push_back(const T& t) noexcept -> std::pair<ConstIterator, bool>
{
    const auto it = find(t);
    if (it != storage.end()) { return {it, false}; }
    storage.push_back(t);
    return {storage.cend(), true};
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::push_back(T&& t) noexcept -> std::pair<ConstIterator, bool>
{
    const auto it = find(t);
    if (it != storage.end()) { return {it, false}; }
    storage.push_back(t);
    return {storage.cend(), true};
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::begin() const noexcept -> ConstIterator
{
    return storage.begin();
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::end() const noexcept -> ConstIterator
{
    return storage.end();
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::cbegin() const noexcept -> ConstIterator
{
    return storage.cbegin();
}

template<typename T, typename Equal, typename Allocator>
auto UniqueValueVector<T, Equal, Allocator>::cend() const noexcept -> ConstIterator
{
    return storage.cend();
}

template<typename T, typename Equal, typename Allocator>
std::size_t UniqueValueVector<T, Equal, Allocator>::max_size() const noexcept
{
    return storage.max_size();
}

template<typename T, typename Equal, typename Allocator>
const T* UniqueValueVector<T, Equal, Allocator>::data() const noexcept
{
    return storage.data();
}

template<typename T, typename Equal, typename Allocator>
const std::vector<T, Allocator>& UniqueValueVector<T, Equal, Allocator>::get() const& noexcept
{
    return storage;
}

template<typename T, typename Equal, typename Allocator>
std::vector<T, Allocator>&& UniqueValueVector<T, Equal, Allocator>::get() && noexcept
{
    return storage;
}

}

#endif
