#ifndef MAO_CONFIGURATION_HPP
#define MAO_CONFIGURATION_HPP

#include "MAO/FlowControl.hpp"
#include "MAO/Indexing.hpp"
#include "MAO/Trio.hpp"
#include "MAO/TaggedNode.hpp"
#include "MAO/UniqueValueVector.hpp"
#include "MAO/ConstrainedNode.hpp"
#include "MAO/Equivalence.hpp"

#include <MAL.hpp>

#include <functional>
#include <string>
#include <unordered_set>

namespace MAO {

class Host final {
  private:
    std::string name;
    unsigned trust;

  public:
    Host(std::string name, unsigned trust) noexcept;

    [[nodiscard]] const std::string& get_name() const noexcept;
    [[nodiscard]] unsigned get_trust() const noexcept;
};

struct HostEqualTo final {
    [[nodiscard]] bool operator()(const Host& lhs, const Host& rhs) const noexcept;
};

using HostVector = UniqueValueVector<Host, HostEqualTo>;

class Constraint final {
  private:
    ConstrainedNodeVector elements;
    unsigned importance;

  public:
    Constraint(ConstrainedNodeVector elements, unsigned importance) noexcept;

    [[nodiscard]] const ConstrainedNodeVector& get_elements() const noexcept;
    [[nodiscard]] unsigned get_importance() const noexcept;

    [[nodiscard]] bool contains(ConstrainedNode constrained_node) const noexcept;
};

struct ConstraintEqualTo final {
    [[nodiscard]] bool operator()(const Constraint& lhs, const Constraint& rhs) const noexcept;
};

using ConstraintVector = UniqueValueVector<Constraint, ConstraintEqualTo>;

[[nodiscard]] ConstrainedNodeVector get_nodes_to_optimize(const ConstraintVector& constraints,
                                                          const EquivalenceMap& equivalences,
                                                          const TrioVector& trios) noexcept;

[[nodiscard]] ConstrainedNodeVector get_deduced_nodes_to_optimize(const ConstraintVector& constraints,
                                                                  const EquivalenceMap& equivalences,
                                                                  const TrioVector& trios) noexcept;

class Configuration final {
  private:
    HostVector hosts;
    TaggedNodeVector tagged_nodes;
    ConstrainedNodeVector nodes_to_optimize;
    ConstraintVector constraints;
    EquivalenceMap equivalences;
    IndexingInformation indexing_information;
    TrioVector trios;
    FlowControlInformation flow_control_information;

  public:
    Configuration(HostVector hosts,
                  TaggedNodeVector tagged_nodes,
                  ConstrainedNodeVector nodes_to_optimize,
                  ConstraintVector constraints,
                  EquivalenceMap equivalences,
                  IndexingInformation indexing_information,
                  TrioVector trios,
                  FlowControlInformation flow_control_information) noexcept;

    [[nodiscard]] const HostVector& get_hosts() const noexcept;
    [[nodiscard]] const TaggedNodeVector& get_tagged_nodes() const noexcept;
    [[nodiscard]] const ConstrainedNodeVector& get_nodes_to_optimize() const noexcept;
    [[nodiscard]] const ConstraintVector& get_constraints() const noexcept;
    [[nodiscard]] const EquivalenceMap& get_equivalences() const noexcept;
    [[nodiscard]] const IndexingInformation& get_indexing_information() const noexcept;
    [[nodiscard]] const TrioVector& get_trios() const noexcept;
    [[nodiscard]] const FlowControlInformation& get_flow_control_information() const noexcept;
};

}

#endif
