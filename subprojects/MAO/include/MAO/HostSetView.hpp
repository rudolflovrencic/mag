#ifndef MAO_HOSTSETVIEW_HPP
#define MAO_HOSTSETVIEW_HPP

#include "MAO/Genotype.hpp"

#include <cstddef>

namespace MAO {

class HostSetView final {
  private:
    std::reference_wrapper<Genotype> genotype;
    std::size_t n_hosts;
    std::size_t node_index;

  public:
    HostSetView(Genotype& genotype, std::size_t n_hosts, std::size_t node_index) noexcept;

    [[nodiscard]] bool test(std::size_t host) const noexcept;
    void set(std::size_t host, bool value = true) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_node_index() const noexcept;

    [[nodiscard]] bool is_subset_of(HostSetView other) const noexcept;
    [[nodiscard]] bool is_superset_of(HostSetView other) const noexcept;

    [[nodiscard]] std::size_t count_hosts() const noexcept;

    [[nodiscard]] bool operator==(HostSetView other) const noexcept;
    [[nodiscard]] bool operator!=(HostSetView other) const noexcept;
};

class HostSetConstView final {
  private:
    std::reference_wrapper<const Genotype> genotype;
    std::size_t n_hosts;
    std::size_t node_index;

  public:
    HostSetConstView(const Genotype& genotype, std::size_t n_hosts, std::size_t node_index) noexcept;

    [[nodiscard]] bool test(std::size_t host) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_node_index() const noexcept;

    [[nodiscard]] std::size_t get_number_of_node_hosts() const noexcept;
};

[[nodiscard]] bool is_intersection_empty(HostSetConstView lhs, HostSetConstView rhs) noexcept;

class HostSetComputationCache final {
  private:
    boost::dynamic_bitset<> cache;

  public:
    explicit HostSetComputationCache(std::size_t n_hosts) noexcept;

    const HostSetComputationCache& compute_intersection(HostSetView a, HostSetView b) noexcept;
    const HostSetComputationCache& compute_intersection(HostSetView a, HostSetView b, HostSetView c) noexcept;

    const HostSetComputationCache& compute_union(HostSetView a, HostSetView b) noexcept;
    const HostSetComputationCache& compute_union(HostSetView a, HostSetView b, HostSetView c) noexcept;

    [[nodiscard]] bool all() const noexcept;
    [[nodiscard]] bool none() const noexcept;

    [[nodiscard]] bool test(std::size_t host) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;

  private:
    void intersect_cache_with_host_set(HostSetView host_set) noexcept;
    void union_cache_with_host_set(HostSetView host_set) noexcept;
};

}

#endif
