#ifndef MAO_TRANSFORMER_HPP
#define MAO_TRANSFORMER_HPP

#include "MAO/Genotype.hpp"
#include "MAO/Equivalence.hpp"
#include "MAO/FlowControl.hpp"
#include "MAO/HostSetView.hpp"
#include "MAO/Indexing.hpp"
#include "MAO/Trio.hpp"

#include <boost/dynamic_bitset.hpp>

#include <tuple>
#include <functional>
#include <optional>

namespace MAO {

class EquivalenceComplianceTransformer final {
  private:
    std::size_t n_hosts;
    std::size_t n_nodes_to_optimize;
    boost::dynamic_bitset<> encoded_equivalences;

  public:
    EquivalenceComplianceTransformer(std::size_t n_hosts,
                                     const ConstrainedNodeVector& nodes_to_optimize,
                                     const EquivalenceMap& equivalences) noexcept;

    [[nodiscard]] Genotype transform(Genotype genotype) const noexcept;

    [[nodiscard]] bool is_node_i_assigned_node_j(std::size_t i, std::size_t j) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;

  public:
    [[nodiscard]] static boost::dynamic_bitset<> encode_eqivalences(const ConstrainedNodeVector& nodes_to_optimize,
                                                                    const EquivalenceMap& equivalences) noexcept;
};

class IndexingInformationComplianceTransformer final {
  private:
    std::size_t n_hosts;
    std::size_t n_nodes_to_optimize;
    boost::dynamic_bitset<> encoded_indexing_information;

  public:
    IndexingInformationComplianceTransformer(std::size_t n_hosts,
                                             const ConstrainedNodeVector& nodes_to_optimize,
                                             const IndexingInformation& indexing_information);

    [[nodiscard]] Genotype transform(Genotype genotype) const noexcept;

    [[nodiscard]] bool is_node_i_indexed_by_node_j(std::size_t i, std::size_t j) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;

  public:
    [[nodiscard]] static boost::dynamic_bitset<>
    encode_indexing_information(const ConstrainedNodeVector& nodes_to_optimize,
                                const IndexingInformation& indexing_information) noexcept;
};

class TrioTransformer final {
  public:
    struct EncodedOperationNodes final {
        std::size_t first;
        std::optional<std::size_t> second;
        std::size_t third;
    };

  private:
    std::reference_wrapper<const HostVector> host_vector;
    std::size_t n_nodes_to_optimize;
    std::vector<EncodedOperationNodes> encoded_operation_nodes_vector;

    HostSetComputationCache cache;

  public:
    TrioTransformer(const HostVector& host_vector,
                    const ConstrainedNodeVector& nodes_to_optimize,
                    const TrioVector& trios,
                    const EquivalenceMap& equivalences) noexcept;

    [[nodiscard]] Genotype transform(Genotype genotype) noexcept;
    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;

  public:
    [[nodiscard]] static std::vector<EncodedOperationNodes>
    encode_operation_nodes_vector(const ConstrainedNodeVector& nodes_to_optimize,
                                  const TrioVector& trios,
                                  const EquivalenceMap& equivalences) noexcept;

  private:
    void handle_duo(HostSetView first, HostSetView second) noexcept;
    void handle_trio(HostSetView first, HostSetView second, HostSetView third) noexcept;

  private:
    [[nodiscard]] static std::optional<std::size_t>
    get_most_secure_host_with_multiple_elements(const HostVector& host_vector,
                                                const HostSetComputationCache& intersection_cache) noexcept;
};

class LenFunctionCallInformationComplianceTransformer final {
  private:
    std::size_t n_hosts;
    std::vector<std::optional<std::size_t>> encoded_len_vars;

  public:
    LenFunctionCallInformationComplianceTransformer(std::size_t n_hosts,
                                                    const ConstrainedNodeVector& nodes_to_optimize) noexcept;

    [[nodiscard]] Genotype transform(Genotype genotype) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;

  private:
    [[nodiscard]] bool complies_with_len_function_call_information(std::size_t len_fn_call_index,
                                                                   std::size_t declaration_index,
                                                                   const Genotype& genotype) const noexcept;
};

class FlowControlInformationComplianceTransformer final {
  private:
    std::size_t n_hosts;
    std::size_t n_nodes_to_optimize;
    boost::dynamic_bitset<> encoded_flow_control_information;

  public:
    FlowControlInformationComplianceTransformer(std::size_t n_hosts,
                                                const ConstrainedNodeVector& nodes_to_optimize,
                                                const FlowControlInformation& flow_control_information) noexcept;

    [[nodiscard]] Genotype transform(Genotype genotype) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;

  public:
    [[nodiscard]] static boost::dynamic_bitset<>
    encode_flow_control_information(const ConstrainedNodeVector& nodes_to_optimize,
                                    const FlowControlInformation& flow_control_information) noexcept;
};

class NodeOnHostTransformer final {
  private:
    std::size_t n_hosts;
    std::size_t n_nodes_to_optimize;

  public:
    NodeOnHostTransformer(std::size_t n_hosts, std::size_t n_nodes_to_optimize) noexcept;

    [[nodiscard]] Genotype transform(Genotype genotype) const noexcept;

    [[nodiscard]] std::size_t get_n_hosts() const noexcept;
    [[nodiscard]] std::size_t get_n_nodes_to_optimize() const noexcept;
};

using TransformerTuple = std::tuple<EquivalenceComplianceTransformer,
                                    IndexingInformationComplianceTransformer,
                                    TrioTransformer,
                                    LenFunctionCallInformationComplianceTransformer,
                                    FlowControlInformationComplianceTransformer,
                                    NodeOnHostTransformer>;

[[nodiscard]] Genotype transform(TransformerTuple& transformer_tuple, Genotype genotype) noexcept;

}

#endif
