#ifndef MAO_INDEXING_HPP
#define MAO_INDEXING_HPP

#include "MAO/ConstrainedNode.hpp"

#include <MAL.hpp>

#include <unordered_map>
#include <functional>
#include <optional>
#include <vector>

namespace MAO {

using IndexingInformation = std::unordered_map<std::reference_wrapper<const MAL::AST::VariableDeclaration>,
                                               ConstrainedNodeVector,
                                               VariableDeclarationReferenceWrapperHasher,
                                               VariableDeclarationReferenceWrapperEqualTo>;

class StatementIndexingInformationExtractor final {
  public:
    IndexingInformation result;

  private:
    std::reference_wrapper<const ConstrainedNodeVector> nodes_to_optimize;

  public:
    explicit StatementIndexingInformationExtractor(const ConstrainedNodeVector& nodes_to_optimize) noexcept;

    void operator()(const MAL::AST::Assignment& assignment) noexcept;
    void operator()(const MAL::AST::Send& send) noexcept;
    void operator()(const MAL::AST::Receive& receive) const noexcept;
    void operator()(const MAL::AST::Print& print) const noexcept;
    void operator()(const MAL::AST::IfStatement& if_statement) noexcept;
    void operator()(const MAL::AST::ForStatement& for_statement) noexcept;

    void operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept;
    void operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept;
    void operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept;

    ConstrainedNodeVector operator()(const MAL::AST::AdditionExpression& add_expr) noexcept;
    ConstrainedNodeVector operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept;
    ConstrainedNodeVector operator()(const MAL::AST::PrefixExpression& pref_expr) noexcept;

    ConstrainedNodeVector operator()(const MAL::AST::NumericLiteral& num_lit) noexcept;
    ConstrainedNodeVector operator()(const MAL::AST::VariableReference& vr) noexcept;
    ConstrainedNodeVector operator()(const MAL::AST::LenFunctionCall& len_call) noexcept;

  private:
    void add(std::reference_wrapper<const MAL::AST::VariableDeclaration> indexed_node,
             const ConstrainedNodeVector& indexing_nodes) noexcept;
};

[[nodiscard]] IndexingInformation extract_indexing_information(const std::vector<MAL::AST::Statement>& ast,
                                                               const ConstrainedNodeVector& nodes_to_optimize) noexcept;

}

#endif
