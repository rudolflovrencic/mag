#ifndef MAO_OPTIMIZER_HPP
#define MAO_OPTIMIZER_HPP

#include "MAO/Genotype.hpp"
#include "MAO/Transformer.hpp"

#include <SHO/SO/Evaluator.hpp>
#include <boost/dynamic_bitset.hpp>

#include <functional>
#include <cstddef>
#include <utility>
#include <optional>
#include <vector>
#include <memory>
#include <string_view>
#include <string>
#include <stdexcept>
#include <type_traits>
#include <unordered_map>

namespace MAO {

class Optimizer {
  public:
    using Factory    = std::function<std::unique_ptr<Optimizer>()>;
    using FactoryMap = std::unordered_map<std::string_view, Factory>;

  public:
    virtual ~Optimizer() noexcept = default;

    [[nodiscard]] virtual Genotype optimize(const Configuration& configuration) = 0;

  protected:
    Optimizer() noexcept                                  = default;
    Optimizer(const Optimizer& other) noexcept            = default;
    Optimizer& operator=(const Optimizer& other) noexcept = default;
    Optimizer(Optimizer&& other) noexcept                 = default;
    Optimizer& operator=(Optimizer&& other) noexcept      = default;

  public:
    [[nodiscard]] static std::unique_ptr<Optimizer> create_from_name(std::string_view optimizer_name);
};

class ConstraintEvaluator final : public SHO::SO::Evaluator<Genotype> {
  private:
    std::vector<std::pair<std::vector<std::size_t>, unsigned>> index_constraints;
    std::vector<unsigned> host_trusts;
    boost::dynamic_bitset<> intersecting_hosts;

  public:
    ConstraintEvaluator(const ConstrainedNodeVector& constrained_nodes,
                        const ConstraintVector& constraints,
                        const HostVector& hosts) noexcept;

    [[nodiscard]] double evaluate(const Genotype& genotype) final;
};

[[nodiscard]] ConstrainedNodeVector get_nodes_to_optimize(const ConstraintVector& constraints) noexcept;

class UnrecognizedOptimizerNameError final : public std::runtime_error {
  private:
    std::string_view provided_optimizer_name;
    std::reference_wrapper<const Optimizer::FactoryMap> optimizer_factories;

  public:
    UnrecognizedOptimizerNameError(std::string_view provided_optimizer_name,
                                   const Optimizer::FactoryMap& optimizer_factories) noexcept;

    [[nodiscard]] std::string_view get_provided_optimizer_name() const noexcept;
    [[nodiscard]] const Optimizer::FactoryMap& get_optimizer_factories() const noexcept;

  public:
    [[nodiscard]] static std::string format_what_message(std::string_view provided_optimizer_name,
                                                         const Optimizer::FactoryMap& optimizer_factories) noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<UnrecognizedOptimizerNameError>);

}

#endif
