#ifndef MAO_EQUIVALENCE_HPP
#define MAO_EQUIVALENCE_HPP

#include "MAO/TaggedNode.hpp"

#include <unordered_map>
#include <functional>
#include <vector>

namespace MAO {

using EquivalenceMap = std::unordered_map<std::reference_wrapper<const MAL::AST::VariableDeclaration>,
                                          TaggedNodeVector,
                                          VariableDeclarationReferenceWrapperHasher,
                                          VariableDeclarationReferenceWrapperEqualTo>;

class EquivalenceExtractor final {
  public:
    EquivalenceMap result;

  public:
    EquivalenceExtractor() noexcept;

    void operator()(const MAL::AST::Assignment& assignment) noexcept;
    void operator()(const MAL::AST::Send& send) const noexcept;
    void operator()(const MAL::AST::Receive& receive) const noexcept;
    void operator()(const MAL::AST::Print& print) const noexcept;
    void operator()(const MAL::AST::IfStatement& if_statement) noexcept;
    void operator()(const MAL::AST::ForStatement& for_statement) noexcept;

    void apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept;

  private:
    void add(const MAL::AST::VariableDeclaration& key, TaggedNode value) noexcept;
};

[[nodiscard]] EquivalenceMap extract_equivalences(const std::vector<MAL::AST::Statement>& ast) noexcept;

}

#endif
