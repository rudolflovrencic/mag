#ifndef MAO_GENOTYPE_HPP
#define MAO_GENOTYPE_HPP

#include "MAO/ConstrainedNode.hpp"
#include "MAO/Configuration.hpp"

#include <boost/dynamic_bitset.hpp>

#include <functional>
#include <ostream>
#include <string>
#include <cstddef>

namespace MAO {

using Genotype = boost::dynamic_bitset<>;

[[nodiscard]] bool is_subset(const Genotype& genotype,
                             std::size_t n_hosts,
                             std::size_t subset_node_index,
                             std::size_t superset_node_index) noexcept;

struct GenotypeLogger final {
    std::reference_wrapper<const Genotype> genotype;
    std::reference_wrapper<const ConstrainedNodeVector> constrained_nodes;
    std::reference_wrapper<const HostVector> hosts;
    std::string indent{"    "};
};

std::ostream& operator<<(std::ostream& os, GenotypeLogger logger) noexcept;

}

#endif
