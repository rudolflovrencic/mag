#ifndef MAO_VISITOR_HPP
#define MAO_VISITOR_HPP

namespace MAO {

template<typename... Base>
struct Visitor final : Base... {
    using Base::operator()...;
};

template<typename... T>
Visitor(T...) -> Visitor<T...>;

}

#endif
