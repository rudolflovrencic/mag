#ifndef MAO_MAPPING_HPP
#define MAO_MAPPING_HPP

#include "MAO/TaggedNode.hpp"
#include "MAO/UniqueValueVector.hpp"
#include "MAO/Genotype.hpp"
#include "MAO/FlowControl.hpp"

#include <functional>
#include <unordered_map>
#include <ostream>
#include <string>

namespace MAO {

struct HostRefEqualTo final {
    [[nodiscard]] bool operator()(std::reference_wrapper<const Host> lhs,
                                  std::reference_wrapper<const Host> rhs) const noexcept;
};

using HostRefVector = UniqueValueVector<std::reference_wrapper<const Host>, HostRefEqualTo>;

[[nodiscard]] bool is_subset(const HostRefVector& subset, const HostRefVector& superset) noexcept;
[[nodiscard]] bool is_intersection_empty(const HostRefVector& lhs, const HostRefVector& rhs) noexcept;
[[nodiscard]] const Host* get_first_common_host(const HostRefVector& lhs, const HostRefVector& rhs) noexcept;

[[nodiscard]] const Host*
get_first_common_host(const HostRefVector& a, const HostRefVector& b, const HostRefVector& c) noexcept;

[[nodiscard]] HostRefVector compute_intersection(const HostRefVector& a, const HostRefVector& b) noexcept;
[[nodiscard]] HostRefVector compute_union(const HostRefVector& a, const HostRefVector& b) noexcept;

using Mapping = std::unordered_map<TaggedNode, HostRefVector, TaggedNodeHasher, TaggedNodeEqualTo>;

struct MappingLogger final {
    std::reference_wrapper<const Mapping> mapping;
    std::string indent{"    "};
};
std::ostream& operator<<(std::ostream& os, const MappingLogger& mapping_logger) noexcept;

[[nodiscard]] const HostRefVector& get_hosts(const Mapping& mapping, TaggedNode node) noexcept;

class GenotypeToMappingConverter final {
  public:
    std::reference_wrapper<const TaggedNodeVector> nodes;
    std::reference_wrapper<const ConstrainedNodeVector> nodes_to_optimize;
    std::reference_wrapper<const HostVector> hosts;
    std::reference_wrapper<const EquivalenceMap> equivalences;
    std::reference_wrapper<const TrioVector> trios;

  public:
    [[nodiscard]] Mapping convert(const Genotype& genotype) const noexcept;

  private:
    [[nodiscard]] HostRefVector get_node_hosts(const Genotype& genotype, TaggedNode node) const noexcept;
    [[nodiscard]] HostRefVector get_optimized_node_hosts(const Genotype& genotype, ConstrainedNode node) const noexcept;
    [[nodiscard]] HostRefVector get_nonoptimized_node_hosts(const Genotype& genotype, TaggedNode node) const noexcept;
};

[[nodiscard]] Mapping convert_genotype_to_mapping(const Genotype& genotype,
                                                  const ConstrainedNodeVector& nodes_to_optimize,
                                                  const HostVector& hosts,
                                                  const TaggedNodeVector& nodes,
                                                  const EquivalenceMap& eqivalences,
                                                  const TrioVector& trios) noexcept;

[[nodiscard]] Mapping convert_genotype_to_mapping(const Genotype& genotype, const MAO::Configuration& config) noexcept;

}

#endif
