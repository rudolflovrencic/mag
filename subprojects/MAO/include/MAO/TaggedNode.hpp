#ifndef MAO_TAGGEDNODE_HPP
#define MAO_TAGGEDNODE_HPP

#include "MAO/UniqueValueVector.hpp"
#include "MAO/ConstrainedNode.hpp"

#include <MAL.hpp>

#include <variant>
#include <functional>
#include <type_traits>
#include <optional>

namespace MAO {

using TaggedNode = std::variant<std::reference_wrapper<const MAL::AST::VariableDeclaration>,
                                std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>,
                                std::reference_wrapper<const MAL::AST::AdditionExpression::Element>,
                                std::reference_wrapper<const MAL::AST::ComparisonExpression::Element>,
                                std::reference_wrapper<const MAL::AST::BooleanOrExpression::Element>,
                                std::reference_wrapper<const MAL::AST::BooleanAndExpression::Element>,
                                std::reference_wrapper<const MAL::AST::NumericLiteral>,
                                ArrayVariableLength>;
static_assert(std::is_trivially_copyable_v<TaggedNode>);

struct TaggedNodeEqualTo final {
    [[nodiscard]] bool operator()(TaggedNode lhs, TaggedNode rhs) const noexcept;
};

struct TaggedNodeHasher final {
    [[nodiscard]] std::size_t operator()(TaggedNode tagged_node) const noexcept;
};

std::ostream& operator<<(std::ostream& os, TaggedNode tagged_node) noexcept;

using TaggedNodeVector = UniqueValueVector<TaggedNode, TaggedNodeEqualTo>;

class TaggedNodeExtractor final {
  public:
    TaggedNodeVector result;

  public:
    TaggedNodeExtractor() noexcept;

    void operator()(const MAL::AST::Assignment& assignment) noexcept;
    void operator()(const MAL::AST::Send& send) noexcept;
    void operator()(const MAL::AST::Receive& receive) noexcept;
    void operator()(const MAL::AST::Print& print) noexcept;
    void operator()(const MAL::AST::IfStatement& if_statement) noexcept;
    void operator()(const MAL::AST::ForStatement& for_statement) noexcept;

    void operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept;
    void operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept;
    void operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept;
    void operator()(const MAL::AST::AdditionExpression& add_expr) noexcept;
    void operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept;
    void operator()(const MAL::AST::PrefixExpression& pref_expr) noexcept;

    void operator()(const MAL::AST::VariableReference& var_ref) noexcept;

    void apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept;
};

[[nodiscard]] TaggedNodeVector extract_tagged_nodes(const std::vector<MAL::AST::Statement>& ast) noexcept;

[[nodiscard]] TaggedNode get_top_level_tagged_node(const MAL::AST::BooleanOrExpression& or_expr) noexcept;
[[nodiscard]] TaggedNode get_top_level_tagged_node(const MAL::AST::BooleanAndExpression& ae) noexcept;
[[nodiscard]] TaggedNode get_top_level_tagged_node(const MAL::AST::ComparisonExpression& ce) noexcept;
[[nodiscard]] TaggedNode get_top_level_tagged_node(const MAL::AST::AdditionExpression& ae) noexcept;
[[nodiscard]] TaggedNode get_top_level_tagged_node(const MAL::AST::MultiplicationExpression& me) noexcept;
[[nodiscard]] TaggedNode get_top_level_tagged_node(const MAL::AST::PrefixExpression& pref_expr) noexcept;

[[nodiscard]] std::optional<ConstrainedNode> to_constrained_node(TaggedNode tagged_node) noexcept;
[[nodiscard]] ConstrainedNodeVector get_constrained_nodes(const TaggedNodeVector& tagged_nodes) noexcept;

[[nodiscard]] TaggedNode to_tagged_node(ConstrainedNode constrained_node) noexcept;

[[nodiscard]] std::optional<std::size_t> get_node_index_if_present(const ConstrainedNodeVector& constrained_nodes,
                                                                   TaggedNode tagged_node) noexcept;

}

#endif
