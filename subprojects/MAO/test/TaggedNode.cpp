#include <boost/test/unit_test.hpp>

#include "MAO/TaggedNode.hpp"

BOOST_AUTO_TEST_SUITE(tagged_node_extraction);

BOOST_AUTO_TEST_CASE(basic_code_with_five_tagged_nodes_in_five_extracted_tagged_nodes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int;
        var c int = a[42] + b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes = MAO::extract_tagged_nodes(app_ast);

    BOOST_REQUIRE_EQUAL(tagged_nodes.size(), 5);
    {
        const auto* const a_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[0]);
        BOOST_REQUIRE_NE(a_decl, nullptr);
        BOOST_CHECK_EQUAL(a_decl->get().variable_name, "a");
    }
    {
        const auto* const b_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[1]);
        BOOST_REQUIRE_NE(b_decl, nullptr);
        BOOST_CHECK_EQUAL(b_decl->get().variable_name, "b");
    }
    {
        const auto* const c_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[2]);
        BOOST_REQUIRE_NE(c_decl, nullptr);
        BOOST_CHECK_EQUAL(c_decl->get().variable_name, "c");
    }
    {
        const auto* const num_lit =
            std::get_if<std::reference_wrapper<const MAL::AST::NumericLiteral>>(&tagged_nodes[3]);
        BOOST_REQUIRE_NE(num_lit, nullptr);
        const auto* const value = boost::get<int>(&num_lit->get());
        BOOST_REQUIRE_NE(value, nullptr);
        BOOST_CHECK_EQUAL(*value, 42);
    }
    {
        const auto* const add_element =
            std::get_if<std::reference_wrapper<const MAL::AST::AdditionExpression::Element>>(&tagged_nodes[4]);
        BOOST_CHECK_NE(add_element, nullptr);
    }
}

BOOST_AUTO_TEST_SUITE_END();
