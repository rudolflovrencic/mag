#include <boost/test/unit_test.hpp>

#include "MAO/Equivalence.hpp"
#include "MAO/TaggedNode.hpp"

BOOST_AUTO_TEST_SUITE(equivalence_extraction);

BOOST_AUTO_TEST_CASE(code_with_two_assignments_results_in_two_equivalences)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int;
        var c int = a[42] + b;
        b = c;
        send c, b;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes = MAO::extract_tagged_nodes(app_ast);
    const auto equivalences = MAO::extract_equivalences(app_ast);

    const auto* const b_decl =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[1]);
    BOOST_REQUIRE_NE(b_decl, nullptr);
    BOOST_CHECK_EQUAL(b_decl->get().variable_name, "b");

    const auto* const c_decl =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[2]);
    BOOST_REQUIRE_NE(c_decl, nullptr);
    BOOST_CHECK_EQUAL(c_decl->get().variable_name, "c");

    BOOST_REQUIRE_EQUAL(equivalences.size(), 2);
    {
        const auto it = equivalences.find(*c_decl);
        BOOST_REQUIRE(it != equivalences.cend());
        const auto& assigned_nodes = it->second;
        BOOST_REQUIRE_EQUAL(assigned_nodes.size(), 1);
        const auto* const add_el =
            std::get_if<std::reference_wrapper<const MAL::AST::AdditionExpression::Element>>(&assigned_nodes[0]);
        BOOST_CHECK(add_el);
    }
    {
        const auto it = equivalences.find(*b_decl);
        BOOST_REQUIRE(it != equivalences.cend());
        const auto& assigned_nodes = it->second;
        BOOST_REQUIRE_EQUAL(assigned_nodes.size(), 1);
        const auto* const c_decl =
            std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&assigned_nodes[0]);
        BOOST_REQUIRE(c_decl);
        BOOST_CHECK_EQUAL(c_decl->get().variable_name, "c");
    }
}

BOOST_AUTO_TEST_CASE(self_assignment_results_in_no_equivalences)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int;
        a = a;
        send a;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes = MAO::extract_tagged_nodes(app_ast);
    const auto equivalences = MAO::extract_equivalences(app_ast);

    BOOST_CHECK(equivalences.empty());
}

BOOST_AUTO_TEST_CASE(code_with_loop_and_branch_results_two_equivalences_with_two_assigned_nodes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int, var c int;
        var d int = b;
        for (var i int = 0; i < len(a); i = i + 1) {
            if (a[i] > 10) {
                d = c;
            }
        }
        send b;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes = MAO::extract_tagged_nodes(app_ast);
    const auto equivalences = MAO::extract_equivalences(app_ast);

    const auto* const b_decl =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[1]);
    BOOST_REQUIRE_NE(b_decl, nullptr);
    BOOST_CHECK_EQUAL(b_decl->get().variable_name, "b");

    const auto* const c_decl =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[2]);
    BOOST_REQUIRE_NE(c_decl, nullptr);
    BOOST_CHECK_EQUAL(c_decl->get().variable_name, "c");

    const auto* const d_decl =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[3]);
    BOOST_REQUIRE_NE(d_decl, nullptr);
    BOOST_CHECK_EQUAL(d_decl->get().variable_name, "d");

    const auto* const i_decl =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&tagged_nodes[4]);
    BOOST_REQUIRE_NE(i_decl, nullptr);
    BOOST_CHECK_EQUAL(i_decl->get().variable_name, "i");

    BOOST_REQUIRE_EQUAL(equivalences.size(), 2);
    {
        const auto it = equivalences.find(*d_decl);
        BOOST_REQUIRE(it != equivalences.cend());
        const auto& assigned_nodes = it->second;
        BOOST_REQUIRE_EQUAL(assigned_nodes.size(), 2);
        {
            const auto* const decl =
                std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&assigned_nodes[0]);
            BOOST_REQUIRE(decl);
            BOOST_CHECK_EQUAL(decl->get().variable_name, "b");
        }
        {
            const auto* const decl =
                std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&assigned_nodes[1]);
            BOOST_REQUIRE(decl);
            BOOST_CHECK_EQUAL(decl->get().variable_name, "c");
        }
    }
    {
        const auto it = equivalences.find(*i_decl);
        BOOST_REQUIRE(it != equivalences.cend());
        const auto& assigned_nodes = it->second;
        BOOST_REQUIRE_EQUAL(assigned_nodes.size(), 2);
        {
            const auto* const num_lit =
                std::get_if<std::reference_wrapper<const MAL::AST::NumericLiteral>>(&assigned_nodes[0]);
            BOOST_REQUIRE(num_lit);
            const auto* const num_lit_int = boost::get<int>(&num_lit->get());
            BOOST_REQUIRE(num_lit_int);
            BOOST_CHECK_EQUAL(*num_lit_int, 0);
        }
        {
            const auto* const add_el =
                std::get_if<std::reference_wrapper<const MAL::AST::AdditionExpression::Element>>(&assigned_nodes[1]);
            BOOST_CHECK(add_el);
        }
    }
}

BOOST_AUTO_TEST_SUITE_END();
