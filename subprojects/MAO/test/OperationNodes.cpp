#include <boost/test/unit_test.hpp>

#include "MAO/OperationNodes.hpp"
#include "MAO/TaggedNode.hpp"

BOOST_AUTO_TEST_SUITE(operation_nodes_extraction);

BOOST_AUTO_TEST_CASE(code_with_single_multiplication_expression_results_in_a_single_trio)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int;
        var c int = a[42] * b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 3);

    BOOST_REQUIRE_EQUAL(operation_nodes_vector.size(), 1);
    const auto* const trio = std::get_if<MAO::Trio>(&operation_nodes_vector[0]);
    BOOST_REQUIRE(trio);
    BOOST_CHECK(trio->contains(nodes_to_optimize[0]));
    BOOST_CHECK(trio->contains(nodes_to_optimize[1]));
    BOOST_CHECK(trio->contains(nodes_to_optimize[2]));
}

BOOST_AUTO_TEST_CASE(code_with_same_node_in_multiplication_expression_results_in_no_operation_nodes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int;
        var b int = a * a;
        send b;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 2);

    BOOST_REQUIRE(operation_nodes_vector.empty());
}

BOOST_AUTO_TEST_CASE(code_with_literal_multiplication_results_in_no_operation_nodes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int;
        var c int = 23 * b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);

    BOOST_CHECK(operation_nodes_vector.empty());
}

BOOST_AUTO_TEST_CASE(code_with_non_constrained_node_multiplication_results_in_single_duo)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int;
        var c int = a * b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes = MAO::extract_tagged_nodes(app_ast);
    auto nodes_to_optimize  = MAO::get_constrained_nodes(tagged_nodes);
    nodes_to_optimize.erase_if([](const MAO::ConstrainedNode& node) noexcept -> bool {
        if (const auto* const var_decl =
                std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&node)) {
            return var_decl->get().variable_name == "a";
        }
        return false;
    });
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 2);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);

    BOOST_REQUIRE_EQUAL(operation_nodes_vector.size(), 1);
    const auto* const duo = std::get_if<MAO::Duo>(&operation_nodes_vector[0]);
    BOOST_REQUIRE(duo);
    BOOST_CHECK(duo->contains(nodes_to_optimize[0]));
    BOOST_CHECK(duo->contains(nodes_to_optimize[1]));
}

BOOST_AUTO_TEST_CASE(code_with_multiplication_and_addition_results_in_two_duos)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int, var d int;
        var c int = a * b + d;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 4);

    BOOST_REQUIRE_EQUAL(operation_nodes_vector.size(), 2);
    {
        const auto* const duo = std::get_if<MAO::Duo>(&operation_nodes_vector[0]);
        BOOST_REQUIRE(duo);
        BOOST_CHECK(duo->contains(nodes_to_optimize[0]));
        BOOST_CHECK(duo->contains(nodes_to_optimize[1]));
    }
    {
        const auto* const duo = std::get_if<MAO::Duo>(&operation_nodes_vector[1]);
        BOOST_REQUIRE(duo);
        BOOST_CHECK(duo->contains(nodes_to_optimize[2]));
        BOOST_CHECK(duo->contains(nodes_to_optimize[3]));
    }
}

BOOST_AUTO_TEST_CASE(code_with_multiplication_and_addition_with_literals_results_in_two_duos)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int, var d int;
        var c int = a * b + 43 + d + 18 * 28;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 4);

    BOOST_REQUIRE_EQUAL(operation_nodes_vector.size(), 2);
    {
        const auto* const duo = std::get_if<MAO::Duo>(&operation_nodes_vector[0]);
        BOOST_REQUIRE(duo);
        BOOST_CHECK(duo->contains(nodes_to_optimize[0]));
        BOOST_CHECK(duo->contains(nodes_to_optimize[1]));
    }
    {
        const auto* const duo = std::get_if<MAO::Duo>(&operation_nodes_vector[1]);
        BOOST_REQUIRE(duo);
        BOOST_CHECK(duo->contains(nodes_to_optimize[2]));
        BOOST_CHECK(duo->contains(nodes_to_optimize[3]));
    }
}

BOOST_AUTO_TEST_CASE(code_with_boolean_expression_results_in_no_operation_nodes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a bool, var b bool;
        var c bool = a && b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 3);

    BOOST_REQUIRE(operation_nodes_vector.empty());
}

BOOST_AUTO_TEST_CASE(code_with_same_node_in_addition_expression_assignment_results_in_no_operation_nodes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int;
        a = a + b;
        send a;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes           = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize      = MAO::get_constrained_nodes(tagged_nodes);
    const auto operation_nodes_vector = MAO::extract_operation_nodes(app_ast, nodes_to_optimize);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 2);

    BOOST_REQUIRE(operation_nodes_vector.empty());
}

BOOST_AUTO_TEST_SUITE_END();
