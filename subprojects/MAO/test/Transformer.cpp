#include <boost/test/unit_test.hpp>

#include "MAO/Transformer.hpp"
#include "MAO/TaggedNode.hpp"

BOOST_AUTO_TEST_SUITE(transformer);

BOOST_AUTO_TEST_SUITE(equivalence);

BOOST_AUTO_TEST_CASE(encoding_results_in_expected_bitset)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var i int;
        i = a;
        var c int = a;
        send a;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize = MAO::get_constrained_nodes(tagged_nodes);
    const auto equivalences      = MAO::extract_equivalences(app_ast);

    const auto encoded_equivalences =
        MAO::EquivalenceComplianceTransformer::encode_eqivalences(nodes_to_optimize, equivalences);

    BOOST_CHECK_EQUAL(encoded_equivalences,
                      boost::dynamic_bitset<>{std::string{
                          // clang-format off
                          //     cia
                                "001" // c
                                "001" // i
                                "000" // a
                          // clang-format on
                      }});
}

BOOST_AUTO_TEST_CASE(genotype_transformation_results_in_expected_genotypes)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int;
         var b int = a;
         send b;
     )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize = MAO::get_constrained_nodes(tagged_nodes);
    const auto equivalences      = MAO::extract_equivalences(app_ast);

    static constexpr std::size_t n_hosts{2};
    const MAO::EquivalenceComplianceTransformer t{n_hosts, nodes_to_optimize, equivalences};

    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"0101"}}), MAO::Genotype{std::string{"0101"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"1010"}}), MAO::Genotype{std::string{"1010"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"0111"}}), MAO::Genotype{std::string{"0111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"1111"}}), MAO::Genotype{std::string{"1111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"1101"}}), MAO::Genotype{std::string{"1111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"1110"}}), MAO::Genotype{std::string{"1111"}});
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(indexing);

BOOST_AUTO_TEST_CASE(encoding_results_in_expected_bitset)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int[], var i int;
         send a[i];
     )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    const auto indexing_information = MAO::extract_indexing_information(app_ast, constrained_nodes);

    const auto encoded_indexing_information =
        MAO::IndexingInformationComplianceTransformer::encode_indexing_information(constrained_nodes,
                                                                                   indexing_information);

    BOOST_REQUIRE_EQUAL(encoded_indexing_information, boost::dynamic_bitset<>{std::string{"0010"}});
}

BOOST_AUTO_TEST_CASE(genotype_transformation_results_in_expected_genotypes)
{
    static constexpr std::string_view app_mal_code{R"(
         receive var a int[], var b int;
         send a[b];
     )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    const auto indexing_information = MAO::extract_indexing_information(app_ast, constrained_nodes);

    static constexpr std::size_t n_hosts{2};
    const MAO::IndexingInformationComplianceTransformer t{n_hosts, constrained_nodes, indexing_information};

    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"0101"}}), MAO::Genotype{std::string{"0101"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"0111"}}), MAO::Genotype{std::string{"1111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"1011"}}), MAO::Genotype{std::string{"1111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"1001"}}), MAO::Genotype{std::string{"1101"}});
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(trio);

BOOST_AUTO_TEST_CASE(genotype_transformation_results_in_expected_genotypes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int;
        var c int = a + b;
        send c;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const MAO::HostVector hosts{
        {"host1", 10},
        {"host2", 20},
        {"host3", 30},
    };

    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize = MAO::get_constrained_nodes(tagged_nodes);
    const auto equivalences      = MAO::extract_equivalences(app_ast);
    const auto trios             = MAO::extract_trios(app_ast);
    BOOST_REQUIRE_EQUAL(nodes_to_optimize.size(), 3);

    MAO::TrioTransformer t{hosts, nodes_to_optimize, trios, equivalences};

    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"001001001"}}), MAO::Genotype{std::string{"001001001"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"001001011"}}), MAO::Genotype{std::string{"001001011"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"001011011"}}), MAO::Genotype{std::string{"011011011"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"101011011"}}), MAO::Genotype{std::string{"111011011"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"100011001"}}), MAO::Genotype{std::string{"101011001"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"110101011"}}), MAO::Genotype{std::string{"111111111"}});
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(len_function_call);

BOOST_AUTO_TEST_CASE(genotype_transformation_results_in_expected_genotypes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[];
        var b int = len(a);
        send b;
    )"};
    const auto app_ast = MAL::parse_and_check_semantics(app_mal_code);

    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize = MAO::get_constrained_nodes(tagged_nodes);

    static constexpr std::size_t n_hosts{2};
    const MAO::LenFunctionCallInformationComplianceTransformer t{n_hosts, nodes_to_optimize};

    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"010101"}}), MAO::Genotype{std::string{"010101"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"101010"}}), MAO::Genotype{std::string{"101010"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"010111"}}), MAO::Genotype{std::string{"110111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"100111"}}), MAO::Genotype{std::string{"110111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"110111"}}), MAO::Genotype{std::string{"110111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"111111"}}), MAO::Genotype{std::string{"111111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"101111"}}), MAO::Genotype{std::string{"111111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"101101"}}), MAO::Genotype{std::string{"111101"}});
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE(flow_control);

BOOST_AUTO_TEST_CASE(encoding_results_in_expected_bitset)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a bool, var b int, var c int;
        if (a) {
            send b;
        }
        send c;
     )"};

    const auto app_ast                  = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes             = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize        = MAO::get_constrained_nodes(tagged_nodes);
    const auto flow_control_information = MAO::extract_flow_control_information(app_ast);

    const auto encoded_flow_control_information =
        MAO::FlowControlInformationComplianceTransformer::encode_flow_control_information(nodes_to_optimize,
                                                                                          flow_control_information);

    BOOST_REQUIRE_EQUAL(encoded_flow_control_information,
                        boost::dynamic_bitset<>{std::string{
                            // clang-format off
                            //   cba
                                "000" // c
                                "000" // b
                                "010" // a
                            // clang-format on
                        }});
}

BOOST_AUTO_TEST_CASE(genotype_transformation_results_in_expected_genotypes)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b bool;
        if (b) {
            if (len(a) > 0) {
                send a[1];
            } 
        }
    )"};

    const auto app_ast                  = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes             = MAO::extract_tagged_nodes(app_ast);
    const auto nodes_to_optimize        = MAO::get_constrained_nodes(tagged_nodes);
    const auto flow_control_information = MAO::extract_flow_control_information(app_ast);

    static constexpr std::size_t n_hosts{2};
    const MAO::FlowControlInformationComplianceTransformer t{n_hosts, nodes_to_optimize, flow_control_information};

    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"010101"}}), MAO::Genotype{std::string{"010101"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"101010"}}), MAO::Genotype{std::string{"101010"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"101011"}}), MAO::Genotype{std::string{"101111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"100111"}}), MAO::Genotype{std::string{"101111"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"110110"}}), MAO::Genotype{std::string{"111110"}});
    BOOST_CHECK_EQUAL(t.transform(MAO::Genotype{std::string{"100101"}}), MAO::Genotype{std::string{"101101"}});
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_SUITE_END();
