#include <boost/test/unit_test.hpp>

#include "MAO/Indexing.hpp"
#include "MAO/TaggedNode.hpp"
#include "MAO/ConstrainedNode.hpp"

BOOST_AUTO_TEST_SUITE(indexing);

BOOST_AUTO_TEST_CASE(literal_results_in_empty_information)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[];
        send a[1];
    )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    const auto indexing_info = MAO::extract_indexing_information(app_ast, constrained_nodes);

    BOOST_REQUIRE_EQUAL(indexing_info.size(), 0);
}

BOOST_AUTO_TEST_CASE(addition_expression_results_in_empty_information)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int, var b int, var c int[];
        var x int = c[a + b];
        send x;
    )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    const auto indexing_info = MAO::extract_indexing_information(app_ast, constrained_nodes);

    BOOST_REQUIRE_EQUAL(indexing_info.size(), 0);
}

BOOST_AUTO_TEST_CASE(doubly_nested_index_results_in_two_sets)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int[], var c int[]; 
        var x int = a[b[c[1]]];
        send x;
    )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 4);
    const auto* const a =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&constrained_nodes[0]);
    BOOST_REQUIRE_NE(a, nullptr);
    const auto* const b =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&constrained_nodes[1]);
    BOOST_REQUIRE_NE(b, nullptr);
    const auto* const c =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&constrained_nodes[2]);
    BOOST_REQUIRE_NE(c, nullptr);

    const auto indexing_info = MAO::extract_indexing_information(app_ast, constrained_nodes);

    BOOST_CHECK_EQUAL(indexing_info.size(), 2);
    {
        const auto it = indexing_info.find(*a);
        BOOST_REQUIRE(it != indexing_info.cend());
        BOOST_CHECK(it->second.contains(*b));
        BOOST_CHECK(it->second.contains(*c));
    }
    {
        const auto it = indexing_info.find(*b);
        BOOST_REQUIRE(it != indexing_info.cend());
        BOOST_CHECK(it->second.contains(*c));
    }
}

BOOST_AUTO_TEST_CASE(doubly_nested_index_with_multiplication_results_in_a_single_set)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[], var b int[], var c int, var x int; 
        var y int = a[b[c] * x];
    )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    BOOST_REQUIRE_EQUAL(constrained_nodes.size(), 5);
    const auto* const b =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&constrained_nodes[1]);
    BOOST_REQUIRE_NE(b, nullptr);
    const auto* const c =
        std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&constrained_nodes[2]);
    BOOST_REQUIRE_NE(c, nullptr);

    const auto indexing_info = MAO::extract_indexing_information(app_ast, constrained_nodes);

    BOOST_CHECK_EQUAL(indexing_info.size(), 1);
    {
        const auto it = indexing_info.find(*b);
        BOOST_REQUIRE(it != indexing_info.end());
        BOOST_CHECK(it->second.contains(*c));
    }
}

BOOST_AUTO_TEST_CASE(self_indexing_results_in_empty_indexing_information)
{
    static constexpr std::string_view app_mal_code{R"(
        receive var a int[];
        send a[a[1]];
    )"};
    const auto app_ast           = MAL::parse_and_check_semantics(app_mal_code);
    const auto tagged_nodes      = MAO::extract_tagged_nodes(app_ast);
    const auto constrained_nodes = MAO::get_constrained_nodes(tagged_nodes);

    const auto indexing_info = MAO::extract_indexing_information(app_ast, constrained_nodes);

    BOOST_REQUIRE_EQUAL(indexing_info.size(), 0);
}

BOOST_AUTO_TEST_SUITE_END();
