#include "MAO/Indexing.hpp"

#include "MAO/Visitor.hpp"

#include <cassert>
#include <exception>

namespace MAO {

StatementIndexingInformationExtractor::StatementIndexingInformationExtractor(
    const ConstrainedNodeVector& nodes_to_optimize) noexcept
        : nodes_to_optimize{nodes_to_optimize}
{
    static constexpr std::size_t reserve_size{16};
    result.reserve(reserve_size);
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::Assignment& assignment) noexcept
{
    boost::apply_visitor(
        Visitor{
            [this](const MAL::AST::BooleanOrExpression& or_expr) noexcept -> void { operator()(or_expr); },
            [this](const MAL::AST::ArrayLiteral& arr_lit) noexcept -> void {
                operator()(arr_lit.value);
                operator()(arr_lit.size);
            },
        },
        assignment.right_side);
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::Send& send) noexcept
{
    for (const auto& var_ref : send) { operator()(var_ref); }
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::Receive& /*receive*/) const noexcept {}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::Print& /*print*/) const noexcept
{
    // Print statement should never be a part of application MAL code.
    assert(false);
    std::terminate();
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::IfStatement& if_statement) noexcept
{
    operator()(if_statement.condition);
    for (const auto& statement : if_statement.then_statements) { boost::apply_visitor(*this, statement); }
    for (const auto& statement : if_statement.else_statements) { boost::apply_visitor(*this, statement); }
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::ForStatement& for_statement) noexcept
{
    if (for_statement.init_statement) { operator()(*for_statement.init_statement); }
    operator()(for_statement.condition);
    if (for_statement.step_statement) { operator()(*for_statement.step_statement); }
    for (const auto& statement : for_statement.body) { boost::apply_visitor(*this, statement); }
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    operator()(or_expr.first_expression);
    for (const auto& elem : or_expr.other_expressions) { operator()(elem.boolean_and_expression); }
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept
{
    operator()(and_expr.first_expression);
    for (const auto& elem : and_expr.other_expressions) { operator()(elem.comparison_expression); }
}

void StatementIndexingInformationExtractor::operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept
{
    operator()(comp_expr.first_expression);
    for (const auto& elem : comp_expr.other_expressions) { operator()(elem.addition_expression); }
}

ConstrainedNodeVector
StatementIndexingInformationExtractor::operator()(const MAL::AST::AdditionExpression& add_expr) noexcept
{
    auto indexing_nodes = operator()(add_expr.first_expression);
    if (!add_expr.other_expressions.empty()) { indexing_nodes.clear(); }
    for (const auto& elem : add_expr.other_expressions) { operator()(elem.multiplication_expression); }
    return indexing_nodes;
}

ConstrainedNodeVector
StatementIndexingInformationExtractor::operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept
{
    auto indexing_nodes = operator()(mul_expr.first_expression);
    if (!mul_expr.other_expressions.empty()) { indexing_nodes.clear(); }
    for (const auto& elem : mul_expr.other_expressions) { operator()(elem.prefix_expression); }
    return indexing_nodes;
}

ConstrainedNodeVector
StatementIndexingInformationExtractor::operator()(const MAL::AST::PrefixExpression& pref_expr) noexcept
{
    return boost::apply_visitor(*this, pref_expr.primary_expression);
}

ConstrainedNodeVector
StatementIndexingInformationExtractor::operator()(const MAL::AST::NumericLiteral& /*num_lit*/) noexcept
{
    return ConstrainedNodeVector{};
}

ConstrainedNodeVector StatementIndexingInformationExtractor::operator()(const MAL::AST::VariableReference& vr) noexcept
{
    assert(vr.variable_identifier.declaration);

    ConstrainedNodeVector ret;
    if (vr.index) {
        const auto& declaration   = *vr.variable_identifier.declaration;
        const auto indexing_nodes = operator()(*vr.index);
        add(declaration, indexing_nodes);
        for (const auto& indexing_node : indexing_nodes) { ret.push_back(indexing_node); }
    }
    ret.push_back(*vr.variable_identifier.declaration);
    return ret;
}

ConstrainedNodeVector
StatementIndexingInformationExtractor::operator()(const MAL::AST::LenFunctionCall& len_call) noexcept
{
    return ConstrainedNodeVector{MAO::ArrayVariableLength{len_call}};
}

void StatementIndexingInformationExtractor::add(
    std::reference_wrapper<const MAL::AST::VariableDeclaration> indexed_node,
    const ConstrainedNodeVector& indexing_nodes) noexcept
{
    if (nodes_to_optimize.get().contains(indexed_node)) {
        for (const auto indexing_node : indexing_nodes) {
            if (!ConstrainedNodeEqualTo{}(indexed_node, indexing_node) &&
                nodes_to_optimize.get().contains(indexing_node)) {
                result[indexed_node].push_back(indexing_node);
            }
        }
    }
}

IndexingInformation extract_indexing_information(const std::vector<MAL::AST::Statement>& ast,
                                                 const ConstrainedNodeVector& nodes_to_optimize) noexcept
{
    StatementIndexingInformationExtractor extractor{nodes_to_optimize};
    for (const auto& statement : ast) { boost::apply_visitor(extractor, statement); }
    return std::move(extractor.result);
}

}
