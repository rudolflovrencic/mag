#include "MAO/ExhaustiveSearchOptimizer.hpp"

#include <iostream>
#include <iomanip>

namespace MAO {

ExhaustiveSearchOptimizer::SimpleAdvancement::SimpleAdvancement(
    const HostVector& hosts,
    const ConstrainedNodeVector& nodes_to_optimize,
    const EquivalenceMap& equivalences,
    const IndexingInformation& indexing_information,
    const TrioVector& trios,
    const FlowControlInformation& flow_control_informatio) noexcept
        : transformers{
              EquivalenceComplianceTransformer{hosts.size(), nodes_to_optimize, equivalences},
              IndexingInformationComplianceTransformer{hosts.size(), nodes_to_optimize, indexing_information},
              TrioTransformer{hosts, nodes_to_optimize, trios, equivalences},
              LenFunctionCallInformationComplianceTransformer{hosts.size(), nodes_to_optimize},
              FlowControlInformationComplianceTransformer{hosts.size(), nodes_to_optimize, flow_control_informatio},
              NodeOnHostTransformer{hosts.size(), nodes_to_optimize.size()},
          }
{}

bool ExhaustiveSearchOptimizer::SimpleAdvancement::advance(Genotype& genotype)
{
    const auto n_hosts             = get_n_hosts();
    const auto n_nodes_to_optimize = get_n_nodes_to_optimize();

    assert(genotype.size() == n_nodes_to_optimize * n_hosts);
    assert(genotype.count() >= n_nodes_to_optimize);

    if (!non_transformed_genotype) { non_transformed_genotype = genotype; }

    for (std::size_t n{0}; n < n_nodes_to_optimize; ++n) {
        const auto var_begin_index = n * n_hosts;
        for (std::size_t h{0}; h < n_hosts; ++h) {
            const auto i = var_begin_index + h;
            if (!non_transformed_genotype->test(i)) {
                non_transformed_genotype->set(i);
                non_transformed_genotype->set(var_begin_index, h, false);
                genotype = *non_transformed_genotype; // Same size bitsets assign without allocation.
                genotype = transform(transformers, std::move(genotype));
                return true;
            }
        }
        non_transformed_genotype->set(var_begin_index, n_hosts, false);
        non_transformed_genotype->set(var_begin_index);
    }

    return false;
}

std::size_t ExhaustiveSearchOptimizer::SimpleAdvancement::get_n_hosts() const noexcept
{
    const auto& equivalence_transformer = std::get<0>(transformers);
    return equivalence_transformer.get_n_hosts();
}

std::size_t ExhaustiveSearchOptimizer::SimpleAdvancement::get_n_nodes_to_optimize() const noexcept
{
    const auto& equivalence_transformer = std::get<0>(transformers);
    return equivalence_transformer.get_n_nodes_to_optimize();
}

Genotype ExhaustiveSearchOptimizer::optimize(const Configuration& configuration)
{
    const auto n_hosts           = configuration.get_hosts().size();
    const auto nodes_to_optimize = configuration.get_nodes_to_optimize();

    Algorithm alg{std::make_unique<ConstraintEvaluator>(
                      nodes_to_optimize, configuration.get_constraints(), configuration.get_hosts()),
                  std::vector<std::unique_ptr<SHO::TerminationCriterion>>{},
                  std::make_unique<SimpleAdvancement>(configuration.get_hosts(),
                                                      nodes_to_optimize,
                                                      configuration.get_equivalences(),
                                                      configuration.get_indexing_information(),
                                                      configuration.get_trios(),
                                                      configuration.get_flow_control_information())};

    Genotype initial_genotype{nodes_to_optimize.size() * n_hosts}; // All declarations are mapped to the first host.
    for (std::size_t i{0}; i < initial_genotype.size(); i += n_hosts) { initial_genotype.set(i); }

    std::cout << "Initial:\n" << GenotypeLogger{initial_genotype, nodes_to_optimize, configuration.get_hosts()} << '\n';

    auto [met_criterion, best_individual, state] = alg.run(
        std::move(initial_genotype),
        [&nodes_to_optimize, &hosts = configuration.get_hosts()](const Algorithm::Individual& individual,
                                                                 const Algorithm::Individual& best_individual,
                                                                 const Algorithm::State& state) noexcept -> void {
            static constexpr unsigned long long log_frequency{1};
            if (state.iteration % log_frequency == 0) {
                std::cout << "Iteration: " << state.iteration << "\nCurrent (" << std::setprecision(4)
                          << -individual.get_fitness() << "):\n"
                          << GenotypeLogger{individual.solution, nodes_to_optimize, hosts} << "Best ("
                          << -best_individual.get_fitness() << "):\n"
                          << GenotypeLogger{best_individual.solution, nodes_to_optimize, hosts} << '\n';
            }
        });

    return std::move(best_individual.solution);
}

}
