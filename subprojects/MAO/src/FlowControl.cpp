#include "MAO/FlowControl.hpp"

#include "MAO/Visitor.hpp"

#include <cassert>
#include <exception>

namespace MAO {

namespace {

class RunningConditionVariableGuard final {
  private:
    std::reference_wrapper<TaggedNodeVector> running_condition_nodes;
    bool running_condition_node_added;

  public:
    RunningConditionVariableGuard(TaggedNodeVector& running_condition_nodes, TaggedNode node) noexcept;
    ~RunningConditionVariableGuard() noexcept;

    RunningConditionVariableGuard(const RunningConditionVariableGuard& o)                = delete;
    RunningConditionVariableGuard& operator=(const RunningConditionVariableGuard& o)     = delete;
    RunningConditionVariableGuard(RunningConditionVariableGuard&& o) noexcept            = delete;
    RunningConditionVariableGuard& operator=(RunningConditionVariableGuard&& o) noexcept = delete;
};

}

FlowControlInformationExtractor::FlowControlInformationExtractor() noexcept
{
    static constexpr std::size_t reserve_size{8};
    result.reserve(reserve_size);
    running_condition_nodes.reserve(reserve_size);
}

void FlowControlInformationExtractor::operator()(const MAL::AST::Assignment& assignment) noexcept
{
    boost::apply_visitor(
        Visitor{
            [this](const MAL::AST::VariableDeclaration& var_decl) noexcept -> void {
                add_for_all_running_condition_nodes(TaggedNode{var_decl});
            },
            [this](const MAL::AST::VariableReference& var_ref) noexcept -> void { operator()(var_ref); },
        },
        assignment.left_side);

    boost::apply_visitor(Visitor{
                             [this](const MAL::AST::BooleanOrExpression& boolean_or_expr) noexcept -> void {
                                 operator()(boolean_or_expr);
                             },
                             [this](const MAL::AST::ArrayLiteral& array_literal) noexcept -> void {
                                 operator()(array_literal.value);
                                 operator()(array_literal.size);
                             },
                         },
                         assignment.right_side);
}

void FlowControlInformationExtractor::operator()(const MAL::AST::Send& send) noexcept
{
    for (const auto& var_ref : send) { operator()(var_ref); }
}

void FlowControlInformationExtractor::operator()(const MAL::AST::Receive& /*receive*/) noexcept {}

void FlowControlInformationExtractor::operator()(const MAL::AST::Print& /*print*/) noexcept
{
    assert(false); // Print statement should never be a part of application MAL code.
    std::terminate();
}

void FlowControlInformationExtractor::operator()(const MAL::AST::IfStatement& if_statement) noexcept
{
    operator()(if_statement.condition);

    const RunningConditionVariableGuard guard{
        running_condition_nodes,
        get_top_level_tagged_node(if_statement.condition),
    };

    apply_to_all(if_statement.then_statements);
    apply_to_all(if_statement.else_statements);
}

void FlowControlInformationExtractor::operator()(const MAL::AST::ForStatement& for_statement) noexcept
{
    if (for_statement.init_statement) { operator()(*for_statement.init_statement); }
    operator()(for_statement.condition);
    if (for_statement.step_statement) { operator()(*for_statement.step_statement); }

    const RunningConditionVariableGuard guard{
        running_condition_nodes,
        get_top_level_tagged_node(for_statement.condition),
    };

    apply_to_all(for_statement.body);
}

void FlowControlInformationExtractor::operator()(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    operator()(or_expr.first_expression);
    for (const auto& element : or_expr.other_expressions) {
        add_for_all_running_condition_nodes(TaggedNode{element});
        operator()(element.boolean_and_expression);
    }
}

void FlowControlInformationExtractor::operator()(const MAL::AST::BooleanAndExpression& and_expr) noexcept
{
    operator()(and_expr.first_expression);
    for (const auto& element : and_expr.other_expressions) {
        add_for_all_running_condition_nodes(TaggedNode{element});
        operator()(element.comparison_expression);
    }
}

void FlowControlInformationExtractor::operator()(const MAL::AST::ComparisonExpression& comp_expr) noexcept
{
    operator()(comp_expr.first_expression);
    for (const auto& element : comp_expr.other_expressions) {
        add_for_all_running_condition_nodes(TaggedNode{element});
        operator()(element.addition_expression);
    }
}

void FlowControlInformationExtractor::operator()(const MAL::AST::AdditionExpression& add_expr) noexcept
{
    operator()(add_expr.first_expression);
    for (const auto& element : add_expr.other_expressions) {
        add_for_all_running_condition_nodes(TaggedNode{element});
        operator()(element.multiplication_expression);
    }
}

void FlowControlInformationExtractor::operator()(const MAL::AST::MultiplicationExpression& mul_expr) noexcept
{
    operator()(mul_expr.first_expression);
    for (const auto& element : mul_expr.other_expressions) {
        add_for_all_running_condition_nodes(TaggedNode{element});
        operator()(element.prefix_expression);
    }
}

void FlowControlInformationExtractor::operator()(const MAL::AST::PrefixExpression& pref_expr) noexcept
{
    boost::apply_visitor(Visitor{
                             [this](const MAL::AST::NumericLiteral& num_lit) noexcept -> void {
                                 add_for_all_running_condition_nodes(TaggedNode{num_lit});
                             },
                             [this](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> void {
                                 add_for_all_running_condition_nodes(ArrayVariableLength{len_fn_call});
                             },
                             [this](const MAL::AST::VariableReference& var_ref) noexcept -> void {
                                 const auto* const declaration = var_ref.variable_identifier.declaration;
                                 assert(declaration);
                                 add_for_all_running_condition_nodes(TaggedNode{*declaration});
                                 if (var_ref.index) { operator()(*var_ref.index); }
                             },
                         },
                         pref_expr.primary_expression);
}

void FlowControlInformationExtractor::operator()(const MAL::AST::VariableReference& var_ref) noexcept
{
    const auto* const declaration = var_ref.variable_identifier.declaration;
    assert(declaration);
    add_for_all_running_condition_nodes(TaggedNode{*declaration});

    if (var_ref.index) { operator()(*var_ref.index); }
}

void FlowControlInformationExtractor::add_for_all_running_condition_nodes(TaggedNode node) noexcept
{
    for (const auto running_condition_node : running_condition_nodes) {
        result[running_condition_node].push_back(node);
    }
}

void FlowControlInformationExtractor::apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    for (const auto& statement : ast) { boost::apply_visitor(*this, statement); }
}

FlowControlInformation extract_flow_control_information(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    FlowControlInformationExtractor extractor;
    extractor.apply_to_all(ast);
    return std::move(extractor.result);
}

namespace {

RunningConditionVariableGuard::RunningConditionVariableGuard(TaggedNodeVector& running_condition_nodes,
                                                             TaggedNode node) noexcept
        : running_condition_nodes{running_condition_nodes}, running_condition_node_added{false}
{
    const auto [it, inserted]    = running_condition_nodes.push_back(node);
    running_condition_node_added = inserted;
}

RunningConditionVariableGuard::~RunningConditionVariableGuard() noexcept
{
    if (running_condition_node_added) {
        assert(!running_condition_nodes.get().empty());
        running_condition_nodes.get().pop_back();
    }
}

}

}
