#include "MAO/ConstrainedNode.hpp"

#include "MAO/Visitor.hpp"

namespace MAO {

ArrayVariableLength::ArrayVariableLength(const MAL::AST::VariableDeclaration& variable_declaration) noexcept
        : variable_declaration{variable_declaration}
{
    assert(variable_declaration.is_array_declaration);
}

ArrayVariableLength::ArrayVariableLength(const MAL::AST::LenFunctionCall& len_fn_call) noexcept
        : ArrayVariableLength{
              [](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> const MAL::AST::VariableDeclaration& {
                  const auto* const declaration = len_fn_call.variable_reference.variable_identifier.declaration;
                  assert(declaration);
                  return *declaration;
              }(len_fn_call)}
{}

const MAL::AST::VariableDeclaration& ArrayVariableLength::get_variable_declaration() const noexcept
{
    return variable_declaration;
}

std::ostream& operator<<(std::ostream& os, ConstrainedNode constrained_node) noexcept
{
    std::visit(Visitor{
                   [&os](const MAL::AST::VariableDeclaration& variable_declaration) noexcept -> void {
                       os << variable_declaration.variable_name;
                   },
                   [&os](const ArrayVariableLength& variable_len) noexcept -> void {
                       os << "len(" << variable_len.get_variable_declaration().variable_name << ')';
                   },
               },
               constrained_node);
    return os;
}

bool ConstrainedNodeEqualTo::operator()(ConstrainedNode lhs, ConstrainedNode rhs) const noexcept
{
    return std::visit(
        [rhs](auto lhs) noexcept -> bool {
            using T                       = decltype(lhs);
            const auto* const rhs_ref_ptr = std::get_if<T>(&rhs);
            if (!rhs_ref_ptr) { return false; }
            if constexpr (std::is_same_v<T, ArrayVariableLength>) {
                return &lhs.get_variable_declaration() == &rhs_ref_ptr->get_variable_declaration();
            } else {
                const auto* const lhs_ptr = &lhs.get();
                return &rhs_ref_ptr->get() == lhs_ptr;
            }
        },
        lhs);
}

std::size_t ConstrainedNodeHasher::operator()(ConstrainedNode constrained_node) const noexcept
{
    return std::hash<const void*>{}(std::visit(
        Visitor{[](ArrayVariableLength array_length) noexcept -> const void* {
                    return &array_length.get_variable_declaration();
                },
                [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> const void* { return &var_decl; }},
        constrained_node));
}

std::size_t get_node_index(const ConstrainedNodeVector& constrained_nodes, ConstrainedNode node) noexcept
{
    const auto node_it = constrained_nodes.find(node);
    assert(node_it != constrained_nodes.end());
    return static_cast<std::size_t>(std::distance(constrained_nodes.begin(), node_it));
}

bool VariableDeclarationReferenceWrapperEqualTo::operator()(
    std::reference_wrapper<const MAL::AST::VariableDeclaration> lhs,
    std::reference_wrapper<const MAL::AST::VariableDeclaration> rhs) const noexcept
{
    return &lhs.get() == &rhs.get();
}

std::size_t VariableDeclarationReferenceWrapperHasher::operator()(
    std::reference_wrapper<const MAL::AST::VariableDeclaration> vd) const noexcept
{
    return std::hash<const MAL::AST::VariableDeclaration*>{}(&vd.get());
}

std::optional<ConstrainedNode> get_top_level_constrained_node(const MAL::AST::BooleanOrExpression& or_expr) noexcept
{
    return or_expr.other_expressions.empty() ? get_top_level_constrained_node(or_expr.first_expression) : std::nullopt;
}

std::optional<ConstrainedNode> get_top_level_constrained_node(const MAL::AST::BooleanAndExpression& ae) noexcept
{
    return ae.other_expressions.empty() ? get_top_level_constrained_node(ae.first_expression) : std::nullopt;
}

std::optional<ConstrainedNode> get_top_level_constrained_node(const MAL::AST::ComparisonExpression& ce) noexcept
{
    return ce.other_expressions.empty() ? get_top_level_constrained_node(ce.first_expression) : std::nullopt;
}

std::optional<ConstrainedNode> get_top_level_constrained_node(const MAL::AST::AdditionExpression& ae) noexcept
{
    return ae.other_expressions.empty() ? get_top_level_constrained_node(ae.first_expression) : std::nullopt;
}

std::optional<ConstrainedNode> get_top_level_constrained_node(const MAL::AST::MultiplicationExpression& me) noexcept
{
    return me.other_expressions.empty() ? get_top_level_constrained_node(me.first_expression) : std::nullopt;
}

std::optional<ConstrainedNode> get_top_level_constrained_node(const MAL::AST::PrefixExpression& pref_expr) noexcept
{
    return boost::apply_visitor(
        Visitor{
            [](const MAL::AST::NumericLiteral& /*num_lit*/) noexcept -> std::optional<ConstrainedNode> {
                return std::nullopt;
            },
            [](const MAL::AST::LenFunctionCall& len_fn_call) noexcept -> std::optional<ConstrainedNode> {
                return ArrayVariableLength{len_fn_call};
            },
            [](const MAL::AST::VariableReference& var_ref) noexcept -> std::optional<ConstrainedNode> {
                assert(var_ref.variable_identifier.declaration);
                return *var_ref.variable_identifier.declaration;
            },
        },
        pref_expr.primary_expression);
}

}
