#include "MAO/Transformer.hpp"

#include "MAO/Visitor.hpp"

namespace MAO {

namespace {

void make_available_on_all_hosts_in_cache(HostSetView node_hosts, const HostSetComputationCache& cache) noexcept;
void make_available_on_all_hosts_in_cache(HostSetView node_hosts, const HostSetComputationCache& cache) noexcept;

}

EquivalenceComplianceTransformer::EquivalenceComplianceTransformer(std::size_t n_hosts,
                                                                   const ConstrainedNodeVector& nodes_to_optimize,
                                                                   const EquivalenceMap& equivalences) noexcept
        : n_hosts{n_hosts},
          n_nodes_to_optimize{nodes_to_optimize.size()},
          encoded_equivalences{encode_eqivalences(nodes_to_optimize, equivalences)}
{}

Genotype EquivalenceComplianceTransformer::transform(Genotype genotype) const noexcept
{
    const auto n_nodes_to_optimize = get_n_nodes_to_optimize();

    assert(genotype.size() == n_nodes_to_optimize * n_hosts);

    for (std::size_t i{0}; i < n_nodes_to_optimize; ++i) {
        for (std::size_t j{0}; j < n_nodes_to_optimize; ++j) {
            if (is_node_i_assigned_node_j(i, j)) {
                const HostSetView i_hostset{genotype, n_hosts, i};
                const HostSetView j_hostset{genotype, n_hosts, j};
                for (std::size_t h{0}; h < n_hosts; ++h) {
                    if (i_hostset.test(h)) { j_hostset.set(h); }
                }
            }
        }
    }
    return genotype;
}

bool EquivalenceComplianceTransformer::is_node_i_assigned_node_j(std::size_t i, std::size_t j) const noexcept
{
    return encoded_equivalences.test(i * get_n_nodes_to_optimize() + j);
}

std::size_t EquivalenceComplianceTransformer::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t EquivalenceComplianceTransformer::get_n_nodes_to_optimize() const noexcept
{
    return n_nodes_to_optimize;
}

boost::dynamic_bitset<>
EquivalenceComplianceTransformer::encode_eqivalences(const ConstrainedNodeVector& nodes_to_optimize,
                                                     const EquivalenceMap& equivalences) noexcept
{
    const auto n_nodes_to_optimize = nodes_to_optimize.size();
    boost::dynamic_bitset<> encoded_equivalences{n_nodes_to_optimize * n_nodes_to_optimize};
    for (std::size_t i{0}; i < n_nodes_to_optimize; ++i) {
        if (const auto* const var_ref =
                std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&nodes_to_optimize[i])) {
            const auto equivalences_it = equivalences.find(*var_ref);
            if (equivalences_it != equivalences.cend()) {
                for (const auto assigned_node : equivalences_it->second) {
                    if (const auto node_to_optimize = to_constrained_node(assigned_node)) {
                        const auto j = get_node_index(nodes_to_optimize, *node_to_optimize);
                        encoded_equivalences.set(i * n_nodes_to_optimize + j);
                    }
                }
            }
        }
    }
    return encoded_equivalences;
}

IndexingInformationComplianceTransformer::IndexingInformationComplianceTransformer(
    std::size_t n_hosts,
    const ConstrainedNodeVector& nodes_to_optimize,
    const IndexingInformation& indexing_information)
        : n_hosts{n_hosts},
          n_nodes_to_optimize{nodes_to_optimize.size()},
          encoded_indexing_information{encode_indexing_information(nodes_to_optimize, indexing_information)}
{
    assert(n_hosts > 0);
}

Genotype IndexingInformationComplianceTransformer::transform(Genotype genotype) const noexcept
{
    assert(genotype.size() == n_nodes_to_optimize * n_hosts);

    for (std::size_t i{0}; i < n_nodes_to_optimize; ++i) {
        for (std::size_t j{0}; j < n_nodes_to_optimize; ++j) {
            if (is_node_i_indexed_by_node_j(i, j)) {
                const HostSetView i_hostset{genotype, n_hosts, i};
                const HostSetView j_hostset{genotype, n_hosts, j};
                for (std::size_t h{0}; h < n_hosts; ++h) {
                    if (i_hostset.test(h)) { j_hostset.set(h); }
                }
            }
        }
    }
    return genotype;
}

bool IndexingInformationComplianceTransformer::is_node_i_indexed_by_node_j(std::size_t i, std::size_t j) const noexcept
{
    return encoded_indexing_information.test(i * n_nodes_to_optimize + j);
}

std::size_t IndexingInformationComplianceTransformer::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t IndexingInformationComplianceTransformer::get_n_nodes_to_optimize() const noexcept
{
    return n_nodes_to_optimize;
}

boost::dynamic_bitset<> IndexingInformationComplianceTransformer::encode_indexing_information(
    const ConstrainedNodeVector& nodes_to_optimize, const IndexingInformation& indexing_information) noexcept
{
    const auto n_nodes_to_optimize = nodes_to_optimize.size();
    boost::dynamic_bitset<> encoded_indexing_information{n_nodes_to_optimize * n_nodes_to_optimize};
    for (std::size_t i{0}; i < n_nodes_to_optimize; ++i) {
        if (const auto* const indexed_var =
                std::get_if<std::reference_wrapper<const MAL::AST::VariableDeclaration>>(&nodes_to_optimize[i])) {
            const auto indexing_vars_it = indexing_information.find(*indexed_var);
            if (indexing_vars_it != indexing_information.cend()) {
                for (const auto indexing_node : indexing_vars_it->second) {
                    const auto j = get_node_index(nodes_to_optimize, indexing_node);
                    encoded_indexing_information.set(i * n_nodes_to_optimize + j);
                }
            }
        }
    }
    return encoded_indexing_information;
}

TrioTransformer::TrioTransformer(const HostVector& host_vector,
                                 const ConstrainedNodeVector& nodes_to_optimize,
                                 const TrioVector& trios,
                                 const EquivalenceMap& equivalences) noexcept
        : host_vector{host_vector},
          n_nodes_to_optimize{nodes_to_optimize.size()},
          encoded_operation_nodes_vector{encode_operation_nodes_vector(nodes_to_optimize, trios, equivalences)},
          cache{get_n_hosts()}
{}

Genotype TrioTransformer::transform(Genotype genotype) noexcept
{
    for (const auto [i, j, k] : encoded_operation_nodes_vector) {
        const HostSetView hs_i{genotype, get_n_hosts(), i};
        const HostSetView hs_k{genotype, get_n_hosts(), k};
        if (j) {
            const HostSetView hs_j{genotype, get_n_hosts(), *j};
            handle_trio(hs_i, hs_j, hs_k);
        } else if (get_n_hosts() == 2) {
            handle_duo(hs_i, hs_k);
        }
    }
    return genotype;
}

std::size_t TrioTransformer::get_n_hosts() const noexcept
{
    return host_vector.get().size();
}

std::size_t TrioTransformer::get_n_nodes_to_optimize() const noexcept
{
    return n_nodes_to_optimize;
}

auto TrioTransformer::encode_operation_nodes_vector(const ConstrainedNodeVector& nodes_to_optimize,
                                                    const TrioVector& trios,
                                                    const EquivalenceMap& equivalences) noexcept
    -> std::vector<EncodedOperationNodes>
{
    std::vector<EncodedOperationNodes> result;
    static constexpr std::size_t reserve_size{16};
    result.reserve(reserve_size);

    for (const auto trio : trios) {
        const auto first_index = get_node_index_if_present(nodes_to_optimize, trio.get_first());
        const auto third_index = get_node_index_if_present(nodes_to_optimize, trio.get_third());
        if (first_index && third_index) {
            std::optional<std::size_t> second_index;
            const auto it =
                std::find_if(equivalences.cbegin(),
                             equivalences.cend(),
                             [second = trio.get_second()](const EquivalenceMap::value_type& node) noexcept -> bool {
                                 return node.second.contains(to_outer_node(second));
                             });
            if (it != equivalences.cend()) { second_index = get_node_index_if_present(nodes_to_optimize, it->first); }
            result.push_back(EncodedOperationNodes{*first_index, second_index, *third_index});
        }
    }

    return result;
}

void TrioTransformer::handle_duo(HostSetView first, HostSetView second) noexcept
{
    assert(host_vector.get().size() == 2);

    if (cache.compute_intersection(first, second).none()) {
        const auto safer_host_index =
            host_vector.get()[0].get_trust() < host_vector.get()[1].get_trust() ? std::size_t{1} : std::size_t{0};

        // Setting both is not necessary as one of the nodes is surely already set on the safer host.
        first.set(safer_host_index);
        second.set(safer_host_index);
    }
}

void TrioTransformer::handle_trio(HostSetView first, HostSetView second, HostSetView third) noexcept
{
    cache.compute_intersection(first, second);
    make_available_on_all_hosts_in_cache(third, cache);

    cache.compute_intersection(first, third);
    make_available_on_all_hosts_in_cache(second, cache);

    cache.compute_intersection(second, third);
    make_available_on_all_hosts_in_cache(first, cache);
}

std::optional<std::size_t>
TrioTransformer::get_most_secure_host_with_multiple_elements(const HostVector& host_vector,
                                                             const HostSetComputationCache& intersection_cache) noexcept
{
    assert(host_vector.size() == intersection_cache.get_n_hosts());

    std::optional<std::size_t> result;
    for (std::size_t h{0}; h < host_vector.size(); ++h) {
        if (intersection_cache.test(h) && (!result || host_vector[h].get_trust() > host_vector[*result].get_trust())) {
            result = h;
        }
    }
    return result;
}

LenFunctionCallInformationComplianceTransformer::LenFunctionCallInformationComplianceTransformer(
    std::size_t n_hosts, const ConstrainedNodeVector& nodes_to_optimize) noexcept
        : n_hosts{n_hosts}
{
    encoded_len_vars.resize(nodes_to_optimize.size(), std::nullopt);
    for (std::size_t i{0}; i < nodes_to_optimize.size(); ++i) {
        const auto node = nodes_to_optimize[i];
        if (const auto* const array_var_len = std::get_if<ArrayVariableLength>(&node)) {
            const auto& declaration = array_var_len->get_variable_declaration();
            encoded_len_vars[i]     = get_node_index(nodes_to_optimize, ConstrainedNode{declaration});
        }
    }
}

Genotype LenFunctionCallInformationComplianceTransformer::transform(Genotype genotype) const noexcept
{
    const auto n_nodes_to_optimize = get_n_nodes_to_optimize();
    assert(n_hosts * n_nodes_to_optimize == genotype.size());

    for (std::size_t len_var_index{0}; len_var_index < n_nodes_to_optimize; ++len_var_index) {
        if (const auto var_decl_index_opt = encoded_len_vars[len_var_index]) {
            const HostSetView len_var_hostset{genotype, n_hosts, len_var_index};
            const HostSetView var_decl_hostset{genotype, n_hosts, *var_decl_index_opt};
            for (std::size_t h{0}; h < n_hosts; ++h) {
                if (var_decl_hostset.test(h)) { len_var_hostset.set(h); }
            }
        }
    }
    return genotype;
}

std::size_t LenFunctionCallInformationComplianceTransformer::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t LenFunctionCallInformationComplianceTransformer::get_n_nodes_to_optimize() const noexcept
{
    return encoded_len_vars.size();
}

FlowControlInformationComplianceTransformer::FlowControlInformationComplianceTransformer(
    std::size_t n_hosts,
    const ConstrainedNodeVector& nodes_to_optimize,
    const FlowControlInformation& flow_control_information) noexcept
        : n_hosts{n_hosts},
          n_nodes_to_optimize{nodes_to_optimize.size()},
          encoded_flow_control_information{encode_flow_control_information(nodes_to_optimize, flow_control_information)}
{}

Genotype FlowControlInformationComplianceTransformer::transform(Genotype genotype) const noexcept
{
    assert(genotype.size() == n_nodes_to_optimize * n_hosts);

    for (std::size_t i{0}; i < n_nodes_to_optimize; ++i) {
        for (std::size_t j{0}; j < n_nodes_to_optimize; ++j) {
            if (encoded_flow_control_information.test(i * n_nodes_to_optimize + j)) {
                const HostSetView control_node_hosts{genotype, n_hosts, i};
                const HostSetView affected_node_hosts{genotype, n_hosts, j};
                for (std::size_t h{0}; h < n_hosts; ++h) {
                    if (affected_node_hosts.test(h)) { control_node_hosts.set(h); }
                }
            }
        }
    }
    return genotype;
}

std::size_t FlowControlInformationComplianceTransformer::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t FlowControlInformationComplianceTransformer::get_n_nodes_to_optimize() const noexcept
{
    return n_nodes_to_optimize;
}

boost::dynamic_bitset<> FlowControlInformationComplianceTransformer::encode_flow_control_information(
    const ConstrainedNodeVector& nodes_to_optimize, const FlowControlInformation& flow_control_information) noexcept
{
    const auto n_nodes_to_optimize = nodes_to_optimize.size();
    boost::dynamic_bitset<> result{n_nodes_to_optimize * n_nodes_to_optimize};

    for (const auto& [condition_node, affected_nodes] : flow_control_information) {
        if (const auto condition_node_index = get_node_index_if_present(nodes_to_optimize, condition_node)) {
            for (const auto affected_node : affected_nodes) {
                if (const auto affected_node_index = get_node_index_if_present(nodes_to_optimize, affected_node)) {
                    result.set(*condition_node_index * n_nodes_to_optimize + *affected_node_index);
                }
            }
        }
    }

    return result;
}

NodeOnHostTransformer::NodeOnHostTransformer(std::size_t n_hosts, std::size_t n_nodes_to_optimize) noexcept
        : n_hosts{n_hosts}, n_nodes_to_optimize{n_nodes_to_optimize}
{
    assert(n_hosts > 0);
}

Genotype NodeOnHostTransformer::transform(Genotype genotype) const noexcept
{
    assert(genotype.size() == n_nodes_to_optimize * n_hosts);

    for (std::size_t n{0}; n < n_nodes_to_optimize; ++n) {
        const HostSetView host_set{genotype, n_hosts, n};
        if (host_set.count_hosts() == 0) { host_set.set(0); }
    }
    return genotype;
}

std::size_t NodeOnHostTransformer::get_n_hosts() const noexcept
{
    return n_hosts;
}

std::size_t NodeOnHostTransformer::get_n_nodes_to_optimize() const noexcept
{
    return n_nodes_to_optimize;
}

Genotype transform(TransformerTuple& transformer_tuple, Genotype genotype) noexcept
{
    return std::apply(
        [genotype = std::move(genotype)](auto&... transformer) mutable -> Genotype {
            ((genotype = transformer.transform(std::move(genotype))), ...);
            return std::move(genotype);
        },
        transformer_tuple);
}

namespace {

void make_available_on_all_hosts_in_cache(HostSetView node_hosts, const HostSetComputationCache& cache) noexcept
{
    for (std::size_t h{0}; h < node_hosts.get_n_hosts(); ++h) {
        if (cache.test(h)) { node_hosts.set(h); }
    }
}

}

}
