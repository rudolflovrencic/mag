#include "MAO/Configuration.hpp"

#include <cassert>
#include <algorithm>
#include <numeric>

namespace MAO {

Host::Host(std::string name, unsigned trust) noexcept : name{std::move(name)}, trust{trust}
{
    assert(!this->name.empty());
    assert(this->trust > 0);
}

const std::string& Host::get_name() const noexcept
{
    return name;
}

unsigned Host::get_trust() const noexcept
{
    return trust;
}

bool HostEqualTo::operator()(const Host& lhs, const Host& rhs) const noexcept
{
    return lhs.get_name() == rhs.get_name();
}

Constraint::Constraint(ConstrainedNodeVector elements, unsigned importance) noexcept
        : elements{std::move(elements)}, importance{importance}
{
    assert(this->elements.size() >= 2);
    assert(this->importance > 0);
}

const ConstrainedNodeVector& Constraint::get_elements() const noexcept
{
    return elements;
}

unsigned Constraint::get_importance() const noexcept
{
    return importance;
}

bool Constraint::contains(ConstrainedNode constrained_node) const noexcept
{
    return elements.contains(constrained_node);
}

bool ConstraintEqualTo::operator()(const Constraint& lhs, const Constraint& rhs) const noexcept
{
    if (lhs.get_elements().size() != rhs.get_elements().size()) { return false; }
    for (auto lhs_element : lhs.get_elements()) {
        if (!rhs.contains(lhs_element)) { return false; }
    }
    return true;
}

ConstrainedNodeVector get_nodes_to_optimize(const ConstraintVector& constraints,
                                            const EquivalenceMap& equivalences,
                                            const TrioVector& trios) noexcept
{
    ConstrainedNodeVector result;
    result.reserve(std::accumulate(constraints.cbegin(),
                                   constraints.cend(),
                                   std::size_t{0},
                                   [](std::size_t sum, const Constraint& constraint) noexcept -> std::size_t {
                                       return sum + constraint.get_elements().size();
                                   }));
    for (const auto& constraint : constraints) {
        for (auto element : constraint.get_elements()) { result.push_back(element); }
    }

    const auto deduced_nodes = get_deduced_nodes_to_optimize(constraints, equivalences, trios);
    result.reserve(result.size() + deduced_nodes.size());
    for (const auto deduced_node : deduced_nodes) { result.push_back(deduced_node); }

    return result;
}

ConstrainedNodeVector get_deduced_nodes_to_optimize(const ConstraintVector& constraints,
                                                    const EquivalenceMap& equivalences,
                                                    const TrioVector& trios) noexcept
{
    ConstrainedNodeVector result;
    static constexpr std::size_t reserve_size{16};
    result.reserve(reserve_size);

    for (const auto& [var_decl, assigned_nodes] : equivalences) {
        for (const auto assigned_node : assigned_nodes) {
            if (const auto middle_node_opt = to_middle_node(assigned_node)) {
                const auto trio_it = std::find_if(
                    trios.cbegin(), trios.cend(), [middle_node = *middle_node_opt](const Trio& trio) noexcept -> bool {
                        return Trio::MiddleNodeNodeEqualTo{}(trio.get_second(), middle_node);
                    });
                if (trio_it != trios.cend()) {
                    const auto first_opt = to_constrained_node(trio_it->get_first());
                    const auto third_opt = to_constrained_node(trio_it->get_third());
                    if (first_opt && third_opt) {
                        const auto constraint_it = std::find_if(
                            constraints.cbegin(),
                            constraints.cend(),
                            [first = *first_opt, third = *first_opt](const Constraint& constraint) noexcept -> bool {
                                const auto& elems = constraint.get_elements();
                                return elems.size() == 2 && elems.contains(first) && elems.contains(third);
                            });
                        if (constraint_it != constraints.cend()) { result.push_back(var_decl); }
                    }
                }
            }
        }
    }

    return result;
}

Configuration::Configuration(HostVector hosts,
                             TaggedNodeVector tagged_nodes,
                             ConstrainedNodeVector nodes_to_optimize,
                             ConstraintVector constraints,
                             EquivalenceMap equivalences,
                             IndexingInformation indexing_information,
                             TrioVector trios,
                             FlowControlInformation flow_control_information) noexcept
        : hosts{std::move(hosts)},
          tagged_nodes{std::move(tagged_nodes)},
          nodes_to_optimize{std::move(nodes_to_optimize)},
          constraints{std::move(constraints)},
          equivalences{std::move(equivalences)},
          indexing_information{std::move(indexing_information)},
          trios{std::move(trios)},
          flow_control_information{std::move(flow_control_information)}
{
    assert(!this->hosts.empty());
}

const HostVector& Configuration::get_hosts() const noexcept
{
    return hosts;
}

const TaggedNodeVector& Configuration::get_tagged_nodes() const noexcept
{
    return tagged_nodes;
}

const ConstraintVector& Configuration::get_constraints() const noexcept
{
    return constraints;
}

const ConstrainedNodeVector& Configuration::get_nodes_to_optimize() const noexcept
{
    return nodes_to_optimize;
}

const EquivalenceMap& Configuration::get_equivalences() const noexcept
{
    return equivalences;
}

const IndexingInformation& Configuration::get_indexing_information() const noexcept
{
    return indexing_information;
}

const TrioVector& Configuration::get_trios() const noexcept
{
    return trios;
}

const FlowControlInformation& Configuration::get_flow_control_information() const noexcept
{
    return flow_control_information;
}

}
