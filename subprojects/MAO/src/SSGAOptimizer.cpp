#include "MAO/SSGAOptimizer.hpp"

#include <cassert>
#include <algorithm>
#include <iostream>

namespace MAO {

SSGAOptimizer::UniformCrossover::UniformCrossover(TransformerTuple& transformers) noexcept
        : rng{std::random_device{}()}, transformers{transformers}
{}

Genotype SSGAOptimizer::UniformCrossover::mate(const Genotype& first_parent, const Genotype& second_parent)
{
    assert(first_parent.size() == second_parent.size());

    Genotype child{first_parent};
    std::bernoulli_distribution coin_flip;
    for (std::size_t i{0}; i < child.size(); ++i) {
        if (coin_flip(rng)) { child[i] = second_parent[i]; }
    }
    return transform(transformers, std::move(child));
}

SSGAOptimizer::SinglePointCrossover::SinglePointCrossover(TransformerTuple& transformers) noexcept
        : rng{std::random_device{}()}, transformers{transformers}
{}

Genotype SSGAOptimizer::SinglePointCrossover::mate(const Genotype& first_parent, const Genotype& second_parent)
{
    assert(first_parent.size() == second_parent.size());

    Genotype child{first_parent};
    std::uniform_int_distribution<std::size_t> dist{0, child.size()};
    for (auto i = dist(rng); i < child.size(); ++i) { child.set(i, second_parent.test(i)); }
    return transform(transformers, std::move(child));
}

SSGAOptimizer::RandomMutation::RandomMutation(TransformerTuple& transformers) noexcept
        : rng{std::random_device{}()}, transformers{transformers}
{}

void SSGAOptimizer::RandomMutation::mutate(Genotype& solution)
{
    std::bernoulli_distribution coin_flip;
    for (std::size_t i{0}; i < solution.size(); ++i) { solution.set(i, coin_flip(rng)); }
    solution = transform(transformers, std::move(solution));
}

SSGAOptimizer::RandomFlipMutation::RandomFlipMutation(TransformerTuple& transformers) noexcept
        : rng{std::random_device{}()}, transformers{transformers}
{}

void SSGAOptimizer::RandomFlipMutation::mutate(Genotype& solution)
{
    std::uniform_int_distribution<std::size_t> dist{0, solution.size() - 1};
    solution.flip(dist(rng));
    solution = transform(transformers, std::move(solution));
}

Genotype SSGAOptimizer::optimize(const Configuration& configuration)
{
    const auto n_hosts           = configuration.get_hosts().size();
    const auto nodes_to_optimize = configuration.get_nodes_to_optimize();
    const auto genotype_size     = n_hosts * nodes_to_optimize.size();

    static constexpr std::size_t population_size{10000};
    static constexpr std::size_t tournament_size{3};
    static constexpr double mutation_probability{0.3};

    TransformerTuple transformers{
        EquivalenceComplianceTransformer{
            configuration.get_hosts().size(), nodes_to_optimize, configuration.get_equivalences()},
        IndexingInformationComplianceTransformer{
            configuration.get_hosts().size(), nodes_to_optimize, configuration.get_indexing_information()},
        TrioTransformer{
            configuration.get_hosts(), nodes_to_optimize, configuration.get_trios(), configuration.get_equivalences()},
        LenFunctionCallInformationComplianceTransformer{configuration.get_hosts().size(), nodes_to_optimize},
        FlowControlInformationComplianceTransformer{
            configuration.get_hosts().size(), nodes_to_optimize, configuration.get_flow_control_information()},
        NodeOnHostTransformer{configuration.get_hosts().size(), nodes_to_optimize.size()},
    };

    std::vector<Genotype> initial_solutions{population_size, Genotype{genotype_size}};
    auto random_mutation = std::make_unique<RandomMutation>(transformers);
    for (auto& solution : initial_solutions) { random_mutation->mutate(solution); }

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds{20}));

    std::vector<std::unique_ptr<Algorithm::Selection>> selections;
    selections.push_back(std::make_unique<Algorithm::TournamentSelection>(population_size, tournament_size));
    const std::vector<double> selection_weights{1.0};

    std::vector<std::unique_ptr<Algorithm::Crossover>> crossovers;
    crossovers.push_back(std::make_unique<UniformCrossover>(transformers));
    crossovers.push_back(std::make_unique<SinglePointCrossover>(transformers));
    const std::vector<double> crossover_weights{0.5, 0.5};

    std::vector<std::unique_ptr<Algorithm::Mutation>> mutations;
    mutations.push_back(std::move(random_mutation));
    mutations.push_back(std::make_unique<RandomFlipMutation>(transformers));
    const std::vector<double> mutation_weights{0.2, 0.8};

    Algorithm algorithm{std::make_unique<ConstraintEvaluator>(
                            nodes_to_optimize, configuration.get_constraints(), configuration.get_hosts()),
                        std::move(terminations),
                        {std::move(selections), std::move(selection_weights)},
                        {std::move(crossovers), std::move(crossover_weights)},
                        {std::move(mutations), std::move(mutation_weights)},
                        mutation_probability};

    auto [final_population, met_criterion, state] = algorithm.run(
        std::move(initial_solutions),
        [&configuration](const Algorithm::Population& population, const Algorithm::State& state) noexcept -> void {
            static constexpr std::size_t log_frequency{1 << 13};
            if (state.iteration % log_frequency == 0) {
                std::cout << "Time: "
                          << std::chrono::duration_cast<std::chrono::milliseconds>(state.running_time).count() / 1e3
                          << "s\nStddev: " << compute_fitness_standard_deviation(population) << "\nBest ("
                          << population.front().get_fitness() << "):\n"
                          << GenotypeLogger{population.front().solution,
                                            configuration.get_nodes_to_optimize(),
                                            configuration.get_hosts()}
                          << '\n';
            }
        });

    return std::move(final_population.front().solution);
}

double SSGAOptimizer::compute_fitness_variance(const Algorithm::Population& population) noexcept
{
    const auto mean = std::accumulate(population.cbegin(),
                                      population.cend(),
                                      0.0,
                                      [](double sum, const Algorithm::Individual& individual) noexcept -> double {
                                          return sum + individual.get_fitness();
                                      }) /
                      population.size();

    return std::accumulate(population.cbegin(),
                           population.cend(),
                           0.0,
                           [mean](double variance, const Algorithm::Individual& individual) noexcept -> double {
                               return variance + std::pow(individual.get_fitness() - mean, 2);
                           });
}

double SSGAOptimizer::compute_fitness_standard_deviation(const Algorithm::Population& population) noexcept
{
    return std::sqrt(compute_fitness_variance(population));
}

}
