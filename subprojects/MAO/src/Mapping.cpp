#include "MAO/Mapping.hpp"
#include "MAO/HostSetView.hpp"
#include "MAO/Visitor.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>

namespace MAO {

namespace {

[[nodiscard]] HostRefVector build_host_ref_vector_with_exclusion_set(const HostVector& all_hosts,
                                                                     const HostRefVector& excluded_hosts) noexcept;

[[nodiscard]] bool is_multiplication_or_additon_element(TaggedNode node) noexcept;

}

bool HostRefEqualTo::operator()(std::reference_wrapper<const Host> lhs,
                                std::reference_wrapper<const Host> rhs) const noexcept
{
    return HostEqualTo{}(lhs.get(), rhs.get());
}

bool is_subset(const HostRefVector& subset, const HostRefVector& superset) noexcept
{
    for (auto subset_host_ref : subset) {
        if (!superset.contains(subset_host_ref)) { return false; }
    }
    return true;
}

bool is_intersection_empty(const HostRefVector& lhs, const HostRefVector& rhs) noexcept
{
    return !get_first_common_host(lhs, rhs);
}

const Host* get_first_common_host(const HostRefVector& lhs, const HostRefVector& rhs) noexcept
{
    for (const auto h : lhs) {
        if (rhs.contains(h)) { return &h.get(); }
    }
    return nullptr;
}

const Host* get_first_common_host(const HostRefVector& a, const HostRefVector& b, const HostRefVector& c) noexcept
{
    for (const auto h : a) {
        if (b.contains(h) && c.contains(h)) { return &h.get(); }
    }
    return nullptr;
}

HostRefVector compute_intersection(const HostRefVector& a, const HostRefVector& b) noexcept
{
    HostRefVector result;
    result.reserve(std::min(a.size(), b.size()));
    for (const auto i : a) {
        if (b.contains(i)) { result.push_back(i); }
    }
    return result;
}

HostRefVector compute_union(HostRefVector a, const HostRefVector& b) noexcept
{
    for (const auto i : b) { a.push_back(i); }
    return a;
}

std::ostream& operator<<(std::ostream& os, const MappingLogger& mapping_logger) noexcept
{
    for (const auto& [tagged_node, host_ref_vec] : mapping_logger.mapping.get()) {
        os << mapping_logger.indent << tagged_node << ": ";
        assert(!host_ref_vec.empty());
        for (const auto& host_ref : host_ref_vec) { os << host_ref.get().get_name() << ' '; }
        os << '\n';
    }
    return os;
}

const HostRefVector& get_hosts(const Mapping& mapping, TaggedNode node) noexcept
{
    const auto it = mapping.find(node);
    assert(it != mapping.cend());
    return it->second;
}

Mapping GenotypeToMappingConverter::convert(const Genotype& genotype) const noexcept
{
    assert(genotype.size() == nodes_to_optimize.get().size() * hosts.get().size());
    assert(std::all_of(nodes_to_optimize.get().cbegin(),
                       nodes_to_optimize.get().cend(),
                       [&nodes = this->nodes.get()](ConstrainedNode node_to_optimize) noexcept -> bool {
                           return nodes.contains(to_tagged_node(node_to_optimize));
                       }));

    Mapping mapping;
    mapping.reserve(nodes.get().size());
    for (const auto node : nodes.get()) { mapping.emplace(node, get_node_hosts(genotype, node)); }

    // Every node must be assigned to at least one host.
    assert(std::none_of(mapping.cbegin(), mapping.cend(), [](const Mapping::value_type& value) noexcept -> bool {
        return value.second.empty();
    }));

    return mapping;
}

HostRefVector GenotypeToMappingConverter::get_node_hosts(const Genotype& genotype, TaggedNode node) const noexcept
{
    const auto constrained_node_opt = to_constrained_node(node);
    return constrained_node_opt && nodes_to_optimize.get().contains(*constrained_node_opt) ?
               get_optimized_node_hosts(genotype, *constrained_node_opt) :
               get_nonoptimized_node_hosts(genotype, node);
}

HostRefVector GenotypeToMappingConverter::get_optimized_node_hosts(const Genotype& genotype,
                                                                   ConstrainedNode node) const noexcept
{
    assert(nodes_to_optimize.get().contains(node));

    const auto n_hosts = hosts.get().size();
    const HostSetConstView host_view{genotype, n_hosts, get_node_index(nodes_to_optimize, node)};

    HostRefVector result;
    result.reserve(host_view.get_number_of_node_hosts());
    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (host_view.test(h)) { result.push_back(hosts.get()[h]); }
    }
    return result;
}

HostRefVector GenotypeToMappingConverter::get_nonoptimized_node_hosts(const Genotype& genotype,
                                                                      TaggedNode node) const noexcept
{
    assert(!to_constrained_node(node) || !nodes_to_optimize.get().contains(*to_constrained_node(node)));

    HostRefVector hosts_to_exclude;
    static constexpr std::size_t reserve_size{8};
    hosts_to_exclude.reserve(reserve_size);

    for (const auto trio : trios.get()) {
        if (TaggedNodeEqualTo{}(to_outer_node(trio.get_second()), node) && is_multiplication_or_additon_element(node)) {
            const auto first_index = get_node_index_if_present(nodes_to_optimize, trio.get_first());
            const auto third_index = get_node_index_if_present(nodes_to_optimize, trio.get_third());
            if (first_index && third_index) {
                const HostSetConstView first_hosts{genotype, hosts.get().size(), *first_index};
                const HostSetConstView second_hosts{genotype, hosts.get().size(), *third_index};
                for (std::size_t h{0}; h < hosts.get().size(); ++h) {
                    if (first_hosts.test(h) + second_hosts.test(h) == 1) { hosts_to_exclude.push_back(hosts.get()[h]); }
                }
            }
        }
    }

    return build_host_ref_vector_with_exclusion_set(hosts, hosts_to_exclude);
}

Mapping convert_genotype_to_mapping(const Genotype& genotype,
                                    const ConstrainedNodeVector& nodes_to_optimize,
                                    const HostVector& hosts,
                                    const TaggedNodeVector& nodes,
                                    const EquivalenceMap& eqivalences,
                                    const TrioVector& trios) noexcept
{
    return GenotypeToMappingConverter{nodes, nodes_to_optimize, hosts, eqivalences, trios}.convert(genotype);
}

Mapping convert_genotype_to_mapping(const Genotype& genotype, const MAO::Configuration& config) noexcept
{
    return convert_genotype_to_mapping(genotype,
                                       config.get_nodes_to_optimize(),
                                       config.get_hosts(),
                                       config.get_tagged_nodes(),
                                       config.get_equivalences(),
                                       config.get_trios());
}

namespace {

HostRefVector build_host_ref_vector_with_exclusion_set(const HostVector& all_hosts,
                                                       const HostRefVector& excluded_hosts) noexcept
{
    assert(std::all_of(excluded_hosts.cbegin(), excluded_hosts.cend(), [&all_hosts](const Host& host) noexcept -> bool {
        return all_hosts.contains(host);
    }));

    HostRefVector result;
    result.reserve(all_hosts.size() - excluded_hosts.size());
    for (const auto& host : all_hosts) {
        if (!excluded_hosts.contains(host)) { result.push_back(host); }
    }

    assert(!result.empty());
    return result;
}

bool is_multiplication_or_additon_element(TaggedNode node) noexcept
{
    return std::holds_alternative<std::reference_wrapper<const MAL::AST::MultiplicationExpression::Element>>(node) ||
           std::holds_alternative<std::reference_wrapper<const MAL::AST::AdditionExpression::Element>>(node);
}

}

}
