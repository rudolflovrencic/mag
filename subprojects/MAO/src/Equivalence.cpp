#include "MAO/Equivalence.hpp"

#include "MAO/Visitor.hpp"

#include <cassert>
#include <exception>

namespace MAO {

EquivalenceExtractor::EquivalenceExtractor() noexcept
{
    static constexpr std::size_t reserve_size{16};
    result.reserve(reserve_size);
}

void EquivalenceExtractor::operator()(const MAL::AST::Assignment& assignment) noexcept
{
    static constexpr Visitor left_side_visitor{
        [](const MAL::AST::VariableDeclaration& var_decl) noexcept -> const MAL::AST::VariableDeclaration& {
            return var_decl;
        },
        [](const MAL::AST::VariableReference& var_ref) noexcept -> const MAL::AST::VariableDeclaration& {
            assert(var_ref.variable_identifier.declaration);
            return *var_ref.variable_identifier.declaration;
        }};

    const auto& var_decl = boost::apply_visitor(left_side_visitor, assignment.left_side);

    boost::apply_visitor(Visitor{[this, &var_decl](const MAL::AST::ArrayLiteral& arr_lit) noexcept -> void {
                                     add(var_decl, get_top_level_tagged_node(arr_lit.value));
                                     add(var_decl, get_top_level_tagged_node(arr_lit.size));
                                 },
                                 [this, &var_decl](const MAL::AST::BooleanOrExpression& or_expr) noexcept -> void {
                                     add(var_decl, get_top_level_tagged_node(or_expr));
                                 }},
                         assignment.right_side);
}

void EquivalenceExtractor::operator()(const MAL::AST::Send& /*send*/) const noexcept {}

void EquivalenceExtractor::operator()(const MAL::AST::Receive& /*receive*/) const noexcept {}

void EquivalenceExtractor::operator()(const MAL::AST::Print& /*print*/) const noexcept
{
    assert(false); // Print statement should never be a part of application MAL code.
    std::terminate();
}

void EquivalenceExtractor::operator()(const MAL::AST::IfStatement& if_statement) noexcept
{
    apply_to_all(if_statement.then_statements);
    apply_to_all(if_statement.else_statements);
}

void EquivalenceExtractor::operator()(const MAL::AST::ForStatement& for_statement) noexcept
{
    if (for_statement.init_statement) { operator()(*for_statement.init_statement); }
    if (for_statement.step_statement) { operator()(*for_statement.step_statement); }
    apply_to_all(for_statement.body);
}

void EquivalenceExtractor::apply_to_all(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    for (const auto& statement : ast) { boost::apply_visitor(*this, statement); }
}

void EquivalenceExtractor::add(const MAL::AST::VariableDeclaration& key, TaggedNode value) noexcept
{
    // Protect against self-assignment and ensure node actually has to be optimized.
    if (!TaggedNodeEqualTo{}(key, value)) { result[key].push_back(value); }
}

EquivalenceMap extract_equivalences(const std::vector<MAL::AST::Statement>& ast) noexcept
{
    EquivalenceExtractor equivalence_extractor;
    equivalence_extractor.apply_to_all(ast);
    return std::move(equivalence_extractor.result);
}

}
