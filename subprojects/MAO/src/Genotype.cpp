#include "MAO/Genotype.hpp"

namespace MAO {

bool is_subset(const Genotype& genotype,
               std::size_t n_hosts,
               std::size_t subset_node_index,
               std::size_t superset_node_index) noexcept
{
    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (genotype.test(subset_node_index * n_hosts + h) && !genotype.test(superset_node_index * n_hosts + h)) {
            return false;
        }
    }
    return true;
}

bool is_intersection_empty(const Genotype& genotype,
                           std::size_t n_hosts,
                           std::size_t node_a_index,
                           std::size_t node_b_index) noexcept
{
    for (std::size_t h{0}; h < n_hosts; ++h) {
        if (genotype.test(node_a_index * n_hosts + h) && genotype.test(node_b_index * n_hosts + h)) { return false; }
    }
    return true;
}

std::ostream& operator<<(std::ostream& os, GenotypeLogger logger) noexcept
{
    const auto& genotype           = logger.genotype.get();
    const auto& constrained_nodes  = logger.constrained_nodes.get();
    const auto& hosts              = logger.hosts.get();
    const auto n_hosts             = hosts.size();
    const auto n_constrained_nodes = constrained_nodes.size();
    assert(genotype.size() == n_hosts * n_constrained_nodes);

    for (std::size_t h{0}; h < n_hosts; ++h) {
        os << logger.indent << hosts[h].get_name() << ": ";
        for (std::size_t n{0}; n < n_constrained_nodes; ++n) {
            if (genotype.test(n * n_hosts + h)) { os << constrained_nodes[n] << ' '; }
        }
        os << '\n';
    }

    return os;
}

}
